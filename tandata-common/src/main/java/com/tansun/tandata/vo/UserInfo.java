package com.tansun.tandata.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@ApiModel(value="User基本对象",description="基本用户对象user")
@Data
public class UserInfo implements Serializable {

    private static final long serialVersionUID = 1513522594496991171L;

    @ApiModelProperty(value="用户编号")
    private String userCode;

    @ApiModelProperty(value="用户名称")
    private String userName;

    @ApiModelProperty(value="登陆名称")
    private String loginName;

    @ApiModelProperty(value="用户电话")
    private String userPhone;

    @ApiModelProperty(value="密码")
    private String password;

    @ApiModelProperty(value="类型")
    private String type;

    @ApiModelProperty(value="状态")
    private Integer status;
    
    @ApiModelProperty(value="角色信息")
    private List<String> roles;
    
    
}
