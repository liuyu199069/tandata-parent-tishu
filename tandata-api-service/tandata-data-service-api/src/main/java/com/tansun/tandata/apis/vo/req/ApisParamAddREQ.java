package com.tansun.tandata.apis.vo.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.util.Date;

/**
 * 新增接口参数信息配置
 * @author LinLaiChun
 * @date 2020-06-10 15:08:46
 * @Version v1.0
 */
@Data
@ApiModel(value="新增接口参数信息配置")
public class ApisParamAddREQ implements Serializable {
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "ID", required = false)
	private String id;

	@ApiModelProperty(value = "类型（请求参数或返回参数）", required = false)
	private Integer type;

	@ApiModelProperty(value = "所属接口（接口表）", required = false)
	private String interfaceId;

	@ApiModelProperty(value = "参数名称", required = false)
	private String paramName;

	@ApiModelProperty(value = "参数类型", required = false)
	private String paramType;

	@ApiModelProperty(value = "参数示例值", required = false)
	private String paramSampleValue;

	@ApiModelProperty(value = "参数默认值", required = false)
	private String paramDefaultValue;

	@ApiModelProperty(value = "参数测试值", required = false)
	private String paramTestValue;

	@ApiModelProperty(value = "参数描述", required = false)
	private String remark;

	@ApiModelProperty(value = "排序字段", required = false)
	private Integer sort;

	@ApiModelProperty(value = "创建者ID", required = false)
	private String creatorId;

	@ApiModelProperty(value = "创建时间", required = false)
	private Date createTime;

	@ApiModelProperty(value = "修改者ID", required = false)
	private String modifierId;

	@ApiModelProperty(value = "修改时间", required = false)
	private Date modifyTime;

}