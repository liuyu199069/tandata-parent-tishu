package com.tansun.tandata.apis.mapper;

import com.tansun.tandata.apis.entity.ApisParam;
import com.tansun.tjdp.orm.persistence.BaseDao;

import java.util.List;
import java.util.Map;

/**
 * 接口参数信息配置表Dao
 * @author LinLaiChun
 * @date 2020-06-10 14:36:59
 * @Version v1.0
 */
public interface ApisParamDao extends BaseDao<ApisParam> {

	int insert(ApisParam apisParam);

	int batchInsert(List<ApisParam> apisParamList);

	int updateById(ApisParam apisParam);

	int batchUpdateTraditional(List<ApisParam> apisParamList);

	int batchUpdate(List<ApisParam> apisParamList);

	int deleteById(String id);

	int deleteByIds(List<String> idList);

    ApisParam getById(String id);

	List<ApisParam> getByIds(List<String> idList);

	List<ApisParam> queryListByMap(Map<String, Object> queryMap);
}