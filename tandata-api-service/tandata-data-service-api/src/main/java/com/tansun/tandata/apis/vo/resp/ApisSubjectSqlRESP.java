package com.tansun.tandata.apis.vo.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.util.Date;

/**
 * 主题sql文本信息
 * @author LinLaiChun
 * @date 2020-06-10 15:08:46
 * @Version v1.0
 */
@Data
@ApiModel(value="主题sql文本信息")
public class ApisSubjectSqlRESP implements Serializable {
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "ID")
	private String id;

	@ApiModelProperty(value = "所属主题（主题表）")
	private String subjectId;

	@ApiModelProperty(value = "sql文本")
	private String sqlTxt;

}