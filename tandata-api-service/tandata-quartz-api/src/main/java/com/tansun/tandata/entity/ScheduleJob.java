package com.tansun.tandata.entity;

import com.tansun.tjdp.orm.entity.BaseEntity;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * 任务表
 * @author LinLaiChun
 * @date 2020-06-04 11:02:04
 * @Version v1.0
 */
@Entity
@Data
@Table(name="schedule_job")
public class ScheduleJob extends BaseEntity<ScheduleJob, String> implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	 * 主键
	 */
	@Id
	@Column(name = "id")
	private String id;

	/**
	 * 任务名称
	 */
	@Column(name = "job_name")
	private String jobName;

	/**
	 * 任务状态(PAUSED暂停、NORMAL运行)
	 */
	@Column(name = "job_status")
	private String jobStatus;

	/**
	 * 任务分组
	 */
	@Column(name = "job_group")
	private String jobGroup;

	/**
	 * 表达式
	 */
	@Column(name = "cron_expression")
	private String cronExpression;

	/**
	 * 描述
	 */
	@Column(name = "description")
	private String description;

	/**
	 * 调用类型（0类调用1服务调用）
	 */
	@Column(name = "type")
	private Integer type;

	/**
	 * 调用服务名/调用类
	 */
	@Column(name = "bean_name")
	private String beanName;

	/**
	 * 调用url/类中方法
	 */
	@Column(name = "method_name")
	private String methodName;

	/**
	 * 创建者ID
	 */
	@Column(name = "creator_id")
	private String creatorId;

	/**
	 * 创建时间
	 */
	@Column(name = "create_time")
	private Date createTime;

	/**
	 * 修改者ID
	 */
	@Column(name = "modifier_id")
	private String modifierId;

	/**
	 * 修改时间
	 */
	@Column(name = "modify_time")
	private Date modifyTime;





}