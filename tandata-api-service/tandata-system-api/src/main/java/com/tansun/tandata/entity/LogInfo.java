package com.tansun.tandata.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

@Data
@Table(name="log_info")
public class LogInfo implements Serializable {

    private static final long serialVersionUID = -1244581593373387004L;
    /**
     * ID
     */
    @Id
    @Column(name = "ID")
    private String id;//主键
    /**
     * 外键
     */
    @Column(name = "bkey")
    private String bkey;
    /**
     * 是否成功
     */
    @Column(name = "success")
    private Integer success;
    /**
     * 服务英文名
     */
    @Column(name = "server_id")
    private String serverId;
    /**
     * 服务员中文名
     */
    @Column(name = "server_name")
    private String severName;
    /**
     * 操作名称
     */
    @Column(name = "operation")
    private String operation;//
    /**
     * 模块名称
     */
    @Column(name = "modular")
    private String modular;//
    /**
     * ip地址
     */
    @Column(name = "ip_str")
    private String ipStr;//
    /**
     * 日志描述
     */
    @Column(name = "comment")
    private String comment;//日志描述
    /**
     * 创建人
     */
    @Column(name = "creater")
    private String creater;
    /**
     * 创建时间
     */
    @Column(name = "creater_time")
    @JsonFormat(locale = "ZH",timezone = "GMT+8")
    private Date createTime;
}
