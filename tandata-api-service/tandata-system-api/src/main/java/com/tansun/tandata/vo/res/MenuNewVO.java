package com.tansun.tandata.vo.res;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@ApiModel(value="菜单展示对象",description="菜单展示对象")
@Data
public class MenuNewVO {

    @ApiModelProperty(value="菜单Id")
    private String idVal;

    @ApiModelProperty(value="菜单Id")
    private String id;

    @ApiModelProperty(value="父类Id")
    private String parentId;

    @ApiModelProperty(value="组件的的url")
    private String muUrl;

    @ApiModelProperty(value="页面路径")
    private String pathUrl;

    @ApiModelProperty(value="路由name")
    private String muName;

    @ApiModelProperty(value="路由名称")
    private String titleName;

    @ApiModelProperty(value="是否隐藏")
    private boolean hiddenVal = false;

    @ApiModelProperty(value="转发 url")
    private String redirectVal;

    @ApiModelProperty(value="图标")
    private String mIcon;

    @ApiModelProperty(value = "排序字段")
    private Integer sort;

    @ApiModelProperty(value="子菜单")
    private List<MenuNewVO> childrens = new ArrayList<>();

    public void setChildrens(List<MenuNewVO> children) {
        if (children.size() > 1){
            children.sort((o1, o2) -> {
                if (o1.sort == null){
                    return -1;
                }else if (o2.sort == null){
                    return 1;
                }
                return o1.sort - o2.sort;
            });
        }
        this.childrens = children;
    }
}
