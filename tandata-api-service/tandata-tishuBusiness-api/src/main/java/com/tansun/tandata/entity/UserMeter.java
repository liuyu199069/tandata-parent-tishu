package com.tansun.tandata.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.tansun.tjdp.orm.entity.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;
@Entity
@Table(
        name = "BUS_USER_METER"
)
public class UserMeter extends BaseEntity<UserMeter, String> implements Serializable {

    private static final long serialVersionUID = -2701052307289939922L;
    /**
     * ID
     */
    @Id
    @Column(name = "ID")
    private String id;
    /**
     * 用户账号
     */
    @Column(name = "USER_NAME")
    private String userName;
    /**
     * 仪表板id
     */
    @Column(name = "METER_ID")
    private String meterId;
    /**
     * 仪表板名称
     */
    @Column(name = "METER_NAME")
    private String meterName;
    /**
     * 仪表板创建时间
     */
    @Column(name = "METER_CREATE_TIME")
    @JsonFormat(locale = "ZH",timezone = "GMT+8")
    private Date meterCreateTime;
    /**
     * 提数状态
     */
    @Column(name = "STATE")
    private Integer state;
    /**
     * 来源表表名
     */
    @Column(name = "TABLE_NAME")
    private String tableName;
    /**
     * 创建时间
     */
    @Column(name = "CREATE_TIME")
    @JsonFormat(locale = "ZH",timezone = "GMT+8")
    private Date createTime;
    /**
     * 处理完成时间
     */
    @Column(name = "UPDATE_TIME")
    @JsonFormat(locale = "ZH",timezone = "GMT+8")
    private Date updateTime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getMeterId() {
        return meterId;
    }

    public void setMeterId(String meterId) {
        this.meterId = meterId;
    }

    public String getMeterName() {
        return meterName;
    }

    public void setMeterName(String meterName) {
        this.meterName = meterName;
    }

    public Date getMeterCreateTime() {
        return meterCreateTime;
    }

    public void setMeterCreateTime(Date meterCreateTime) {
        this.meterCreateTime = meterCreateTime;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}