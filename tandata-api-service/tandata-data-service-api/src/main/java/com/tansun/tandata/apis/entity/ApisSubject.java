package com.tansun.tandata.apis.entity;

import com.tansun.tandata.apis.vo.resp.SubjectTreeRESP;
import com.tansun.tjdp.orm.entity.BaseEntity;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * 接口主题信息
 * @author LinLaiChun
 * @date 2020-06-10 15:08:46
 * @Version v1.0
 */
@Entity
@Data
@Table(name="t_ds_apis_subject")
public class ApisSubject extends BaseEntity<ApisSubject, String> implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	 * ID
	 */
	@Id
	@Column(name = "id")
	private String id;

	/**
	 * 主题名称
	 */
	@Column(name = "name")
	private String name;

	/**
	 * 父主题ID
	 */
	@Column(name = "parent_id")
	private String parentId;

	/**
	 * 类型（0服务、1主题）
	 */
	@Column(name = "type")
	private Integer type;

	/**
	 * 主题描述
	 */
	@Column(name = "remark")
	private String remark;

	/**
	 * 排序字段
	 */
	@Column(name = "sort")
	private Integer sort;

	/**
	 * 创建者ID
	 */
	@Column(name = "creator_id")
	private String creatorId;

	/**
	 * 创建时间
	 */
	@Column(name = "create_time")
	private Date createTime;

	/**
	 * 修改者ID
	 */
	@Column(name = "modifier_id")
	private String modifierId;

	/**
	 * 修改时间
	 */
	@Column(name = "modify_time")
	private Date modifyTime;

	/**
	 * 创建者名称
	 */
	private String creatorName;

	/**
	 * 修改者名称
	 */
	private String modifierName;

	/**
	 * 转换为树节点
	 * @return
	 */
	public SubjectTreeRESP toTree(){
		SubjectTreeRESP treeVo = new SubjectTreeRESP();
		treeVo.setId(this.id);
		treeVo.setLabel(this.name);
		treeVo.setParentId(this.parentId);
		treeVo.setType(this.type);
		return treeVo;
	}


}