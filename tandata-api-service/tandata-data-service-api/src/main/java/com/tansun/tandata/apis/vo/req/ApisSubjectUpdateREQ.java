package com.tansun.tandata.apis.vo.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * 编辑接口主题信息
 * @author LinLaiChun
 * @date 2020-06-10 15:08:46
 * @Version v1.0
 */
@Data
@ApiModel(value="编辑接口主题信息")
public class ApisSubjectUpdateREQ implements Serializable {
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "ID", required = true)
	@Size(max = 70)
	@NotBlank
	private String id;

	@ApiModelProperty(value = "主题名称", required = false)
	@Size(max = 200)
	private String name;

	@ApiModelProperty(value = "父主题ID", required = false)
	@Size(max = 70)
	private String parentId;

	@ApiModelProperty(value = "类型（0服务、1主题）", required = false)
	@PositiveOrZero
	@Max(value = 1)
	private Integer type;

	@ApiModelProperty(value = "主题描述", required = false)
	@Size(max = 1000)
	private String remark;

	@ApiModelProperty(value = "排序字段", required = false)
	@PositiveOrZero
	@Max(value = 999)
	private Integer sort;


}