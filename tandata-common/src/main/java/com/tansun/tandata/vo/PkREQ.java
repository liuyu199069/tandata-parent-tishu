package com.tansun.tandata.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * ID集合
 * @author linlaichun
 * @date 2020-05-21 14:12:15
 * @Version v1.0
 */
@Data
@ApiModel(value="ID集合")
public class PkREQ implements Serializable {
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "ID集合",required = true)
	private List<String> idList;

}