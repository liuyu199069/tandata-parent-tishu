package com.tansun.tandata.service;

import com.tansun.tandata.entity.Approval;
import com.tansun.tandata.entity.Role;
import com.tansun.tandata.entity.User;
import com.tansun.tandata.utils.ResponseBase;
import com.tansun.tandata.vo.req.ApprovalPageVo;
import com.tansun.tandata.vo.res.UserRoleVO;

import java.util.List;

public interface ApprovalService {
    public ResponseBase addApproval(Approval approval);
    public ResponseBase updateApproval(Approval approval);
    public List<Approval> getApploval(ApprovalPageVo vo);
    public Integer deleteApproval(String id);
    public ResponseBase authUserApprovalRole(String loginName);
    public List<Role> authUserApprovalRole(UserRoleVO user);
}
