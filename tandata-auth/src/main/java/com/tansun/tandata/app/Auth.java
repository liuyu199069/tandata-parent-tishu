package com.tansun.tandata.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.support.ResourceBundleMessageSource;

/**
 * @Description 启动类
 * @CreateDate 20200425
 * @author yangliu 
 * @Version v1.0
 */
@SpringBootApplication(scanBasePackages=("com.tansun"))
@EnableEurekaClient
@EnableFeignClients(basePackages=("com.tansun.tandata"))
@EnableCircuitBreaker
@ComponentScan(basePackages=("com.tansun"))
public class Auth {

	public static void main(String[] args) {
		SpringApplication.run(Auth.class, args);
	}
	
	public ResourceBundleMessageSource getMessageSource() throws Exception {
        ResourceBundleMessageSource rbms = new ResourceBundleMessageSource();
        rbms.setDefaultEncoding("UTF-8");
        rbms.setBasenames("i18n/messages");
        return rbms;
    }
}
