package com.tansun.tandata.vo.res;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 信息
 * @author wzf
 * @date 2020-06-09 16:26:22
 * @Version v1.0
 */
@Data
@ApiModel(value="信息")
public class OrgInfoTreeVO implements Serializable {
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "机构编码")
	private String orgCode;
	
	@ApiModelProperty(value = "机构名称")
	private String orgName;
	
	@ApiModelProperty(value = "上一级机构节点")
	private String parentCode;

	@ApiModelProperty(value = "机构类型")
	private Integer type;
	
	@ApiModelProperty(value="子机构")
	private List<OrgInfoTreeVO> children = new ArrayList<>();

}