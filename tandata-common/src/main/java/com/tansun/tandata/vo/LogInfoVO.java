package com.tansun.tandata.vo;

import lombok.Data;

@Data
public class LogInfoVO {
	//服务英文名
	private String serverId;
	//日志id
	private String bkey;
	//描述
	private String comment;
	//是否成功（0：否；1：是）
	private Integer success;
	//操作名称
	private String operation;
	//模块名称
	private String modular;
}
