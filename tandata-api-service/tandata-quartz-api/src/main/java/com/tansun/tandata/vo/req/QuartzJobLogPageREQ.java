package com.tansun.tandata.vo.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 分页查询任务日志
 * @author LinLaiChun
 * @date 2020-06-08 15:41:04
 * @Version v1.0
 */
@Data
@ApiModel(value="分页查询任务日志")
public class QuartzJobLogPageREQ implements Serializable {
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "搜索名称(任务名称/分组名称查询)", required = false)
	private String selectName;

	@ApiModelProperty(value = "计划任务主键", required = false)
	private String jobId;

	@ApiModelProperty(value = "执行状态(SUCCESS 成功、ERROR失败)", required = false)
	private String runStatus;

	@ApiModelProperty(value = "排序语句",required = false)
	private String orderBy;

	@ApiModelProperty(value = "当前页码",required = true)
	private Integer currentPage;

	@ApiModelProperty(value = "单页数据量",required = true)
	private Integer pageSize;
}