package com.tansun.tandata.vo.req.bi;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel(
        value = "bi建表集合对象",
        description = "bi建表集合对象"
)
@Data
public class AddBiTableListVo {
    @ApiModelProperty(
            value = "表名称",
            example = "liuydev"
    )
    private String tableName;
    @ApiModelProperty(
            value = "连接名",
            example = "ceshitishu"
    )
    private String connectionName;
}
