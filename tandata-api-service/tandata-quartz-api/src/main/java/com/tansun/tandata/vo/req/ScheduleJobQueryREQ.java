package com.tansun.tandata.vo.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 查询任务表
 * @author LinLaiChun
 * @date 2020-06-04 11:02:04
 * @Version v1.0
 */
@Data
@ApiModel(value="查询任务表")
public class ScheduleJobQueryREQ implements Serializable {
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "搜索名称(任务名称/分组名称查询)", required = false)
	private String selectName;

	@ApiModelProperty(value = "任务名称", required = false)
	private String jobName;

	@ApiModelProperty(value = "任务状态(PAUSED暂停、NORMAL运行)", required = false)
	private String jobStatus;

	@ApiModelProperty(value = "任务分组", required = false)
	private String jobGroup;

	@ApiModelProperty(value = "调用类型（0类调用1服务调用）", required = false)
	private Integer type;

	@ApiModelProperty(value = "调用服务名/调用类", required = false)
	private String beanName;

	@ApiModelProperty(value = "调用url/类中方法", required = false)
	private String methodName;

	@ApiModelProperty(value = "排序语句", required = false)
	private String orderBy;
}