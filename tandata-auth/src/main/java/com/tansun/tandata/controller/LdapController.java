package com.tansun.tandata.controller;

import com.tansun.tandata.service.LdapService;
import com.tansun.tandata.service.impl.CustomizeServiceImpl;
import com.tansun.tandata.vo.LdapUser;
import com.tansun.tjmas.cloud.base.auth.entity.CheckUserResultDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@Api(value = "Ldap接口")
@RestController
@RequestMapping(value = "/ldap")
public class LdapController {
    @Autowired
    private CustomizeServiceImpl customizeServiceImpl;
    @Autowired
    private LdapService ldapService;
    @ApiOperation(value = "测试校验账号密码", notes = "测试校验账号密码")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userName", value = "账号名", required = true, dataType = "String"),
            @ApiImplicitParam(name = "password", value = "密码", required = true, dataType = "String"),
    })
    @RequestMapping(value = "/testCheckPassword", method = RequestMethod.POST)
    public CheckUserResultDTO testCheckPassword(String username, String password){
         return customizeServiceImpl.checkPassword(username,password);
    }

    @RequestMapping(value = "/testCheckPassword2", method = RequestMethod.POST)
    public LdapUser testCheckPassword2(String username, String password){
        return ldapService.ldapAuth(username,password);
    }
    @ApiOperation(value = "根据关键字查询ldap用户列表", notes = "测试校验账号密码")
    @RequestMapping(value = "/seachLdapUserByKeywords", method = RequestMethod.POST)
    public List<LdapUser> seachLdapUserByKeywords(@Valid @RequestBody LdapUser ldapUser){
        return ldapService.seachLdapUserByKeywords(ldapUser.getCn());
    }
}
