package com.tansun.tandata.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

import com.tansun.tjdp.orm.entity.BaseEntity;

import lombok.Data;

/**
 * @Description 角色实体类
 * @CreateDate 20200426
 * @author shenxiaodong
 * @Version v1.0
 *
 */

@Entity
@Data
@Table(name="sys_role")
public class Role extends BaseEntity<Role, String> implements Serializable {

	private static final long serialVersionUID = -8034772436466915846L;

	/**
	 * 角色编号
	 */
	@Id
	@Column(name = "org_code")
	private String roleCode;

	/**
	 * 角色名称
	 */
	@Column(name = "role_name")
	private String roleName;

	/**
	 * 角色描述
	 */
	@Column(name = "role_desc")
	private String roleDesc;

	/**
	 * 创建人
	 */
	@Column(name = "creater")
	private String creater;

	/**
	 * 创建时间
	 */
	@Column(name = "create_time")
	private Date createTime;

	/**
	 * 修改人
	 */
	@Column(name = "updater")
	private String updater;

	/**
	 * 修改时间
	 */
	@Column(name = "update_time")
	private Date updateTime;

	/**
	 * 是否删除
	 */
	@Column(name = "is_delete")
	private Integer isDelete;

	/**
	 * 乐观锁
	 */
	@Column(name = "update_count")
	private Integer updateCount;
}
