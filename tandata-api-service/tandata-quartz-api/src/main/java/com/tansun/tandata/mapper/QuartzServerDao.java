package com.tansun.tandata.mapper;

import com.tansun.tandata.entity.QuartzServer;
import com.tansun.tjdp.orm.persistence.BaseDao;

import java.util.List;
import java.util.Map;

/**
 * 服务配置Dao
 * @author LinLaiChun
 * @date 2020-06-09 15:55:26
 * @Version v1.0
 */
public interface QuartzServerDao extends BaseDao<QuartzServer> {

	int insert(QuartzServer quartzServer);

	int batchInsert(List<QuartzServer> quartzServerList);

	int updateById(QuartzServer quartzServer);

	int batchUpdate(List<QuartzServer> quartzServerList);

	int deleteById(String id);

	int deleteByIds(List<String> idList);

    QuartzServer getById(String id);

	List<QuartzServer> getByIds(List<String> idList);

	List<QuartzServer> queryListByMap(Map<String, Object> queryMap);
}