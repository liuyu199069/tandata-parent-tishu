package com.tansun.tandata.apis.vo.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.util.Date;

/**
 * 接口信息配置信息
 * @author LinLaiChun
 * @date 2020-06-10 15:08:46
 * @Version v1.0
 */
@Data
@ApiModel(value="接口信息配置信息")
public class ApisInterfaceRESP implements Serializable {
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "ID")
	private String id;

	@ApiModelProperty(value = "接口名称")
	private String name;

	@ApiModelProperty(value = "接口类型（sql、http）")
	private Integer type;

	@ApiModelProperty(value = "所属主题（主题表）")
	private String subjectId;

	@ApiModelProperty(value = "所属项目")
	private String project;

	@ApiModelProperty(value = "接口路径")
	private String path;

	@ApiModelProperty(value = "请求方式 （字典表）")
	private String requestMethod;

	@ApiModelProperty(value = "协议 （字典表）")
	private String agreement;

	@ApiModelProperty(value = "返回类型 （字典表）")
	private String returnType;

	@ApiModelProperty(value = "接口描述")
	private String remark;

	@ApiModelProperty(value = "排序字段")
	private Integer sort;

	@ApiModelProperty(value = "创建者ID")
	private String creatorId;

	@ApiModelProperty(value = "创建时间")
	private Date createTime;

	@ApiModelProperty(value = "修改者ID")
	private String modifierId;

	@ApiModelProperty(value = "修改时间")
	private Date modifyTime;

}