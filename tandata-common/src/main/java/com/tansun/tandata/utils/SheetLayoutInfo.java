package com.tansun.tandata.utils;

import java.util.List;

/**
 * @Description
 * @Author will
 * @Date 2018/12/12  上午 10:52
 */
public class SheetLayoutInfo {
    /**
     * 自定义的excel列名称
     */
    private String[] headNames;
    /**
     * sheet页名称
     */
    private String sheetName;
    /**
     * 默认样式
     */
    private StyleCallBack styleCallBack;
    /**
     * 是否需要合计
     */
    private boolean hasSum;
    /**
     * 自定义合计统计
     */
    private CustomSumInfo customSumInfo;
    
    /**
     * sheet页起始页
     */
    private int sheetStart;
    
    /**
     * 填充数据起始行
     */
    private int dataStartRow;
    
    /**
     * 填充数据起始列
     */
    private int dataStartColumn;
    
    /**
     * 是否需要指定某些单元格值
     */
    private boolean hasCellValue;
    
    /**
     * 指定某些单元格值
     */
    private List<CustomCell> cellValue;


    public String[] getHeadNames() {
        return headNames;
    }

    public SheetLayoutInfo setHeadNames(String[] headNames) {
        this.headNames = headNames;
        return this;
    }

    public String getSheetName() {
        return sheetName;
    }

    public SheetLayoutInfo setSheetName(String sheetName) {
        this.sheetName = sheetName;
        return this;
    }

    public StyleCallBack getStyleCallBack() {
        return styleCallBack;
    }

    public SheetLayoutInfo setStyleCallBack(StyleCallBack styleCallBack) {
        this.styleCallBack = styleCallBack;
        return this;
    }

    public boolean isHasSum() {
        return hasSum;
    }

    public SheetLayoutInfo setHasSum(boolean hasSum) {
        this.hasSum = hasSum;
        return this;
    }

    public CustomSumInfo getCustomSumInfo() {
        return customSumInfo;
    }

    public SheetLayoutInfo setCustomSumInfo(CustomSumInfo customSumInfo) {
        this.customSumInfo = customSumInfo;
        return this;
    }

	public int getSheetStart() {
		return sheetStart;
	}

	public void setSheetStart(int sheetStart) {
		this.sheetStart = sheetStart;
	}

	public int getDataStartRow() {
		return dataStartRow;
	}

	public SheetLayoutInfo setDataStartRow(int dataStartRow) {
		this.dataStartRow = dataStartRow;
		return this;
	}

	public int getDataStartColumn() {
		return dataStartColumn;
	}

	public SheetLayoutInfo setDataStartColumn(int dataStartColumn) {
		this.dataStartColumn = dataStartColumn;
	    return this;
	}

	public boolean isHasCellValue() {
		return hasCellValue;
	}

	public SheetLayoutInfo setHasCellValue(boolean hasCellValue) {
		this.hasCellValue = hasCellValue;
		 return this;
	}

	public List<CustomCell> getCellValue() {
		return cellValue;
	}

	public SheetLayoutInfo setCellValue(List<CustomCell> cellValue) {
		this.cellValue = cellValue;
		 return this;
	}
	
}
