package com.tansun.tandata.bigdata.vo.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * @author wenzhifeng
 * @date 2020/4/16 9:56
 */
@Data
@ApiModel(value="返回流程图对象")
public class FlinkSqlFetchTaskParamRes implements Serializable {

    private static final long serialVersionUID = -7459169002784317320L;

    @ApiModelProperty(value = "流程图id")
    private Long id;
    @ApiModelProperty(value = "流程图json串")
    private String allJson;
    @ApiModelProperty(value = "流程图任务名")
    private String taskName;
    @ApiModelProperty(value = "source创建sql")
    private String sourceSql;
    @ApiModelProperty(value = "sink创建sql")
    private String sinkSql;
    @ApiModelProperty(value = "执行sql")
    private String executeSql;
    @ApiModelProperty(value = "提交任务参数json")
    private String submitJson;

}
