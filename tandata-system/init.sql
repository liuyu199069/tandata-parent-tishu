/************************************************start yangliu 2020/06/03  初始化表结构*****************************************************/
SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `user_code` varchar(100) NOT NULL COMMENT '用户编码',
  `user_name` varchar(100) DEFAULT NULL COMMENT '用户名称',
  `login_name` varchar(100) DEFAULT NULL COMMENT '登录名称',
  `user_phone` varchar(11) DEFAULT NULL COMMENT '用户手机',
  `password` varchar(50) DEFAULT NULL COMMENT '用户密码',
  `type` varchar(2) DEFAULT NULL COMMENT '用户类型(1web用户0api用户)',
  `status` int(1) DEFAULT NULL COMMENT '1激活0冻结',
  `org_code` varchar(100) DEFAULT NULL COMMENT '机构号',
  `creater` varchar(100) DEFAULT NULL COMMENT '创建人id',
  `creater_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updater` varchar(100) DEFAULT NULL COMMENT '修改人id',
  `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `is_delete` int(1) DEFAULT NULL COMMENT '是否删除',
  `update_count` int(1) DEFAULT NULL COMMENT '乐观锁',
  PRIMARY KEY (`user_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8  COMMENT='用户';

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `role_code` varchar(100) NOT NULL COMMENT '角色编号',
  `role_name` varchar(100) DEFAULT NULL COMMENT '角色名称',
  `role_desc` varchar(100) DEFAULT NULL COMMENT '角色描述',
  `creater` varchar(100) DEFAULT NULL COMMENT '创建人id',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updater` varchar(100) DEFAULT NULL COMMENT '修改者id',
  `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `is_delete` int(1) DEFAULT NULL COMMENT '是否删除',
  `update_count` int(1) DEFAULT NULL COMMENT '乐观锁',
  PRIMARY KEY (`role_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色';

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `id` varchar(100) NOT NULL COMMENT '菜单ID',
  `prnt_id` varchar(100) DEFAULT NULL COMMENT '上级菜单ID',
  `fn_chn_nm` varchar(100) DEFAULT NULL COMMENT '菜单中文名称',
  `fn_eng_nm` varchar(100) DEFAULT NULL COMMENT '菜单英文名称',
  `rsrc_no` varchar(200) DEFAULT NULL COMMENT '菜单访问路径',
  `fn_ctgr_cd` varchar(2) DEFAULT NULL COMMENT '菜单功能分类，1路由菜单，2api菜单',
  `sort` int(5) DEFAULT NULL COMMENT '排序字段',
  `creater` varchar(100) DEFAULT NULL COMMENT '创建人id',
  `create_time` date DEFAULT NULL COMMENT '创建时间',
  `updater` varchar(100) DEFAULT NULL COMMENT '修改人id',
  `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `is_delete` int(1) DEFAULT NULL COMMENT '是否删除',
  `update_count` int(1) DEFAULT NULL COMMENT '乐观锁',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='菜单';

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `user_code` varchar(100) DEFAULT NULL COMMENT '用户编号',
  `role_code` varchar(100) DEFAULT NULL COMMENT '角色编号',
  PRIMARY KEY (`id`),
  KEY `USER_CODE` (`user_code`),
  KEY `ROLE_CODE` (`role_code`)
) ENGINE=InnoDB AUTO_INCREMENT=243 DEFAULT CHARSET=utf8 COMMENT='用户角色';

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `role_code` varchar(100) DEFAULT NULL COMMENT '角色编号',
  `menu_id` varchar(100) DEFAULT NULL COMMENT '菜单ID',
  PRIMARY KEY (`id`),
  KEY `ROLE_CODE` (`role_code`)
) ENGINE=InnoDB AUTO_INCREMENT=813 DEFAULT CHARSET=utf8 COMMENT='角色菜单';

-- ----------------------------
-- Table structure for org_info
-- ----------------------------
DROP TABLE IF EXISTS `org_info`;
CREATE TABLE `org_info` (
  `org_code` varchar(100) NOT NULL COMMENT '机构编码',
  `org_name` varchar(255) DEFAULT NULL COMMENT '机构名称',
  `parent_code` varchar(255) DEFAULT NULL COMMENT '上一级机构节点',
  `type` int(1) DEFAULT NULL COMMENT '机构类型',
  `creater_id` varchar(255) DEFAULT NULL COMMENT '创建者code',
  `creater_time` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `update_id` varchar(70) DEFAULT NULL COMMENT '修改者ID',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `update_count` int(11) DEFAULT '0' COMMENT '修改次数',
  PRIMARY KEY (`org_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='机构';

-- ----------------------------
-- Table structure for log_info
-- ----------------------------
DROP TABLE IF EXISTS `log_info`;
CREATE TABLE `log_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `server_id` varchar(60) DEFAULT NULL,
  `server_name` varchar(255) DEFAULT NULL COMMENT '服务名称',
  `ip_str` varchar(40) DEFAULT NULL COMMENT 'IP',
  `comment` varchar(255) DEFAULT NULL COMMENT '描述',
  `creater` varchar(60) DEFAULT NULL,
  `creater_time` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='日志';

SET FOREIGN_KEY_CHECKS = 1;
/************************************************end wenzhifeng 2020/06/11*****************************************************/
