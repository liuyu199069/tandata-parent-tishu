package com.tansun.tandata.mapper;

import com.tansun.tandata.entity.SysUser;

public interface SysUserMapper{
	/**
	 * @Description 查询用户根据用户名
	 * @param name
	 * @return
	 */
    public SysUser getUserByName(String name);
}
