package com.tansun.tandata.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 失败信息
 * @author linlaichun
 * @date 2020-05-21 14:12:15
 * @Version v1.0
 */
@Data
@ApiModel(value="失败信息" )
public class ImportErrorInfoRESP implements Serializable {
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "行号")
	private Integer row;

	@ApiModelProperty(value = "错误信息")
	private String msg;

	@ApiModelProperty(value = "失败数据")
	private List<Object> errorData = new ArrayList<>();



}