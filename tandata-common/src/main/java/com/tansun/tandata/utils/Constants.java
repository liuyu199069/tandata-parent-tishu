package com.tansun.tandata.utils;

import java.math.BigDecimal;

public interface Constants {

	/******************************************************/
    /* Character                                          */
    /******************************************************/
    public static final String CHAR_SPACE                                       = " ";
    public static final String CHAR_BLANK                                       = "";
    public static final String CHAR_COMMA                                       = ",";
    public static final String CHAR_POINT                                       = ".";
    public static final String CHAR_BAR                                         = "-";
    public static final String CHAR_UNDERSCORE                                  = "_";
    public static final String CHAR_ASTERISK                                    = "*";
    public static final String CHAR_PERCENT                                     = "%";
    public static final String CHAR_VERTICAL_DIVIDE                             = "|";
    public static final String CHAR_AND                                         = "&&";
    public static final String CHAR_SPLIT_POINT                                 = "\\.";
    public static final Integer CHAR_INT_ZERO                                   = 0;
    public static final Integer CHAR_INT_MAX                              		= 999999999;
    public static final double DOUBLE_INT_ZERO                                  = 0.00;
    public static final double DOUBLE_INT_MAX                                  	= 99999999.00;
    public static final float FLOAT_INT_ZERO                                  	= 0.00f;
    public static final BigDecimal DECIMAL_INT_ZERO                             = new BigDecimal(0.00);
    public static final Integer CHAR_INT_ONE                                 	= 1;
    public static final Integer CHAR_INT_TWO                                 	= 2;
    public static final Integer CHAR_INT_EIGHT                                 	= 8;
    public static final String CHAR_STRING_ZERO                                 = "0";
    public static final String CHAR_PRICE_ZERO                                  = "0.00";
    public static final String CHAR_STRING_ONE                                  = "1";
    public static final String CHAR_STRING_TWO                                  = "2";
    public static final String CHAR_STRING_THREE                                = "3";
    public static final String CHAR_STRING_FOUR                                 = "4";
    public static final String CHAR_STRING_FIVE                                 = "5";
    public static final String DEFAULT_CHARSET									="UTF-8"; // 默认编码

    public static final String CHAR_SPLIT_SLASH									="/";
    public static final String EXCEL_2003_DOWN									=".xls";
    public static final String EXCEL_2007_UP									=".xlsx";

    /*频率 										          */
    /******************************************************/
    //天
    public static final String SYS_MESSAGE_RATE_TYPE_DAY                        ="DAY";
    //周
    public static final String SYS_MESSAGE_RATE_TYPE_WEEK                       ="WEEK";
    //月
    public static final String SYS_MESSAGE_RATE_TYPE_MONTH                      ="MONTH";
    //季
    public static final String SYS_MESSAGE_RATE_TYPE_SEASON                     ="SEASON";
    //年
    public static final String SYS_MESSAGE_RATE_TYPE_YEAR                       ="YEAR";
    /******************************************************/
    /*response 										      */
    /******************************************************/
	//响应code
    public static final String  HTTP_RES_CODE_NAME 								="code";
	//响应msg
    public static final String HTTP_RES_CODE_MSG 								="msg";
	//响应data
    public static final String HTTP_RES_CODE_DATA 								="data";
	//响应请求成功
    public static final String HTTP_RES_CODE_200_VALUE 							="success";
	//系统错误
    public static final String HTTP_RES_CODE_500_VALUE 							="fail";
    //数据采集次数校验不通过code
    public static final Integer HTTP_RES_CODE_101 								=101;
    //数据采集格式校验不通过code
    public static final Integer HTTP_RES_CODE_102 								=102;
    //数据采集月限制日校验不通过code
    public static final Integer HTTP_RES_CODE_103 								=103;
    //数据质量校验不通过code
    public static final Integer HTTP_RES_CODE_104 								=104;
	//响应请求成功code
    public static final Integer HTTP_RES_CODE_200 								=200;
	//响应请求成功code
    public static final Integer HTTP_RES_CODE_201 								=201;
	//响应请求成功code
    public static final Integer HTTP_RES_CODE_202 								=202;
    public static final Integer HTTP_RES_CODE_203                               =203;
    //警告提示
    public static final Integer HTTP_RES_CODE_401 								=401;//参数错误
    public static final Integer HTTP_RES_CODE_402 								=402;//存在性错误
    public static final Integer HTTP_RES_CODE_403 								=403;//多用户更新
    //系统错误
    public static final Integer HTTP_RES_CODE_500 								=500;
	//响应code
    public static final String SMS_MAIL 										="sms_mail";
	//token
    public static final String USER_TOKEN 										="userToken";
	//result
    public static final String RESULT_MSG 										="resultMsg";
    public static final String RESULT_CODE 										="resultCode";

    /**
     * 是否删除标识，逻辑删除
     */
    public static final int DELETE_FLAG_DELETED = 1;    //删除
    public static final int DELETE_FLAG_RESERVED = 0;    //保留

    public static final int DELETE_FLAG_SUCCESS = 0;
    public static final int DELETE_FLAG_FAIL = 1;
    public static final int DELETE_FLAG_UNSUPPORTED = 2;
    public static final int DELETE_DATA_NOT_EXIST = 3;

    /**
     * 数据库查询排序类型
     */
    public static final String ORDER_ASC = "ASC";    //正序
    public static final String ORDER_DESC = "DESC";    //倒序

    /**
     * 导出模板名称
     */
    public static final String TEM_TYPE_ONE = "1";
    public static final String TEM_TYPE_TWO = "2";


    //管理员用户ID
    public static final String USER_ADMIN_CODE = "ADMIN";

	/******************************************************/
    /* tandata-auth                                       */
    /******************************************************/
    public static final int REDIS_USER_TIME = 3000;    //用户信息超时时间，3000秒
    public static final String REDIS_USER_FLAG = "tjmas-redis-id";//用户固定标签
	//响应请求成功code
    public static final Integer HTTP_RES_SUCESS = 0;    //成功
    public static final Integer HTTP_RES_FIAL = 1;      //失败
	//api更新版本
    public static final String API_UPDATE_VERSION = "0";    //成功
    /***缓存变量*****/
    public static final String CHILD_ORG_REDIS = "child_org_";

	/******************************************************/
    /* tandata-sys                                       */
    /******************************************************/
    public static final int USER_STATUS_ALL = 2;     //全部
    public static final int USER_STATUS_ACTIVE = 1;    //激活
    public static final int USER_STATUS_FREEZE = 0;     //冻结
    public static final String CHAR_TREE_ZERO = "0";     //树的最大父节点的父id
    public static final int USER_UPDATE_COUNT_INIT = 0;  //乐观锁初始化
    public static final String USER_TYPE_API = "0";    //Api用户
    public static final String USER_TYPE_WEB = "1";     //Web用户
    public static final String ORG_INFO_ROOT_CODE = "0";     //机构树根节点

    /***缓存变量*****/
    public static final String USER_REDIS = "user_redis_";

    /**导出模板名称**/
    public static final String DR_PASH_COLL_VERI_M = "test";

    /******************************************************/
    /**                tandata-quartz                    **/
    /******************************************************/
    //默认key
    public static final String QUARTZ_SCHEDULE_KEY = "scheduleJob";

    //任务状态
    public static final String QUARTZ_JOB_STATUS_NORMAL = "NORMAL";//运行
    public static final String QUARTZ_JOB_STATUS_PAUSED = "PAUSED";//暂停

    //任务类型
    public static final Integer QUARTZ_JOB_TYPE_SIMPLE = 0;//简单
    public static final Integer QUARTZ_JOB_TYPE_REMOTE = 1;//远程

    //任务运行情况
    public static final String QUARTZ_JOB_RUN_STATUS_SUCCESS = "SUCCESS";//成功
    public static final String QUARTZ_JOB_RUN_STATUS_ERROR = "ERROR";//失败
    public static final String QUARTZ_JOB_RUN_STATUS_UNKNOW = "UNKNOW";//未知

    /******************************************************/
    /* tandata-tishubusiness                                       */
    /******************************************************/
    public static final int RAIS_NUM_ERRO_1=-1;//更新的情况找不到原提数记录
    public static final int RAIS_NUM_ERRO_2=-2;//延时提数时预计开始时间超过了当前时间
    public static final int RAIS_NUM_ERRO_3=-3;//调用提数脚本调用失败


}