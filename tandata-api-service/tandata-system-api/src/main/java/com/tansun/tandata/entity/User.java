package com.tansun.tandata.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;


import com.tansun.tjdp.orm.entity.BaseEntity;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @Description 用户实体类
 * @CreateDate 20200426
 * @author shenxiaodong
 * @Version v1.0
 *
 */

@Entity
@Data
@Table(name="sys_user")
public class User extends BaseEntity<User, String> implements Serializable{
	
	private static final long serialVersionUID = -2701052307289939921L;

	/**
	 * 用户编码
	 */
	@Id
	@Column(name = "user_code")
	private String userCode;

	/**
	 * 用户名称
	 */
	@Column(name = "user_name")
	private String userName;

	/**
	 * 登录名称
	 */
	@Column(name = "login_name")
	private String loginName;

	/**
	 * 用户手机
	 */
	@Column(name = "user_phone")
	private String userPhone;

	/**
	 * 用户密码
	 */
	@Column(name = "password")
	private String password;

	/**
	 * 用户类型
	 */
	@Column(name = "type")
	private String type;

	/**
	 * 用户状态
	 */
	@Column(name = "status")
	private Integer status;

	/**
	 * 机构名称
	 */
	@Column(name = "org_code")
	private String orgCode;

	/**
	 * 创建人
	 */
	@Column(name = "creater")
	private String creater;

	/**
	 * 创建时间
	 */
	@Column(name = "creater_time")
	private Date createrTime;

	/**
	 * 修改人
	 */
	@Column(name = "updater")
	private String updater;

	/**
	 * 修改时间
	 */
	@Column(name = "update_time")
	private Date updateTime;

	/**
	 * 是否删除
	 */
	@Column(name = "is_delete")
	private Integer isDelete;

	/**
	 * 乐观锁
	 */
	@Column(name = "update_count")
	private Integer updateCount;
}
