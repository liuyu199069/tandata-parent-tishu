package com.tansun.tandata.feign.config;

import org.springframework.core.env.Environment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.ldap.LdapProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.LdapContextSource;

import java.util.Collections;

/**
 * LDAP配置类
 */
@Configuration
@EnableConfigurationProperties(LdapProperties.class)
public class LdapConfig {
    @Bean
    public LdapContextSource ldapContextSource() {
        LdapContextSource source = new LdapContextSource();
        source.setUserDn(properties.getUsername());
        source.setPassword(properties.getPassword());
        source.setBase(properties.getBase());
        source.setUrls(properties.determineUrls(environment));
        source.setBaseEnvironmentProperties(Collections.unmodifiableMap(properties.getBaseEnvironment()));
        return source;
    }

    @Bean
    public LdapTemplate ldapTemplate() {
        return new LdapTemplate(ldapContextSource());
    }

    @Autowired
    private LdapProperties properties;

    @Autowired
    private Environment environment;

}
