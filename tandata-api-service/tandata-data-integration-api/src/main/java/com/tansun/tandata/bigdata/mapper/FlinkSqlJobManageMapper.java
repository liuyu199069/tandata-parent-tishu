package com.tansun.tandata.bigdata.mapper;


import com.tansun.tandata.bigdata.entity.FlinkSqlJarUploadedModel;

public interface FlinkSqlJobManageMapper {

    Integer addUploadedJar(FlinkSqlJarUploadedModel flinkSqlJarUploadedModel);
}
