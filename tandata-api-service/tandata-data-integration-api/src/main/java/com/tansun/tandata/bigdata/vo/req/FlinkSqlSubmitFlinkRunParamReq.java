package com.tansun.tandata.bigdata.vo.req;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * @author wenzhifeng
 * @date 2020/4/17 12:43
 */

@ApiModel(value="提交执行作业的请求参数",description="提交执行作业的请求参数")
@Data
public class FlinkSqlSubmitFlinkRunParamReq implements Serializable {

    private static final long serialVersionUID = 2310498272524675816L;

    @ApiModelProperty(value="上传的jar包Id")
    @NotBlank
    private String jarId;
    @ApiModelProperty(value="自定义的作业名称")
    @NotBlank
    private String thisJobName;

    @ApiModelProperty(value="提交任务参数的json串")
    @NotBlank
    private String jobSubmitJson;
    @ApiModelProperty(value="创建source的sql")
    @NotBlank
    private String createSourceSql;
    @ApiModelProperty(value="创建sink的sql")
    @NotBlank
    private String createSinkSql;
    @ApiModelProperty(value="执行的sql语句")
    @NotBlank
    private String tableExecuteSql;
}
