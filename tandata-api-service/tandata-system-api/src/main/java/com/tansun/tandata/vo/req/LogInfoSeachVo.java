package com.tansun.tandata.vo.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
@ApiModel(
        value = "操作日志查询对象",
        description = "操作日志查询对象"
)
@Data
public class LogInfoSeachVo implements Serializable {
    @ApiModelProperty(
            value = "当前页",
            example = "1"
    )
    private int page;
    @ApiModelProperty(
            value = "显示数量",
            example = "30"
    )
    private int limit;
    @ApiModelProperty(
            value = "是否成功",
            example = "1"
    )
    private Integer success;
    @ApiModelProperty(
            value = "操作时间开始",
            example = "2020-09-30 09:08:05"
    )
    private String operationTime_beg;
    @ApiModelProperty(
            value = "操作时间结束",
            example = "2020-09-30 12:08:05"
    )
    private String operationTime_end;
    @ApiModelProperty(
            value = "查询关键字",
            example = "2020-09-30 12:08:05"
    )
    private String seachKeyWord;


}
