package com.tansun.tandata.vo.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @Description 用于用户修改密码实体类入参使用
 * @CreateDate 20200427
 * @author shenxiaodong
 * @Version v1.0
 *
 */

@ApiModel(value="传入修改密码对象",description="传入修改密码对象")
@Data
public class UserChangePwdVO implements Serializable{

	private static final long serialVersionUID = 2664298054437501776L;

	@ApiModelProperty(value="用户编码", name="userCode")
	@NotBlank
	private String userCode;

	@ApiModelProperty(value="用户原密码", name="oldPassword")
	@NotNull(message="{sys.CompanyVO.companySocialCode.notNullError}")
	private String oldPassword;

	@ApiModelProperty(value="用户新密码", name="newPassword")
	@NotBlank
	private String newPassword;

}
