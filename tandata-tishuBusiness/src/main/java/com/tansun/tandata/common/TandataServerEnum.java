package com.tansun.tandata.common;

import java.util.HashMap;
import java.util.Map;

public enum TandataServerEnum {
	One("tandata-sys","系统服务"),Two("tandata-quartz","队列服务"),Three("tandata-auth","认证服务"),Four("tandata-asset","数据资产服务"),Five("tandata-tishuussiness","提数业务服务");;
    private String serverId;
    
    private String serverName;
    
    public String getServerId() {
        return serverId;
    }
    
    public String getServerName() {
        return serverName;
    }
    
    private TandataServerEnum(String serverId, String serverName){
        this.serverId = serverId;
        this.serverName = serverName;
    }
    
    public static final Map<String,String> map = new HashMap<String,String>();
    
    //通过number获取对应的name
    public static String getDescription(String serverId){
        return map.get(serverId);
    }
    
    static{
        for(TandataServerEnum tandataServerEnum : TandataServerEnum.values()){
            map.put(tandataServerEnum.getServerId(), tandataServerEnum.getServerName());
        }
    }
}
