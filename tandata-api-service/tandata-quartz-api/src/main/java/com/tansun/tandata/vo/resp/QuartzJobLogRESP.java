package com.tansun.tandata.vo.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.util.Date;

/**
 * 任务日志信息
 * @author LinLaiChun
 * @date 2020-06-08 15:41:04
 * @Version v1.0
 */
@Data
@ApiModel(value="任务日志信息")
public class QuartzJobLogRESP implements Serializable {
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "主键")
	private String id;

	@ApiModelProperty(value = "计划任务主键")
	private String jobId;

	@ApiModelProperty(value = "任务名称")
	private String jobName;

	@ApiModelProperty(value = "任务分组")
	private String jobGroup;

	@ApiModelProperty(value = "描述")
	private String desc;

	@ApiModelProperty(value = "开始")
	private Date startTime;

	@ApiModelProperty(value = "任务结束时间")
	private Date endTime;

	@ApiModelProperty(value = "创建人")
	private String creator;

	@ApiModelProperty(value = "执行状态(SUCCESS 成功、ERROR失败)")
	private String runStatus;

}