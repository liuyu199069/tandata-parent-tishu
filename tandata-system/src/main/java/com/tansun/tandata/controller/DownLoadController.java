package com.tansun.tandata.controller;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.util.IOUtils;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.tansun.tandata.service.UserService;
import com.tansun.tandata.utils.Constants;
import com.tansun.tandata.utils.DateUtils;
import com.tansun.tandata.utils.ExcelUtils;
import com.tansun.tandata.utils.FileUtils;
import com.tansun.tandata.utils.ParentController;
import com.tansun.tandata.utils.ResponseBase;
import com.tansun.tandata.utils.SheetLayoutInfo;
import com.tansun.tandata.vo.res.UserRoleVO;

import io.swagger.annotations.Api;

/**
 * @Description 用户信息控制器

 * @CreateDate 20200426
 * @author shenxiaodong
 * @Version v1.0
 *
 */
@Api(value = "用户接口")
@RestController
@RequestMapping(value = "/sys/user")
public class DownLoadController extends ParentController{
	
	private org.slf4j.Logger logger=LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private UserService userService;
	
	@Value("${upload.suffixs}")
	private String uploadSuffixs;
	@Value("${upload.path}")
	private String uploadFilePath;
	@Value("${upload.size}")
	private String uploadFileSize;
	@Value("${upload.unit}")
	private String fileUnit;
	
	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	public ResponseBase upload(@RequestParam("file") MultipartFile[] files,HttpServletRequest request, HttpServletResponse response) {
		return FileUtils.uploadFile(files, uploadSuffixs, uploadFilePath, Integer.valueOf(uploadFileSize) ,fileUnit, localeMessage);
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(value = "/downLoadExcel")
	public ResponseBase downLoadExcel(HttpServletRequest request,HttpServletResponse response) {
		
		//检索你需要加载的数据，如果又参数则入参过滤
		List<UserRoleVO> userList = userService.selectAllUser();
		List<Map<String,Object>> resultList = new ArrayList<>();
		for (UserRoleVO data : userList) {
			HashMap<String, Object> map = new LinkedHashMap<>();
			map.put("userCode", data.getUserCode());
			map.put("userName", data.getUserName());
			resultList.add(map);
		}
		InputStream in = null;
		try {
			String fileName = Constants.DR_PASH_COLL_VERI_M+ExcelUtils.EXCEL_07_SUFFIX;
			SheetLayoutInfo layoutInfo = new SheetLayoutInfo();
//			List<CustomCell> cellList = new ArrayList<>();
//			CustomCell etlDt = new CustomCell(1, 0, getEtlDt(vo.getEtlDt()));
//			CustomCell region = new CustomCell(2, 1,vo.getOrgName()+Constants.ORG_NAME_ALIAS);
//			cellList.add(etlDt);
//			cellList.add(region);
//          layoutInfo.setDataStartRow(4).setDataStartColumn(2).setHasCellValue(true).setCellValue(cellList);
            layoutInfo.setDataStartRow(1).setDataStartColumn(0);
            in = ExcelUtils.createTempAndStream(Constants.TEM_TYPE_TWO,fileName,new ArrayList() {{
                add(layoutInfo);
            }}, new ArrayList() {{
                add(resultList);
            }});
			ExcelUtils.downloadFile(in, generateRptName(Constants.DR_PASH_COLL_VERI_M, DateUtils.formatDate(DateUtils.DATE_TO_STRING_SHORT_PATTERN, new Date())), response);
			return null;
		} catch (Exception e) {
			return setResultError(e.getMessage());
		} finally{
			IOUtils.closeQuietly(in);
		}
	}
	
	/**
	 * @param rptNm
	 * @param bizDate
	 * @return
	 */
	private String generateRptName(String rptNm, String bizDate) {
        if (StringUtils.isNotBlank(bizDate)) {
            return rptNm + "_" + bizDate + ExcelUtils.EXCEL_07_SUFFIX;
        } else {
            return rptNm + ExcelUtils.EXCEL_07_SUFFIX;
        }
	}
}