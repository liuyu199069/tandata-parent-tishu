package com.tansun.tandata.apis.vo.req;

import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value="运行主题接口")
public class RunSqlVO implements Serializable {
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "ID", required = false)
	private String id;

	@ApiModelProperty(value = "参数名称", required = false)
	private List<String> parameters ;
}