package com.tansun.tandata.mapper;


import com.tansun.tandata.entity.UserMeter;
import com.tansun.tandata.vo.req.UserMeterPageVO;
import com.tansun.tjdp.orm.persistence.BaseDao;

import java.util.List;

public interface UserMeterDao extends BaseDao<UserMeter> {
    int deleteByPrimaryKey(String id);

    int insert(UserMeter record);

    int insertSelective(UserMeter record);

    UserMeter selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(UserMeter record);

    int updateByPrimaryKey(UserMeter record);

    List<String> selectUserMeterByUsercode(String raiseUsercode);

    int merageUserMeter(UserMeter userMeter);

    int insertBath(List<UserMeter> list);

    List<UserMeter> selectUserMeterList(UserMeterPageVO userMeterPageVO);

}