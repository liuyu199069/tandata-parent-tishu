package com.tansun.tandata.bigdata.entity;


import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author wenzhifeng
 * @date 2020/4/15 16:12
 */
@Data
public class FlinkSqlTaskParamModel implements Serializable {

    private static final long serialVersionUID = 3091893231751835976L;

    public FlinkSqlTaskParamModel() {
    }

    public FlinkSqlTaskParamModel(String allJson) {
        this.allJson = allJson;
    }

    private Long id;
    private String allJson;
    private String taskName;
    private String sourceSql;
    private String sinkSql;
    private String executeSql;
    private String submitJson;
    private Integer flag;
    private String createBy;
    private Date createTime;
    private String updateBy;
    private Date updateTime;

}
