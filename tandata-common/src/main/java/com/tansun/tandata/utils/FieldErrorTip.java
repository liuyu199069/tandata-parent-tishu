package com.tansun.tandata.utils;

import lombok.Getter;
import lombok.Setter;
/**
 * 表单提交错误字段提示
 */
@Setter
@Getter
public class FieldErrorTip {
	
	private String label;
	private String msg;
	
	public FieldErrorTip(String label,String msg) {
		this.label = label;
		this.msg = msg;
	}
	
}
