package com.tansun.tandata.fallback;

import com.tansun.tandata.entity.LogInfoErro;
import com.tansun.tandata.feign.AuthFeignService;
import com.tansun.tandata.feign.SysFeignService;
import com.tansun.tandata.utils.LocaleMessage;
import com.tansun.tandata.utils.ParentController;
import com.tansun.tandata.utils.ResponseBase;
import com.tansun.tandata.vo.LdapUser;
import com.tansun.tandata.vo.LogInfoVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Component
public class AuthFeignClientFallback extends ParentController implements AuthFeignService {

	@Autowired
	private LocaleMessage localeMessage;

	@Override
	public List<LdapUser> seachLdapUserByKeywords(@RequestBody LdapUser ldapUser) {
		return new ArrayList<>();
	}
}
