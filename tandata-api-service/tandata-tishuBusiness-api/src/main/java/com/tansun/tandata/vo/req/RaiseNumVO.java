package com.tansun.tandata.vo.req;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@ApiModel(
        value = "传入RaiseNum对象",
        description = "提数记录对象RaiseNum"
)
public class RaiseNumVO implements Serializable {
    @ApiModelProperty(
            value = "ID(编辑操作时传入)",
            example = "uuid"
    )
    private String id;
    @ApiModelProperty(
            value = "标题",
            required = true,
            example = "测试提数1"
    )
    @NotNull(message="{sys.CompanyVO.companySocialCode.notNullError}")
    private String title;
    @ApiModelProperty(
            value = "接收人字符串",
            example = "testUser1,测试用户1;testUser2,测试用户2"
    )
    @NotNull(message="{sys.CompanyVO.companySocialCode.notNullError}")
    private String reciiveStr;
    @ApiModelProperty(
            value = "sql报文",
            example = "select * from T1",
            required = true
    )
    @NotNull(message="{sys.CompanyVO.companySocialCode.notNullError}")
    private String sqlText;
    @ApiModelProperty(
            value = "提数开始时间",
            example = "2020-09-30 09:08:05"
    )
    private String beginTime;
    @ApiModelProperty(
            value = "是否立即提数（1：是；0否）",
            example = "1"
    )
    @NotNull(message="{sys.CompanyVO.companySocialCode.notNullError}")
    private Integer raiseNow;
    @ApiModelProperty(
            value = "结果表表名",
            example = "T1",
            required = true
    )
    @NotNull(message="{sys.CompanyVO.companySocialCode.notNullError}")
    private String tableName;
    @ApiModelProperty(
            value = "可查看天数",
            example = "30"
    )
    private Integer seeDate;
    @ApiModelProperty(
            value = "数据保存天数",
            example = "90"
    )
    private Integer saveDate;
    @ApiModelProperty(
            value = "是否提交（0：否；1：是）",
            example = "1",
            required = true
    )
    @NotNull(message="{sys.CompanyVO.companySocialCode.notNullError}")
    private Integer submitFlag;

    public RaiseNumVO() {
    }

    public String getId() {
        return this.id;
    }

    public String getTitle() {
        return this.title;
    }

    public String getReciiveStr() {
        return this.reciiveStr;
    }

    public String getSqlText() {
        return this.sqlText;
    }

    public String getBeginTime() {
        return this.beginTime;
    }

    public Integer getRaiseNow() {
        return this.raiseNow;
    }

    public String getTableName() {
        return this.tableName;
    }

    public Integer getSeeDate() {
        return this.seeDate;
    }

    public Integer getSaveDate() {
        return this.saveDate;
    }

    public Integer getSubmitFlag() {
        return this.submitFlag;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setReciiveStr(String reciiveStr) {
        this.reciiveStr = reciiveStr;
    }

    public void setSqlText(String sqlText) {
        this.sqlText = sqlText;
    }

    public void setBeginTime(String beginTime) {
        this.beginTime = beginTime;
    }

    public void setRaiseNow(Integer raiseNow) {
        this.raiseNow = raiseNow;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public void setSeeDate(Integer seeDate) {
        this.seeDate = seeDate;
    }

    public void setSaveDate(Integer saveDate) {
        this.saveDate = saveDate;
    }

    public void setSubmitFlag(Integer submitFlag) {
        this.submitFlag = submitFlag;
    }
}
