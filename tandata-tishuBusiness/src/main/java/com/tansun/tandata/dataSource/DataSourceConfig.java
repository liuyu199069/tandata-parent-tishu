package com.tansun.tandata.dataSource;

import com.alibaba.druid.pool.DruidDataSource;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import javax.sql.DataSource;

/**
 * 
 * @Description 数据源(开启事务支持后，然后在访问数据库的Service方法上添加注解 @Transactional 便可)。
 * @CreateDate 20200425
 * @author yangliu
 * @Version v1.0
 *
 */
@Configuration
@MapperScan(basePackages = "com.tansun.tandata.mapper", sqlSessionFactoryRef="sysSqlSessionFactory")
public class DataSourceConfig {
	
	@Autowired
	private Environment env;
	
	@Bean(name="sysDataSource")
	public DataSource sysDataSource() {
		String prefix = "spring.datasource.";
		DruidDataSource dds = new DruidDataSource();
		dds.setUrl(env.getProperty(prefix + "url"));
		dds.setUsername(env.getProperty(prefix + "username"));
		dds.setPassword(env.getProperty(prefix + "password"));
		dds.setDriverClassName(env.getProperty(prefix + "driverClassName"));
		dds.setInitialSize(env.getProperty(prefix + "initialSize", Integer.class));
		dds.setMaxActive(env.getProperty(prefix + "maxActive", Integer.class));
		dds.setMinIdle(env.getProperty(prefix + "minIdle", Integer.class));
		dds.setMaxWait(env.getProperty(prefix + "maxWait", Integer.class));
		
		dds.setPoolPreparedStatements(env.getProperty(prefix + "poolPreparedStatements", Boolean.class));
		dds.setMaxPoolPreparedStatementPerConnectionSize(env.getProperty(prefix + "maxPoolPreparedStatementPerConnectionSize", Integer.class));
		dds.setValidationQuery(env.getProperty(prefix + "validationQuery"));
		dds.setTestOnBorrow(env.getProperty(prefix + "testOnBorrow", Boolean.class));
		dds.setTestOnReturn(env.getProperty(prefix + "testOnReturn", Boolean.class));
		dds.setTestWhileIdle(env.getProperty(prefix + "testWhileIdle", Boolean.class));
		dds.setTimeBetweenEvictionRunsMillis(env.getProperty(prefix + "timeBetweenEvictionRunsMillis", Integer.class));
		dds.setRemoveAbandoned(env.getProperty(prefix + "removeAbandoned", Boolean.class));
		dds.setRemoveAbandonedTimeout(env.getProperty(prefix + "removeAbandonedTimeout", Integer.class));
		dds.setMinEvictableIdleTimeMillis(env.getProperty(prefix + "minEvictableIdleTimeMillis", Integer.class));
		try {
			dds.setFilters(env.getProperty(prefix + "filters"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		dds.setConnectionProperties(env.getProperty(prefix + "connectionProperties"));
		return dds;
	}
	
	@Bean(name = "sysSqlSessionFactory")
	public SqlSessionFactory sysSqlSessionFactory(@Qualifier("sysDataSource") DataSource sysDataSource) throws Exception{
		SqlSessionFactoryBean sessionFactoryBean = new SqlSessionFactoryBean();
		sessionFactoryBean.setDataSource(sysDataSource);
		PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
        sessionFactoryBean.setMapperLocations(resolver.getResources(env.getProperty("mybatis.mapper-locations")));//*Mapper.xml位置
        sessionFactoryBean.setConfigLocation(resolver.getResource(env.getProperty("mybatis.config-location")));//设置mybatis-config.xml配置文件位置           
		return sessionFactoryBean.getObject();
	}
	
	@Bean(name = "sysSqlSessionTemplate")
    public SqlSessionTemplate sysSqlSessionTemplate(@Qualifier("sysSqlSessionFactory") SqlSessionFactory sysSqlSessionFactory) throws Exception {
        SqlSessionTemplate template = new SqlSessionTemplate(sysSqlSessionFactory); // 使用上面配置的Factory
        return template;
    }
	
	@Bean(name = "transactionManager")
    public DataSourceTransactionManager transactionManager(@Qualifier("sysDataSource") DataSource sysDataSource) throws Exception {
        return new DataSourceTransactionManager(sysDataSource);
    }
}