package com.tansun.tandata.utils;

import com.alibaba.fastjson.JSONObject;
import com.tansun.tandata.vo.UserInfo;
import com.tansun.tjdp.orm.entity.BaseEntity;
import com.tansun.tjdp.orm.persistence.BaseDao;
import com.tansun.tjdp.orm.service.BaseService;
import com.tansun.tjmas.cloud.base.redis.common.RedisConstant;
import com.tansun.tjmas.cloud.base.redis.utils.RedisUtils;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;

/**
 * @description: 业务实现父类
 * @author: linLaiChun
 * @create: 2020-05-13
 * @Version v1.0
 **/
public abstract class ParentServiceImp <D extends BaseDao<T>, T extends BaseEntity> extends BaseService<D, T> {
    protected org.slf4j.Logger logger= LoggerFactory.getLogger(this.getClass());

    @Autowired
    protected RedisUtils redisUtils;

    @Autowired
    protected HttpServletRequest currentRequest;


    /**
     * 获取当前登录用户ID
     * @return
     */
    protected String getCurrUserId(){
        UserInfo user = getCurrUser();
        return user.getUserCode();
    }

    /**
     * 获取当前登录用户详情
     * @return
     */
    protected UserInfo getCurrUser(){
        String redisId = currentRequest.getHeader(RedisConstant.REDIS_ID);
        HashMap<String, Object> o = (HashMap<String, Object>) redisUtils.get(redisId);
        if(o == null || o.get("userInfo")==null){
            return new UserInfo();
        }
        UserInfo user = JSONObject.parseObject(o.get("userInfo").toString(), UserInfo.class);
        return user;
    }

    /**
     * 通过ID获取用户名
     * @param userCode
     * @return
     */
    protected String getUserNameById(String userCode){
        Object obj = redisUtils.get(Constants.USER_REDIS + userCode);
        if(obj==null){
            return Constants.CHAR_BLANK;
        }
        return (String) obj;
    }

}
