package com.tansun.tandata.apis.mapper;

import com.tansun.tandata.apis.entity.ApisSubjectSql;
import com.tansun.tjdp.orm.persistence.BaseDao;

import java.util.List;
import java.util.Map;

/**
 * Dao
 * @author LinLaiChun
 * @date 2020-06-10 14:36:59
 * @Version v1.0
 */
public interface ApisSubjectSqlDao extends BaseDao<ApisSubjectSql> {

	int insert(ApisSubjectSql apisSubjectSql);

	int batchInsert(List<ApisSubjectSql> apisSubjectSqlList);

	int updateById(ApisSubjectSql apisSubjectSql);

	int batchUpdateTraditional(List<ApisSubjectSql> apisSubjectSqlList);

	int batchUpdate(List<ApisSubjectSql> apisSubjectSqlList);

	int deleteById(String id);

	int deleteBySubjectId(String subjectId);

	int deleteByIds(List<String> idList);

    ApisSubjectSql getById(String id);

	ApisSubjectSql getBySubjectId(String subjectId);

	List<ApisSubjectSql> getByIds(List<String> idList);

	List<ApisSubjectSql> queryListByMap(Map<String, Object> queryMap);
}