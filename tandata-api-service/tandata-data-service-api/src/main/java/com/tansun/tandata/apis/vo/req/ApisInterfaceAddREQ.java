package com.tansun.tandata.apis.vo.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.util.Date;

/**
 * 新增接口信息配置
 * @author LinLaiChun
 * @date 2020-06-10 15:08:46
 * @Version v1.0
 */
@Data
@ApiModel(value="新增接口信息配置")
public class ApisInterfaceAddREQ implements Serializable {
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "ID", required = false)
	private String id;

	@ApiModelProperty(value = "接口名称", required = false)
	private String name;

	@ApiModelProperty(value = "接口类型（sql、http）", required = false)
	private Integer type;

	@ApiModelProperty(value = "所属主题（主题表）", required = false)
	private String subjectId;

	@ApiModelProperty(value = "所属项目", required = false)
	private String project;

	@ApiModelProperty(value = "接口路径", required = false)
	private String path;

	@ApiModelProperty(value = "请求方式 （字典表）", required = false)
	private String requestMethod;

	@ApiModelProperty(value = "协议 （字典表）", required = false)
	private String agreement;

	@ApiModelProperty(value = "返回类型 （字典表）", required = false)
	private String returnType;

	@ApiModelProperty(value = "接口描述", required = false)
	private String remark;

	@ApiModelProperty(value = "排序字段", required = false)
	private Integer sort;

	@ApiModelProperty(value = "创建者ID", required = false)
	private String creatorId;

	@ApiModelProperty(value = "创建时间", required = false)
	private Date createTime;

	@ApiModelProperty(value = "修改者ID", required = false)
	private String modifierId;

	@ApiModelProperty(value = "修改时间", required = false)
	private Date modifyTime;

}