package com.tansun.tandata.entity;

import com.tansun.tjdp.orm.entity.BaseEntity;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * 任务日志表
 * @author LinLaiChun
 * @date 2020-06-08 15:41:04
 * @Version v1.0
 */
@Entity
@Data
@Table(name="quartz_job_log")
public class QuartzJobLog extends BaseEntity<QuartzJobLog, String> implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	 * 主键
	 */
	@Id
	@Column(name = "id")
	private String id;

	/**
	 * 计划任务主键
	 */
	@Column(name = "job_id")
	private String jobId;

	/**
	 * 任务名称
	 */
	private String jobName;

	/**
	 * 任务分组
	 */
	private String jobGroup;

	/**
	 * 描述
	 */
	@Column(name = "desc")
	private String desc;

	/**
	 * 开始
	 */
	@Column(name = "start_time")
	private Date startTime;

	/**
	 * 任务结束时间
	 */
	@Column(name = "end_time")
	private Date endTime;

	/**
	 * 创建人
	 */
	@Column(name = "creator")
	private String creator;

	/**
	 * 执行状态(SUCCESS 成功、ERROR失败)
	 */
	@Column(name = "run_status")
	private String runStatus;




}