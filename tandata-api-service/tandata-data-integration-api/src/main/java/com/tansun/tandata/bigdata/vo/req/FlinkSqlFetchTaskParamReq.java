package com.tansun.tandata.bigdata.vo.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author wenzhifeng
 * @date 2020/4/16 9:56
 */
@Data
@ApiModel(value="条件查询流程图列表")
public class FlinkSqlFetchTaskParamReq implements Serializable {

    private static final long serialVersionUID = -2962326324994445034L;

    @ApiModelProperty(value = "第几页")
    @NotNull
    private int page;

    @ApiModelProperty(value = "每页的条目数")
    @NotNull
    private int limit;

}
