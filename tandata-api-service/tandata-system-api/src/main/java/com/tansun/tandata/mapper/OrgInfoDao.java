package com.tansun.tandata.mapper;

import com.tansun.tandata.entity.OrgInfo;
import com.tansun.tjdp.orm.persistence.BaseDao;

import java.util.List;
import java.util.Map;

/**
 * Dao
 * @author wzf
 * @date 2020-06-09 16:26:22
 * @Version v1.0
 */
public interface OrgInfoDao extends BaseDao<OrgInfo> {

	/**
	 * 新增机构
	 */
	int insert(OrgInfo orgInfo);

	/**
	 * 新增机构
	 */
	int batchInsert(List<OrgInfo> orgInfoList);

	/**
	 * 更新机构
	 */
	void updateByOrgCode(OrgInfo orgInfo);

	/**
	 * 删除机构
	 */
	void deleteByOrgCode(String orgCode);

	/**
	 * 获取机构
	 */
    OrgInfo getByOrgCode(String orgCode);

	/**
	 * 根据机构名查询机构
	 */
	int getOrgInfoByOrgName(String orgName);

	/**
	 * 根据机构名和编码查询机构
	 */
	int getOrgInfoByOrgNameAndCode(Map<String, Object> params);

	/**
	 * 根据机构编号查询子机构
	 */
	List<OrgInfo> selectAllOrg();
}