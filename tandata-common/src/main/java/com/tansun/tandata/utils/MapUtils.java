package com.tansun.tandata.utils;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.*;

/**
 * Map转换工具类
 * @author dyc
 * @date 2019年7月17日
 */
public class MapUtils {

    public static Map<String,Object> objectToMap(Object obj,String[] excludes){
        if(obj == null){
            return null;
        }
        //过滤排除项
        List<String> excList = null;
        if(excludes != null && excludes.length > 0){
            excList = Arrays.asList(excludes);
        }
        Map<String,Object> map = new HashMap<String,Object>();
        try {
            BeanInfo beanInfo = Introspector.getBeanInfo(obj.getClass());
            PropertyDescriptor[] descriptors = beanInfo.getPropertyDescriptors();
            for (PropertyDescriptor property : descriptors) {
                String key = property.getName();
                if(key.compareToIgnoreCase("class") == 0){
                    continue;
                }
               if(excList != null && excList.contains(key)){
                   continue;
               }
                Method getter = property.getReadMethod();
                Object value = getter != null ? getter.invoke(obj) : null;
                if(value != null){
                    map.put(key, value);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return map;
    }


    public static List<Map<String,Object>> objectToMap(List<Object> objList,String[] excludes){
        List<Map<String,Object>> mapList = new ArrayList<>();
        if(objList==null){
            return null;
        }else if(objList.isEmpty()){
            return mapList;
        }
        for(Object obj:objList){
            mapList.add(objectToMap(obj,excludes));
        }
        return mapList;
    }

    public static <T> List<T> transMap(Class<T> tClass, List<Map> mapList) throws Exception {
        List<T> tList = new ArrayList<>();
        for(Map map:mapList){
            tList.add(transMap(tClass, map));
        }
        return tList;
    }
    
    /**
     * Map转换为对象
     * @param tClass
     * @param map
     * @return
     * @throws Exception
     */
    public static <T> T transMap(Class<T> tClass, Map map) throws Exception {
        T t = tClass.newInstance();    //实例化对象
        BeanInfo info = Introspector.getBeanInfo(tClass); //获取类中属性
        PropertyDescriptor[] propertyDescriptors = info.getPropertyDescriptors();
        for(PropertyDescriptor pro : propertyDescriptors){
            String proName = pro.getName();
            Class properType = pro.getPropertyType();
            if(map.containsKey(proName)){
                Object methodName = map.get(proName);
                Object[] args = new Object[1] ;
                if(properType.getSimpleName().indexOf("Date") > -1){
                    args[0] = DateUtils.stringToDate(methodName.toString(), "yyyy-MM-dd'T'HH:mm:ss") ;
                }else if(properType.getSimpleName().indexOf("BigDecimal") > -1){
                    args[0] = new BigDecimal(methodName.toString());
                } else {
                    args[0] = methodName;
                }
                try{
                    pro.getWriteMethod().invoke(t, args);
                }catch(Exception ex){
                   throw ex;
                }
            }
        }
        return t;
    }
}
