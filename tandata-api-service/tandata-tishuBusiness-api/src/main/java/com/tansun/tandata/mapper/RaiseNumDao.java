
package com.tansun.tandata.mapper;

import com.tansun.tandata.entity.RaiseNum;
import com.tansun.tandata.vo.req.RaiseNumPageVO;
import com.tansun.tjdp.orm.persistence.BaseDao;

import java.util.Date;
import java.util.List;

public interface RaiseNumDao extends BaseDao<RaiseNum> {
    int deleteByPrimaryKey(String var1);

    int insert(RaiseNum var1);

    int insertSelective(RaiseNum var1);

    RaiseNum selectByPrimaryKey(String var1);

    int updateByPrimaryKeySelective(RaiseNum var1);

    int updateByPrimaryKey(RaiseNum var1);

    int getTodayItemSize();

    List<RaiseNum> getRaiseNum(RaiseNumPageVO var1);

    List<RaiseNum> selectReciivetBylimit();

    List<RaiseNum> selectRaisNumByExecShell(Date time);

    int updateRaisNumStateByTime(Date time);

    List<RaiseNum> selectRaisNumOutSaveDate(Date time);



}
