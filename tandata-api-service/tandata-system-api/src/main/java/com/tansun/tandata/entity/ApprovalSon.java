package com.tansun.tandata.entity;

import com.tansun.tjdp.orm.entity.BaseEntity;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Data
@Table(
        name = "SYS_APPROVAL_SON"
)
public class ApprovalSon extends BaseEntity<ApprovalSon,String> implements Serializable {
    private static final long serialVersionUID = -2701052307289939921L;
    /**
     * ID
     */
    @Id
    @Column(name = "ID")
    private String id;
    /**
     * 业务人员域账号
     */
    @Column(name = "SALES_NAME")
    private String sales_name;
    /**
     * 审批人
     */
    @Column(name = "APPROVAL")
    private String approval;

}
