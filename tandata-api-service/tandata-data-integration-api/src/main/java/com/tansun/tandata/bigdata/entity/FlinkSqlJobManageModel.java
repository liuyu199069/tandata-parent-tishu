package com.tansun.tandata.bigdata.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class FlinkSqlJobManageModel implements Serializable {

    private static final long serialVersionUID = -8447991962925828630L;

    private String jid;
    private String name;
    private String state;
    private Integer startTime;
    private Integer endTime;
    private Integer duration;

    //TODO 其他属性

}
