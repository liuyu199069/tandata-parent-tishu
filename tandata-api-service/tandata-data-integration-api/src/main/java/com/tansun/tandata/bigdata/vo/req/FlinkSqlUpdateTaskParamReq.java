package com.tansun.tandata.bigdata.vo.req;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author wenzhifeng
 * @date 2020/4/16 10:28
 */
@ApiModel(value="修改流程图对象",description="修改流程图对象")
@Data
public class FlinkSqlUpdateTaskParamReq implements Serializable {

    private static final long serialVersionUID = 9039641607680571522L;

    @ApiModelProperty(value="用户编码", name="userCode")
    @NotNull
    private Long id;

    @ApiModelProperty(value="流程图的json串", name="allJson")
    @NotBlank
    private String allJson;

}
