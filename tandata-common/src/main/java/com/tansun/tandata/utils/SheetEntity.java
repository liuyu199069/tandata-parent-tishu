package com.tansun.tandata.utils;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;

import java.util.List;
import java.util.Map;

public class SheetEntity {
	/**
	 * 工作簿名称
	 */
	private String name;
	/**
	 * 工作簿序号
	 */
	private Integer index;
	/**
	 * 存在标注的起始行
	 */
	private Integer startRow;
	/**
	 * 存在标注的起始行高度
	 */
	private Short startRowHeigh;
	/**
	 * 标注单元格集合
	 */
	private Map<String, Cell> pointCellMap;
	/**
	 * 标注信息实例集合
	 */
	private List<CellEntity> pointList;

	public Short getStartRowHeigh() {
		return startRowHeigh;
	}

	public void setStartRowHeigh(Short startRowHeigh) {
		this.startRowHeigh = startRowHeigh;
	}

	public Integer getStartRow() {
		return startRow;
	}

	public void setStartRow(Integer startRow) {
		this.startRow = startRow;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getIndex() {
		return index;
	}

	public void setIndex(Integer index) {
		this.index = index;
	}

	public Map<String, Cell> getPointCellMap() {
		return pointCellMap;
	}

	public void setPointCellMap(Map<String, Cell> pointCellMap) {
		this.pointCellMap = pointCellMap;
	}

	public List<CellEntity> getPointList() {
		return pointList;
	}

	public void setPointList(List<CellEntity> pointList) {
		this.pointList = pointList;
	}


	public static class CellEntity{
		/**
		 * 单元格内容值
		 */
		private String value;
		/**
		 * 行号
		 */
		private Integer rowIndex;
		/**
		 * 行内单元格序号
		 */
		private Integer cellIndex;
		/**
		 * 样式
		 */
		private CellStyle cellStyle;

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}

		public Integer getRowIndex() {
			return rowIndex;
		}

		public void setRowIndex(Integer rowIndex) {
			this.rowIndex = rowIndex;
		}

		public Integer getCellIndex() {
			return cellIndex;
		}

		public void setCellIndex(Integer cellIndex) {
			this.cellIndex = cellIndex;
		}

		public CellStyle getCellStyle() {
			return cellStyle;
		}

		public void setCellStyle(CellStyle cellStyle) {
			this.cellStyle = cellStyle;
		}
	}
}
