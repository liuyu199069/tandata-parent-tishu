package com.tansun.tandata.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.tansun.tjdp.orm.entity.BaseEntity;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Entity
@Data
@Table(
        name = "SYS_APPROVAL"
)
public class Approval extends BaseEntity<Approval,String> implements Serializable {
    private static final long serialVersionUID = -2701052307289939921L;
    /**
     * ID
     */
    @Id
    @Column(name = "ID")
    private String id;
    /**
     * 业务人员域账号
     */
    @Column(name = "SALES_NAME")
    private String sales_name;
    /**
     * 业务人员域账号姓名
     */
    @Column(name = "SALES_NICK_NAME")
    private String sales_nick_name;
    /**
     * 审批人
     */
    @Column(name = "APPROVAL")
    private String approval;
    /**
     * 审批人姓名
     */
    @Column(name = "APPROVAL_NICK_NAME")
    private String approval_nick_name;
    /**
     * 创建时间
     */
    @Column(name = "CREATE_TIME")
    @JsonFormat(locale = "ZH",timezone = "GMT+8")
    private Date createTime;

}
