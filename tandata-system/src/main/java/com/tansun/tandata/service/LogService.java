package com.tansun.tandata.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.tansun.tandata.entity.LogInfoErro;
import com.tansun.tandata.mapper.LogInfoErroDao;
import com.tansun.tandata.vo.req.LogInfoSeachErroVo;
import com.tansun.tandata.vo.req.LogInfoSeachVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tansun.tandata.entity.LogInfo;
import com.tansun.tandata.mapper.LogInfoDao;

/**
 * @Description 日志类
 * @CreateDate 20200608
 * @author yangliu
 * @Version v1.0
 *
 */

@Service
public class LogService {
	
	@Autowired
	private LogInfoDao logInfoDao;
	@Autowired
	private LogInfoErroDao logInfoErroDao;
	
	/**
     * @Description 查询日志页面
     */
    public List<LogInfo> getLogInfoByKey(LogInfoSeachVo logInfoSeachVo) {
    	return logInfoDao.selectLogInfoByKey(logInfoSeachVo);
    }
    

	/**
     * @Description 插入日志
     */
    public void insertLogInfo(LogInfo logInfo) {
    	logInfoDao.insertLogInfo(logInfo);
    }
	/**
	 * @Description 查询
	 */
    public String selectLogInfoRaisnumById(String bkey){return logInfoDao.selectLogInfoRaisnumById(bkey);}


	/**
	 * @Description 查询错误日志页面
	 */
	public List<LogInfoErro> getLogInfoErroList(LogInfoSeachErroVo logInfoSeachErroVo) {
		return logInfoErroDao.selectLogInfoErroList(logInfoSeachErroVo);
	}
	/**
	 * @Description 插入错误日志
	 */
	public void insertLogInfoErro(LogInfoErro logInfoErro) {
		logInfoErroDao.insertLogInfoErro(logInfoErro);
	}



}

