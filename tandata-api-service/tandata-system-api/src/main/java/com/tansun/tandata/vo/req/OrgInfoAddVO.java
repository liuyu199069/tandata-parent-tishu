package com.tansun.tandata.vo.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.util.Date;

/**
 * 新增
 * @author wzf
 * @date 2020-06-09 16:26:22
 * @Version v1.0
 */
@Data
@ApiModel(value="新增")
public class OrgInfoAddVO implements Serializable {
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "机构名称", required = true)
	private String orgName;

	@ApiModelProperty(value = "上一级机构节点", required = true)
	private String parentCode;

	@ApiModelProperty(value = "机构类型", required = false)
	private Integer type;

}