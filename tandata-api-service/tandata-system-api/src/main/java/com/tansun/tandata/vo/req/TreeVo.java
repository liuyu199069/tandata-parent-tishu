package com.tansun.tandata.vo.req;

import java.util.ArrayList;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel(value="菜单树对象",description="菜单树的菜单对象")
@Data
public class TreeVo {


	@ApiModelProperty(value = "菜单id")
	private String code;

	@ApiModelProperty(value = "菜单中文名称")
	private String label;

	@ApiModelProperty(value = "排序字段")
	private Integer sort;

	@ApiModelProperty(value = "子菜单集合")
	private List<TreeVo> children = new ArrayList<>();

	public void setChildren(List<TreeVo> children) {
		if (children.size() > 1){
			children.sort((o1, o2) -> {
				if (o1.sort == null){
					return -1;
				}else if (o2.sort == null){
					return 1;
				}
				return o1.sort - o2.sort;
			});
		}
		this.children = children;
	}
}
