package com.tansun.tandata.mapper;

import java.util.List;
import java.util.Map;

import com.tansun.tandata.vo.res.UserRoleVO;
import com.tansun.tjdp.orm.persistence.BaseDao;
import org.apache.ibatis.annotations.Param;

import com.tansun.tandata.entity.User;
import com.tansun.tandata.entity.UserRole;

/**
 * @Description 用户信息接口
 * @CreateDate 20200426
 * @author shenxiaodong
 * @Version v1.0
 *
 */
public interface UserDao extends BaseDao<User> {
	
	/**
	 * @Description 验证用户名是否存在
	 * @param name
	 * @return
	 */
	public int  getUserByName(String name);
	/**
	 * @Description 根据用户名获取用户
	 * @param name
	 * @return
	 */
	public User findUserByName(String name);
	
	
	/**
	 * @Description 验证用户名和电话称唯一
	 * @param name phone
	 * @return
	 */
	public int  getUserByLoginNameOrPhone(@Param(value = "name") String name,@Param(value = "phone") String phone);
	
	/**
	 * 
	* @Description  添加用户信息
	* @param user
	 */
	public void saveUser(UserRoleVO user);
	
	/**
	 * @Description  添加用户角色中间表
	 */
	public void saveUserRole(UserRole ur);
	
	/**
	 * 根据id查询用户信息(用于编辑用户)
	 */
	public UserRoleVO selectUserById(String id);
	
	/**
	 * 查询所有用户信息
	 */
	List<UserRoleVO> selectAllUser();
	
	/**
	 * 修改用户信息
	 */
	void updateUser(UserRoleVO user);
	
	/**
	 * 根据用户编号删除用户的信息
	 */
	void deleteUserByUserId(String userCode);
	
	
	/**
	 * 根据用户角色编号删除用户角色的信息
	 */
	void deleteUserRole(String userCode);
	
	
	/**
	 * 根据用户编号修改用户状态信息
	 */
	int updateUserType(Map<String, Object> params);

	/**
	 * 模糊查询用户信息
	 */
	List<UserRoleVO> getUserByKey(@Param(value = "keywords") String keywords, @Param(value = "status") int status);

	/**
	 * 根据用户登录名称查询用户基本信息
	 */
	UserRoleVO selectUserAndRoleInfoByLoginName(String loginName);

	/**
	 * 用户登录以后返回用户基本信息
	 */
	UserRoleVO getUserInfoAfterLogin(String loginName);

	/**
	 * 修改用户对应的机构
	 */
    void updateOrgCodeByUser(Map<String, Object> params);

	/**
	 * 获取用户通过组织编码
	 */
    int getUserByOrgCode(String orgCode);
}
