package com.tansun.tandata.bigdata.vo.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(value="返回已提交任务信息")
public class FlinkSqlSubmitFlinkJobInfoRes implements Serializable {

    private static final long serialVersionUID = 5641650668630188611L;

    @ApiModelProperty(value = "作业id")
    private String jid;
    @ApiModelProperty(value = "作业自定义名称")
    private String name;
    @ApiModelProperty(value = "作业状态")
    private String state;
    @ApiModelProperty(value = "开始时间")
    private Integer startTime;
    @ApiModelProperty(value = "结束时间")
    private Integer endTime;
    @ApiModelProperty(value = "持续时间")
    private Integer duration;
}
