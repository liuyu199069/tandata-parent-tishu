package com.tansun.tandata.enums;

/**
 *  指标类型枚举
 *
 * @author gaoyufa
 */
public enum IndicatorsTypeEnum {
    /**
     * 衍生指标
     */
    BASIC_INDEX("basic_index","基础指标"),
    /**
     * 衍生指标
     */
    DERIVATIVE_INDEX("derivative_index","衍生指标");

    protected final String name;

    protected final String vaule;

    IndicatorsTypeEnum(String vaule,String name){
        this.vaule = vaule;
        this.name = name;
    }

    public String getVaule() {
        return vaule;
    }

    public String getName() {
        return name;
    }

}
