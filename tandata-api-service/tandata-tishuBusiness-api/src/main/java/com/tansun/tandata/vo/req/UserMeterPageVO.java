package com.tansun.tandata.vo.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
@ApiModel(
        value = "传入UserMeterPageVO对象",
        description = "用户仪表板对象"
)
@Data
public class UserMeterPageVO implements Serializable {
    @ApiModelProperty(
            value = "用户名",
            required = true,
            example = "us1"
    )
    private String userName;
    @ApiModelProperty(
            value = "仪表板名称",
            example = "yibiaoban1"
    )
    private String meterName;
    @ApiModelProperty(
            value = "状态",
            example = "0"
    )
    private Integer state;
    @ApiModelProperty(
            value = "处理时间开始",
            example = "2020-09-30 09:08:05"
    )
    private String handleTime_beg;
    @ApiModelProperty(
            value = "处理时间结束",
            example = "2020-09-30 12:08:05"
    )
    private String handleTime_end;
    @ApiModelProperty(
            value = "当前页",
            example = "1"
    )
    private int page;
    @ApiModelProperty(
            value = "显示数量",
            example = "30"
    )
    private int limit;
}
