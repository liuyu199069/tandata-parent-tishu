package com.tansun.tandata.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.tansun.tjdp.orm.entity.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;
@Entity
@Table(
        name = "BUS_METER_FILE_AUTH"
)
public class MeterFileAuth extends BaseEntity<MeterFileAuth, String> implements Serializable {
    private static final long serialVersionUID = -2701052307289939921L;
    /**
     * 提数ID
     */
    @Id
    @Column(name = "ID")
    private String id;
    /**
     * 用户仪表板表ID
     */
    @Column(name = "USER_METER_ID")
    private String userMeterId;
    /**
     * 提数序号
     */
    @Column(name = "USER_NAME")
    private String userName;
    /**
     * 提数序号
     */
    @Column(name = "METER_ID")
    private String meterId;
    /**
     * 提数序号
     */
    @Column(name = "METER_NAME")
    private String meterName;
    /**
     * 提数序号
     */
    @Column(name = "STATE")
    private Integer state;
    /**
     * 提数序号
     */
    @Column(name = "TABLE_NAME")
    private String tableName;
    /**
     * 提数序号
     */
    @Column(name = "CREATE_TIME")
    @JsonFormat(locale = "ZH",timezone = "GMT+8")
    private Date createTime;
    /**
     * 提数序号
     */
    @Column(name = "USER_NICK_NAME")
    private String userNickName;
    /**
     * 提数序号
     */
    @Column(name = "METER_FILE")
    private String meterFile;
    /**
     * 提数序号
     */
    @Column(name = "AUTHORAZE_TIME")
    @JsonFormat(locale = "ZH",timezone = "GMT+8")
    private Date authorazeTime;
    /**
     * 提数序号
     */
    @Column(name = "AUTH_USER_NAME")
    private String authUserName;
    /**
     * 提数序号
     */
    @Column(name = "AUTH_USER_NICK_NAME")
    private String authUserNickName;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getMeterId() {
        return meterId;
    }

    public void setMeterId(String meterId) {
        this.meterId = meterId;
    }

    public String getMeterName() {
        return meterName;
    }

    public void setMeterName(String meterName) {
        this.meterName = meterName;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getUserNickName() {
        return userNickName;
    }

    public void setUserNickName(String userNickName) {
        this.userNickName = userNickName;
    }

    public String getMeterFile() {
        return meterFile;
    }

    public void setMeterFile(String meterFile) {
        this.meterFile = meterFile;
    }

    public Date getAuthorazeTime() {
        return authorazeTime;
    }

    public void setAuthorazeTime(Date authorazeTime) {
        this.authorazeTime = authorazeTime;
    }

    public String getAuthUserName() {
        return authUserName;
    }

    public void setAuthUserName(String authUserName) {
        this.authUserName = authUserName;
    }

    public String getAuthUserNickName() {
        return authUserNickName;
    }

    public void setAuthUserNickName(String authUserNickName) {
        this.authUserNickName = authUserNickName;
    }

    public String getUserMeterId() {
        return userMeterId;
    }

    public void setUserMeterId(String userMeterId) {
        this.userMeterId = userMeterId;
    }
}