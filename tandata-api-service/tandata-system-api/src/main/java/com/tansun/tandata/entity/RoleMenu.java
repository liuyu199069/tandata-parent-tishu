package com.tansun.tandata.entity;

import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Description 角色菜单实体类
 * @CreateDate 20200429
 * @author shenxiaodong
 * @Version v1.0
 *
 */
@ApiModel(value="角色菜单实体类",description="角色菜单实体类")
@Data
public class RoleMenu implements Serializable{

	private static final long serialVersionUID = 206414710187793468L;

	@ApiModelProperty(value="id", name="id")
	private Integer id;

	@ApiModelProperty(value="角色编码", name="roleCode")
	private String roleCode;

	@ApiModelProperty(value="菜单id", name="menuIds")
	private List<String> menuIds;
}
