package com.tansun.tandata.utils;

/**
 * @Description
 * @Author will
 * @Date 2019/3/15 0015 下午 15:41
 */
public class CustomSumInfo {

    /**
     * 行号
     */
    private int rowNum;
    /**
     * 合并单元格个数
     */
    private int mergeSize;
    /**
     * 列数目
     */
    private int columnSize;
    /**
     * 统计起始行
     */
    private int sumInitRow;

    public CustomSumInfo(int rowNum, int mergeSize, int columnSize, int sumInitRow) {
        this.rowNum = rowNum;
        this.mergeSize = mergeSize;
        this.columnSize = columnSize;
        this.sumInitRow = sumInitRow;
    }

    public int getRowNum() {
        return rowNum;
    }

    public CustomSumInfo setRowNum(int rowNum) {
        this.rowNum = rowNum;
        return this;
    }

    public int getMergeSize() {
        return mergeSize;
    }

    public CustomSumInfo setMergeSize(int mergeSize) {
        this.mergeSize = mergeSize;
        return this;
    }

    public int getColumnSize() {
        return columnSize;
    }

    public CustomSumInfo setColumnSize(int columnSize) {
        this.columnSize = columnSize;
        return this;
    }

    public int getSumInitRow() {
        return sumInitRow;
    }

    public CustomSumInfo setSumInitRow(int sumInitRow) {
        this.sumInitRow = sumInitRow;
        return this;
    }
}
