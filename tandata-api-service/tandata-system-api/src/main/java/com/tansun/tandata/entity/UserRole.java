package com.tansun.tandata.entity;


import java.io.Serializable;
import java.util.List;

import com.tansun.tandata.entity.Role;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
/**
 * @Description 用户角色实体类
 * @CreateDate 20200427
 * @author shenxiaodong
 * @Version v1.0
 *
 */
@ApiModel(value="用户角色实体类",description="用户角色实体类")
@Data
public class UserRole implements Serializable{

	private static final long serialVersionUID = 8889777425624585015L;

	@ApiModelProperty(value="id", name="id")
	private Integer id;

	@ApiModelProperty(value="用户编码", name="userCode")
	private String userCode;

	@ApiModelProperty(value="角色", name="roles")
	private List<Role> roles;

}
