package com.tansun.tandata.apis.mapper;

import com.tansun.tandata.apis.entity.ApisInterface;
import com.tansun.tjdp.orm.persistence.BaseDao;

import java.util.List;
import java.util.Map;

/**
 * 接口信息配置表Dao
 * @author LinLaiChun
 * @date 2020-06-10 14:36:59
 * @Version v1.0
 */
public interface ApisInterfaceDao extends BaseDao<ApisInterface> {

	int insert(ApisInterface apisInterface);

	int batchInsert(List<ApisInterface> apisInterfaceList);

	int updateById(ApisInterface apisInterface);

	int batchUpdateTraditional(List<ApisInterface> apisInterfaceList);

	int batchUpdate(List<ApisInterface> apisInterfaceList);

	int deleteById(String id);

	int deleteByIds(List<String> idList);

    ApisInterface getById(String id);

	List<ApisInterface> getByIds(List<String> idList);

	List<ApisInterface> queryListByMap(Map<String, Object> queryMap);
}