package com.tansun.tandata.service;


import com.tansun.tandata.entity.OrgInfo;
import com.tansun.tandata.vo.res.OrgInfoTreeVO;

import java.util.List;
import java.util.Map;

/**
 * 业务接口
 * @author wzf
 * @date 2020-06-09 16:26:22
 * @Version v1.0
 */
public interface OrgInfoService {
	/**
     * 新增机构
     * @param orgInfo
     */
	void insert(OrgInfo orgInfo);

	/**
     * 更新机构
     * @param orgInfo
     */
	void updateByOrgCode(OrgInfo orgInfo);

	/**
     * 删除机构
     * @param orgCode
     */
	void deleteByOrgCode(String orgCode);

	/**
     * 获取机构
     * @param orgCode
     * @return OrgInfo
     */
	OrgInfo getByOrgCode(String orgCode);

	/**
	 * 根据机构名查询机构
	 */
	int getOrgInfoByOrgName(String orgName);

	/**
	 * 根据机构名和编码查询机构
	 */
	int getOrgInfoByOrgNameAndCode(String orgName, String orgCode);
	
	/**
	 * 根据机构编号查询子机构
	 * @param orgCode
	 * @return
	 */
	List<OrgInfoTreeVO> getChildOrgByOrgCode(String orgCode);

}
