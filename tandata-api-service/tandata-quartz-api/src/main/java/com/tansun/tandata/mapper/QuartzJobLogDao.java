package com.tansun.tandata.mapper;

import com.tansun.tandata.entity.QuartzJobLog;
import com.tansun.tjdp.orm.persistence.BaseDao;

import java.util.List;
import java.util.Map;

/**
 * 任务日志Dao
 * @author LinLaiChun
 * @date 2020-06-08 15:41:04
 * @Version v1.0
 */
public interface QuartzJobLogDao extends BaseDao<QuartzJobLog> {

	int insert(QuartzJobLog quartzJobLog);

	int batchInsert(List<QuartzJobLog> quartzJobLogList);

	int updateById(QuartzJobLog quartzJobLog);

	int batchUpdate(List<QuartzJobLog> quartzJobLogList);

	int deleteById(String id);

	int deleteByIds(List<String> idList);

    QuartzJobLog getById(String id);

	List<QuartzJobLog> getByIds(List<String> idList);

	List<QuartzJobLog> queryListByMap(Map<String, Object> queryMap);
}