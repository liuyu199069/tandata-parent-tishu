package com.tansun.tandata.mapper;


import com.tansun.tandata.entity.RaiseNumReceiveUser;
import com.tansun.tjdp.orm.persistence.BaseDao;

import java.util.List;

public interface RaiseNumReceiveUserDao extends BaseDao<RaiseNumReceiveUser> {
    int deleteByPrimaryKey(String id);

    int insert(RaiseNumReceiveUser record);

    int insertSelective(RaiseNumReceiveUser record);

    RaiseNumReceiveUser selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(RaiseNumReceiveUser record);

    int updateByPrimaryKey(RaiseNumReceiveUser record);

    int insertBath(List<RaiseNumReceiveUser> list);

    int deleteByRaiseNumId(String raiseNumId);

    List<RaiseNumReceiveUser> gainListByRaisenumId(String raiseNumId);

}