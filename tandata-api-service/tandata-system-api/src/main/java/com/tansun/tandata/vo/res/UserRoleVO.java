package com.tansun.tandata.vo.res;

import com.tansun.tandata.entity.Role;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 用户角色
 * @author wzf
 * @date 2020-06-09 16:26:22
 * @Version v1.0
 */
@Data
@ApiModel(value="用户角色")
public class UserRoleVO implements Serializable {
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "用户编码")
	private String userCode;

	@ApiModelProperty(value = "用户名称")
	private String userName;

	@ApiModelProperty(value = "登录名称")
	private String loginName;

	@ApiModelProperty(value = "用户手机")
	private String userPhone;

	@ApiModelProperty(value = "用户密码")
	private String password;

	@ApiModelProperty(value = "用户类型")
	private String type;

	@ApiModelProperty(value = "用户状态")
	private Integer status;

	@ApiModelProperty(value = "机构名称")
	private String orgCode;

	@ApiModelProperty(value = "创建人")
	private String creater;

	@ApiModelProperty(value = "创建时间")
	private Date createrTime;

	@ApiModelProperty(value = "修改人")
	private String updater;

	@ApiModelProperty(value = "修改时间")
	private Date updateTime;

	@ApiModelProperty(value = "是否删除")
	private Integer isDelete;

	@ApiModelProperty(value = "乐观锁")
	private Integer updateCount;

	@ApiModelProperty(value = "对应角色集合")
	private List<Role> roles;

}