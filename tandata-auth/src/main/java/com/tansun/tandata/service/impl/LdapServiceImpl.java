package com.tansun.tandata.service.impl;

import com.tansun.tandata.service.LdapService;
import com.tansun.tandata.utils.StringUtil;
import com.tansun.tandata.vo.LdapUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ldap.core.AttributesMapper;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.filter.EqualsFilter;
import org.springframework.ldap.query.LdapQuery;
import org.springframework.ldap.query.LdapQueryBuilder;
import org.springframework.stereotype.Service;

import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import java.util.List;

/**
 * Ldap服务类
 */
@Service
public class LdapServiceImpl implements LdapService {
    @Autowired
    private LdapTemplate ldapTemplate;
    /**
     * AD认证
     *
     * @param username 用户名
     * @param password 密码
     */
    @Override
    public LdapUser ldapAuth(String username, String password) {
        EqualsFilter filter = new EqualsFilter("sAMAccountName", username);
        if(ldapTemplate.authenticate("", filter.toString(), password)){
            LdapQuery query = LdapQueryBuilder.query().where("sAMAccountName").is(username);
            List<LdapUser> search = ldapTemplate.search(query, new AttributesMapper<LdapUser>() {
                @Override
                public LdapUser mapFromAttributes(Attributes attributes) throws NamingException {
                    String cn =null;
                    if(attributes.get("cn")!=null&&attributes.get("cn").size()>0){
                        cn = (String) attributes.get("cn").get(0);
                    }
                    String sAMAccountName=null;
                    if(attributes.get("sAMAccountName")!=null&&attributes.get("sAMAccountName").size()>0){
                        sAMAccountName = (String) attributes.get("sAMAccountName").get(0);
                    }
                    String apartment =null;
                    if(attributes.get("apartment")!=null&&attributes.get("apartment").size()>0){
                        apartment = (String) attributes.get("apartment").get(0);
                    }
                    return new LdapUser(cn, sAMAccountName, apartment,null);
                }
            });
            if(search!=null&&search.size()>0){
                return search.get(0);
            }else {
                return null;
            }
        } else {
            return null;
        }
    }
    /**
     * 根据关键字查询ldap用户列表
     * @param keyWords 查询关键字
     */
    @Override
    public List<LdapUser> seachLdapUserByKeywords(String keyWords) {
        LdapQuery query = LdapQueryBuilder.query().where("sAMAccountName").like(keyWords+"*").or("cn").like(keyWords+"*");
        List<LdapUser> search = ldapTemplate.search(query, new AttributesMapper<LdapUser>() {
            @Override
            public LdapUser mapFromAttributes(Attributes attributes) throws NamingException {
                String cn =null;
                if(attributes.get("cn")!=null&&attributes.get("cn").size()>0){
                    cn = (String) attributes.get("cn").get(0);
                }
                String sAMAccountName=null;
                if(attributes.get("sAMAccountName")!=null&&attributes.get("sAMAccountName").size()>0){
                    sAMAccountName = (String) attributes.get("sAMAccountName").get(0);
                }
                String apartment =null;
                if(attributes.get("apartment")!=null&&attributes.get("apartment").size()>0){
                    apartment = (String) attributes.get("apartment").get(0);
                }
                return new LdapUser(cn, sAMAccountName, apartment,null);
            }
        });
        return search;
    }
}
