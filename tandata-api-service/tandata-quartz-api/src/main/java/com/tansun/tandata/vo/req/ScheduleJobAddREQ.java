package com.tansun.tandata.vo.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 新增任务
 * @author LinLaiChun
 * @date 2020-06-04 11:02:04
 * @Version v1.0
 */
@Data
@ApiModel(value="新增任务")
public class ScheduleJobAddREQ implements Serializable {
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "任务名称", required = true)
	@NotBlank
	private String jobName;

	@ApiModelProperty(value = "任务分组", required = true)
	@NotBlank
	private String jobGroup;

	@ApiModelProperty(value = "表达式", required = true)
	@NotBlank
	private String cronExpression;

	@ApiModelProperty(value = "描述", required = false)
	private String description;

	@ApiModelProperty(value = "调用类型（0类调用1服务调用）", required = true)
	@NotNull
	private Integer type;

	@ApiModelProperty(value = "调用服务名/调用类", required = true)
	@NotBlank
	private String beanName;

	@ApiModelProperty(value = "调用url/类中方法", required = true)
	@NotBlank
	private String methodName;

}