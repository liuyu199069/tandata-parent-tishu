package com.tansun.tandata.service.impl;

import com.tansun.tandata.entity.MeterFileAuth;
import com.tansun.tandata.entity.UserMeter;
import com.tansun.tandata.mapper.MeterFileAuthDao;
import com.tansun.tandata.mapper.UserMeterDao;
import com.tansun.tandata.service.BiServicer;
import com.tansun.tandata.service.UserMeterService;
import com.tansun.tandata.utils.ParentServiceImp;
import com.tansun.tandata.utils.StringUtil;
import com.tansun.tandata.vo.UserInfo;
import com.tansun.tandata.vo.req.MeterFileAuthPageVO;
import com.tansun.tandata.vo.req.UserMeterPageVO;
import com.tansun.tandata.vo.res.BIMeter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * @author liuyu
 * @Description 我的差数权限业务类
 * @CreateDate 20200924
 * @Version v1.0
 */
@Service("userMeterServiceImpl")
public class UserMeterServiceImpl extends ParentServiceImp<UserMeterDao, UserMeter> implements UserMeterService {
    @Autowired
    private UserMeterDao userMeterDao;
    @Autowired
    private MeterFileAuthDao meterFileAuthDao;
    @Autowired
    private BiServicer biServicer;
    @Value("${spring.profiles.active}")
    private String active;

    /**
     * @Description 根据账号从bi系统拉取仪表列表
     * @CreateDate 20200924
     * @author liuyu
     * @Version v1.0
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public Integer pullUserMeterList(String userName) {
        List<BIMeter> BIMeterList = new ArrayList<>();

        if(!"dev".equals(active)){
            //根据用户名调用BI系统接口获取仪表板列表
            BIMeterList.addAll(biServicer.getDashboardList(userName));
        }

        if("dev".equals(active)) {
            /*创建测试数据开始*/
            BIMeter bIMeter1 = new BIMeter();
            bIMeter1.setMeterId("64372a21f4b943b6b37d7184031358cb");
            bIMeter1.setMeterName("新建仪表板1");
            bIMeter1.setCreateTime(1600327915029l);
            List<String> fromtableName1 = new ArrayList<>();
            fromtableName1.add("T1");
            fromtableName1.add("T2");
            fromtableName1.add("T3");
            bIMeter1.setFromtableName(fromtableName1);
            BIMeterList.add(bIMeter1);
            BIMeter bIMeter2 = new BIMeter();
            bIMeter2.setMeterId("64372a21f4b943b6b37d718403135817");
            bIMeter2.setMeterName("新建仪表板002");
            bIMeter2.setCreateTime(1600327915029L);
            List<String> fromtableName2 = new ArrayList<>();
            fromtableName2.add("T1");
            fromtableName2.add("T4");
            fromtableName2.add("T5");
            bIMeter2.setFromtableName(fromtableName2);
            BIMeterList.add(bIMeter2);
            BIMeter bIMeter3 = new BIMeter();
            bIMeter3.setMeterId("64372a21f4b943b6b37d718403135818");
            bIMeter3.setMeterName("新建仪表板003");
            bIMeter3.setCreateTime(1600327915029L);
            List<String> fromtableName3 = new ArrayList<>();
            fromtableName3.add("T1");
            fromtableName3.add("T4");
            fromtableName3.add("T6");
            bIMeter3.setFromtableName(fromtableName3);
            BIMeterList.add(bIMeter3);
            /*创建测试数据结束*/
        }
        //根据用户名查询
        List<String> userMeterList = userMeterDao.selectUserMeterByUsercode(userName);
        Date now = new Date();
        if (userMeterList != null && userMeterList.size() > 0) {
            for (BIMeter bIMeter : BIMeterList) {
                String userMeterStr = bIMeter.getMeterId() + "*" + bIMeter.getMeterName();
                if (userMeterList.contains(userMeterStr)) {
                    continue;
                }
                UserMeter userMeter = gainUserMeter(bIMeter, userName, now);
                userMeterDao.merageUserMeter(userMeter);
            }
        } else if(BIMeterList.size()>0){
            //该用户第一次拉取数据
            List<UserMeter> userMeters = new ArrayList<>();
            for (BIMeter bIMeter : BIMeterList) {
                UserMeter userMeter = gainUserMeter(bIMeter, userName, now);
                userMeters.add(userMeter);
            }
            userMeterDao.insertBath(userMeters);
        }
        return 1;
    }
    /**
     * @Description 查询用户创建的仪表板列表
     * @CreateDate 20200924
     * @author liuyu
     * @Version v1.0
     */
    @Override
    public List<UserMeter> selectUserMeter(UserMeterPageVO userMeterPageVO) {
        userMeterPageVO.setUserName(getCurrUser().getLoginName());
        return userMeterDao.selectUserMeterList(userMeterPageVO);
    }
    /**
     * @Description 查询仪表板授权列表
     * @CreateDate 20200924
     * @author liuyu
     * @Version v1.0
     */
    @Override
    public List<MeterFileAuth> selectMeterFileAuthList(MeterFileAuthPageVO meterFileAuthPageVO) {
        if(StringUtil.isEmpty(meterFileAuthPageVO.getLoginName())) {
            return meterFileAuthDao.selectMeterFileAuthList(meterFileAuthPageVO);
        }else {
            return meterFileAuthDao.selectMeterFileAuthList2(meterFileAuthPageVO);
        }
    }
    /**
     * @Description 授权下载仪表板导出文件
     * @CreateDate 20200924
     * @author liuyu
     * @Version v1.0
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public int MeterFileAuthoraze(String meterFileId) {
        Date now = new Date();
        MeterFileAuth meterFileAuth = meterFileAuthDao.selectByPrimaryKey(meterFileId);
        //获取用户仪表板表id
        String userMeterId = meterFileAuth.getUserMeterId();
        meterFileAuth = new MeterFileAuth();
        meterFileAuth.setId(meterFileId);
        meterFileAuth.setState(1);//已授权
        meterFileAuth.setAuthorazeTime(now);
        //修改仪表板授权表状态
        meterFileAuthDao.updateByPrimaryKeySelective(meterFileAuth);
        UserMeter userMeter = new UserMeter();
        userMeter.setId(userMeterId);
        userMeter.setState(2);//可以下载
        userMeter.setUpdateTime(now);
        userMeterDao.updateByPrimaryKeySelective(userMeter);
        //修改用户仪表板表状态
        return 1;
    }
    /**
     * @Description 用户处理完成仪表板
     * @CreateDate 20200924
     * @author liuyu
     * @Version v1.0
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public int handleDynamic(String userMeterId) {
        //TODO 调用BI接口将仪表板文件下载到提数系统文件服务器上

        Date now = new Date();
        //修改用户仪表板对象状态
        UserMeter userMeter = userMeterDao.selectByPrimaryKey(userMeterId);
        userMeter.setUpdateTime(now);
        userMeter.setState(1);//处理完成
        userMeterDao.updateByPrimaryKeySelective(userMeter);

        //新建仪表板授权对象
        MeterFileAuth meterFileAuth = new MeterFileAuth();
        meterFileAuth.setId(UUID.randomUUID().toString().replaceAll("-", ""));
        meterFileAuth.setState(0);//新建状态
        UserInfo userInfo = getCurrUser();
        meterFileAuth.setAuthUserName(userInfo.getLoginName());
        meterFileAuth.setAuthUserNickName(userInfo.getUserName());
        meterFileAuth.setCreateTime(now);
        meterFileAuth.setMeterFile("todo");//TODO 填服务器上的文件名
        meterFileAuth.setMeterId(userMeter.getMeterId());
        meterFileAuth.setMeterName(userMeter.getMeterName());
        meterFileAuth.setTableName(userMeter.getTableName());
        meterFileAuth.setUserMeterId(userMeterId);
        meterFileAuth.setUserName(userMeter.getUserName());
        meterFileAuthDao.insert(meterFileAuth);
        return 1;
    }

    /**
     * @Description 组装用户仪表板实体
     * @CreateDate 20200924
     * @author liuyu
     * @Version v1.0
     */
    private UserMeter gainUserMeter(BIMeter bIMeter, String userName, Date now) {
        UserMeter userMeter = new UserMeter();
        userMeter.setId(UUID.randomUUID().toString().replaceAll("-", ""));
        userMeter.setUserName(userName);
        userMeter.setMeterId(bIMeter.getMeterId());
        userMeter.setMeterName(bIMeter.getMeterName());
        userMeter.setMeterCreateTime(new Date(bIMeter.getCreateTime()));
        userMeter.setCreateTime(now);
        List<String> fromtableName = bIMeter.getFromtableName();
        if (fromtableName != null && fromtableName.size() > 0) {
            StringBuffer tableNameBf = new StringBuffer();
            for (String tableName : fromtableName) {
                tableNameBf.append("," + tableName);
            }
            userMeter.setTableName(tableNameBf.substring(1));
        }
        userMeter.setState(0);
        return userMeter;
    }

}
