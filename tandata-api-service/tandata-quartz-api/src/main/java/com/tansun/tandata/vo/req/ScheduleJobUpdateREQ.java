package com.tansun.tandata.vo.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 编辑任务表
 * @author LinLaiChun
 * @date 2020-06-04 11:02:04
 * @Version v1.0
 */
@Data
@ApiModel(value="编辑任务表")
public class ScheduleJobUpdateREQ implements Serializable {
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "主键", required = true)
	private String id;

	@ApiModelProperty(value = "任务名称", required = false)
	private String jobName;

//	@ApiModelProperty(value = "任务状态(PAUSED暂停、NORMAL运行)", required = false)
//	private String jobStatus;

	@ApiModelProperty(value = "任务分组", required = false)
	private String jobGroup;

	@ApiModelProperty(value = "表达式", required = false)
	private String cronExpression;

	@ApiModelProperty(value = "描述", required = false)
	private String description;

	@ApiModelProperty(value = "调用类型（0类调用1服务调用）", required = false)
	private Integer type;

	@ApiModelProperty(value = "调用服务名/调用类", required = false)
	private String beanName;

	@ApiModelProperty(value = "调用url/类中方法", required = false)
	private String methodName;

}