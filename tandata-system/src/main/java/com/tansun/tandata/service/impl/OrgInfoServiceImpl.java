package com.tansun.tandata.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tansun.tandata.entity.OrgInfo;
import com.tansun.tandata.mapper.OrgInfoDao;
import com.tansun.tandata.service.OrgInfoService;
import com.tansun.tandata.utils.CodeGenerator;
import com.tansun.tandata.utils.ParentServiceImp;
import com.tansun.tandata.vo.UserInfo;
import com.tansun.tandata.vo.res.OrgInfoTreeVO;

/**
 * 业务接口实现
 * @author wzf
 * @date 2020-06-09 16:26:22
 * @Version v1.0
 */
@Service("orgInfoService")
public class OrgInfoServiceImpl extends ParentServiceImp<OrgInfoDao, OrgInfo> implements OrgInfoService {
	@Autowired
	private OrgInfoDao orgInfoDao;

	/**
     * 新增机构
     * @param orgInfo
     */
	@Override
	@Transactional(readOnly = false)
	public void insert(OrgInfo orgInfo){
		orgInfo.setOrgCode(CodeGenerator.getUUID(OrgInfo.class.getSimpleName()));
		UserInfo user = getCurrUser();
		orgInfo.setCreaterId(user.getUserCode());
		orgInfo.setCreaterTime(new Date());
		orgInfoDao.insert(orgInfo);
	}

	/**
     * 更新机构
     * @param orgInfo
     */
	@Override
	@Transactional(readOnly = false)
	public void updateByOrgCode(OrgInfo orgInfo){
		UserInfo user = getCurrUser();
		orgInfo.setUpdateId(user.getUserCode());
		orgInfo.setUpdateTime(new Date());
		orgInfoDao.updateByOrgCode(orgInfo);
	}

	/**
     * 删除机构
     * @param orgCode
     */
	@Override
	@Transactional(readOnly = false)
	public void deleteByOrgCode(String orgCode) {
		orgInfoDao.deleteByOrgCode(orgCode);
	}

	/**
     * 获取机构
     * @param orgCode
     * @return OrgInfo
     */
	@Override
	public OrgInfo getByOrgCode(String orgCode) {
		return orgInfoDao.getByOrgCode(orgCode);
	}

	/**
	 * 根据机构名查询机构
	 */
	@Override
	public int getOrgInfoByOrgName(String orgName) {
		return orgInfoDao.getOrgInfoByOrgName(orgName);
	}

	/**
	 * 根据机构名和编码查询机构
	 */
	@Override
	public int getOrgInfoByOrgNameAndCode(String orgName, String orgCode) {
		Map<String, Object> params = new HashMap<>();
		params.put("orgName", orgName);
		params.put("orgCode", orgCode);
		return orgInfoDao.getOrgInfoByOrgNameAndCode(params);
	}

	/**
	 * 根据机构编号查询子机构
	 */
	@Override
	public List<OrgInfoTreeVO> getChildOrgByOrgCode(String orgCode) {
		//全查出所有机构信息
		List<OrgInfo> orgInfos = orgInfoDao.selectAllOrg();
		//所有下级机构
		List<OrgInfoTreeVO> orgInfoTreeVOList = buildTree(orgInfos, orgCode, false);
		// TODO Auto-generated method stub
		return orgInfoTreeVOList;
	}
	
	/**
	 *遍历自身机构 
	 * @param orgInfos
	 * @param orgCode
	 * @param isDisabled
	 * @return
	 */
	public List<OrgInfoTreeVO> buildTree(List<OrgInfo> orgInfos, String orgCode, boolean isDisabled) {
		List<OrgInfoTreeVO> treeVoList = new ArrayList<>();
		OrgInfoTreeVO orgInfoTreeVO;
		for (OrgInfo orgInfo : orgInfos) {
			if(orgInfo.getParentCode().equals(orgCode)) {
				List<OrgInfoTreeVO> childList = buildTree(orgInfos, orgInfo.getOrgCode(), isDisabled);
				orgInfoTreeVO = new OrgInfoTreeVO();
				orgInfoTreeVO.setOrgCode(orgInfo.getOrgCode());
				orgInfoTreeVO.setOrgName(orgInfo.getOrgName());
				orgInfoTreeVO.setParentCode(orgInfo.getParentCode());
				orgInfoTreeVO.setType(orgInfo.getType());
				orgInfoTreeVO.setChildren(childList);
				treeVoList.add(orgInfoTreeVO);
			}
		}
		return treeVoList;
	}
}
