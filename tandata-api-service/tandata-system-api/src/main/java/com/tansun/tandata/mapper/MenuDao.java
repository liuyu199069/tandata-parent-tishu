package com.tansun.tandata.mapper;

import java.util.List;

import com.tansun.tandata.vo.res.RoleCodeApiVO;
import com.tansun.tjdp.orm.persistence.BaseDao;
import org.apache.ibatis.annotations.Param;

import com.tansun.tandata.entity.Menu;
import com.tansun.tandata.entity.Role;

public interface MenuDao extends BaseDao<Menu> {
	/**
	 * 查询角色菜单信息
	 */
	List<String> selectMenuByRoleCode(@Param(value = "roleCode") String roleCode);

	/**
	 * 查询角色菜单api信息
	 */
	List<RoleCodeApiVO> selectMenuApiByRoleCode();

	/**
	 * 查询所有菜单信息
	 */
	List<Menu> selectAllMenu();
	
	
	/**
	 * 新增菜单树
	 */
	void saveMenu(Menu menu);

	/**
	 * 根据菜单中文名或英文名查询菜单是否存在
	 */
    int getMenuByChnNameOrEngName(@Param(value = "fnChnNm") String fnChnNm, @Param(value = "fnEngNm") String fnEngNm, @Param(value = "id") String id);

	/**
	 * 根据菜单id查询菜单信息
	 */
    Menu selectMenuInfoById(String id);

	/**
	 * 更新菜单信息
	 */
    void updateMenu(Menu menu);

	/**
	 * 根据菜单id删除角色菜单表的信息
	 */
	void deleteRoleMenuById(String id);

	/**
	 * 根据菜单id删除菜单信息
	 */
	void deleteMenuById(String id);

	/**
	 * 根据角色编号查询菜单信息
	 */
	List<Menu> selectMenuInfoByRoleCode(String roleCode);

	/**
	 * 根据角色编号查询父菜单信息
	 */
	List<Menu> selectParentMenuInfoByRoleCode(String roleCode);
}
