package com.tansun.tandata.service;

import com.tansun.tandata.entity.MeterFileAuth;
import com.tansun.tandata.entity.UserMeter;
import com.tansun.tandata.vo.req.MeterFileAuthPageVO;
import com.tansun.tandata.vo.req.UserMeterPageVO;

import java.util.List;

public interface UserMeterService {

    public Integer pullUserMeterList(String userName);

    public List<UserMeter> selectUserMeter(UserMeterPageVO userMeterPageVO);

    public List<MeterFileAuth> selectMeterFileAuthList(MeterFileAuthPageVO meterFileAuthPageVO);

    public int MeterFileAuthoraze(String meterFileId);

    public int handleDynamic(String userMeterId);



}
