package com.tansun.tandata.vo;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;

@ApiModel(value="域账号用户基本对象",description="域账号用户基本对象")
@Data
public class LdapUser implements Serializable {
    private static final long serialVersionUID = 2664298054437501779L;
    private String cn;//中文名
    private String sAMAccountName;//域账号
    private String apartment;//部门
    private String password;//密码

    public LdapUser(){}
    public LdapUser(String cn, String sAMAccountName, String apartment, String password) {
        this.cn = cn;
        this.sAMAccountName = sAMAccountName;
        this.apartment = apartment;
        this.password = password;
    }
}
