package com.tansun.tandata.vo;

import lombok.Data;

@Data
public class LoginUserVo {
	private String userName;
	private String password;
}
