package com.tansun.tandata.task;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.tansun.tandata.entity.RaiseNum;
import com.tansun.tandata.mapper.RaiseNumDao;
import com.tansun.tandata.service.RaisNumService;
import com.tansun.tandata.utils.HttpUtil;
import com.tansun.tjmas.cloud.base.redis.utils.RedisUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.Date;
import java.util.List;

/**
 * @author liuyu
 * @Description 提数定时任务
 * @CreateDate 20201202
 * @Version v1.0
 */
@Configuration      //1.主要用于标记配置类，兼备Component的效果。
@EnableScheduling   // 2.开启定时任务
public class RaisNumTask {
    @Autowired
    private RaiseNumDao raiseNumDao;
    @Autowired
    private RaisNumService raisNumService;

    //每分钟执行一次，将延时提数的提数记录调用提数脚本
    @Scheduled(cron = "0 0/1 * * * ? ")
    private void execRaisNumShell() {
        //System.out.println("xian zai de shi ijian"+new Date());
        raisNumService.execRaisNumShell();
        //System.out.println("diao yong cheng gong"+new Date());
    }
    //每天12点执行一次，将过期的表清除
    @Scheduled(cron = "0 0 0 1/1 * ? ")
    private void execCleanTableShell(){
        System.out.println("xian zai de shi ijian"+new Date());
        raisNumService.execCleanTableShell();
        System.out.println("diao yong cheng gong"+new Date());
    }
}
