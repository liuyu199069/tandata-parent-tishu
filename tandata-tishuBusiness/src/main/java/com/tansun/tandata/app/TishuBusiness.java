package com.tansun.tandata.app;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

import javax.validation.Validator;

/**
 * @Description 启动类
 * @CreateDate 20200425
 * @author yangliu 
 * @Version v1.0
 */
@SpringBootApplication(scanBasePackages=("com.tansun"))
@EnableEurekaClient
@MapperScan("com.tansun.tandata.mapper")
@EntityScan(basePackages=("com.tansun.tandata.entity"))
@EnableFeignClients(basePackages=("com.tansun.tandata"))
@EnableCircuitBreaker
@ComponentScan(basePackages=("com.tansun"))
public class TishuBusiness {

	public static void main(String[] args) {
		SpringApplication.run(TishuBusiness.class, args);
	}
	
	public ResourceBundleMessageSource getMessageSource() throws Exception {
        ResourceBundleMessageSource rbms = new ResourceBundleMessageSource();
        rbms.setDefaultEncoding("UTF-8");
        rbms.setBasenames("i18n/messages");
        return rbms;
    }
 
    @Bean
    public Validator getValidator() throws Exception {
        LocalValidatorFactoryBean validator = new LocalValidatorFactoryBean();
        validator.setValidationMessageSource(getMessageSource());
        return validator;
    }
}
