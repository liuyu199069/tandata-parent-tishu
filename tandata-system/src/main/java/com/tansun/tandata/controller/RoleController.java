package com.tansun.tandata.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tansun.tandata.service.RoleService;
import com.tansun.tandata.utils.Constants;
import com.tansun.tandata.utils.LocaleMessage;
import com.tansun.tandata.utils.ParentController;
import com.tansun.tandata.utils.ResponseBase;
import com.tansun.tandata.vo.req.RoleVO;
import com.tansun.tandata.vo.res.RoleMenuVO;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;

/**
 * @Description 角色信息控制器
 * @CreateDate 20200428
 * @author shenxiaodong
 * @Version v1.0
 *
 */
@RestController
@RequestMapping(value = "/sys/role")
public class RoleController extends ParentController{
	@Autowired
	private RoleService roleService;
	@Autowired
	private LocaleMessage localeMessage;

	@ApiOperation(value="根据用户编码获取角色", notes="根据用户编码获取角色")
	@ApiImplicitParam(name = "userCode", value = "用户编码", required = true, dataType = "String")
	@RequestMapping(value = "/selectRolesByUserCode", method = RequestMethod.GET)
	public ResponseBase getRolesByUserCode(String userCode) {
		List<RoleMenuVO> roles = roleService.selectRolesByUserCode(userCode);
		return setResultSuccess(roles);
	}

	@ApiOperation(value="根据特定条件查询所有角色信息", notes="根据特定条件查询所有角色信息")
	@ApiImplicitParam(name = "roleName", value = "角色名称", required = true, dataType = "String")
	@RequestMapping(value = "/selectAllRole", method=RequestMethod.GET)
	public ResponseBase selectAllRole(String roleName) {
		if (roleName == null){
			roleName = Constants.CHAR_BLANK;
		}
		List<RoleMenuVO> roles = roleService.selectAllRole(roleName);
		return setResultSuccess(roles);
	}

	@ApiOperation(value="根据角色编号删除角色信息", notes="根据角色编号删除角色信息")
	@ApiImplicitParam(name = "roleCode", value = "角色编号", required = true, dataType = "String")
	@RequestMapping(value = "/deleteRole", method=RequestMethod.GET)
	@Transactional(propagation=Propagation.REQUIRED,rollbackFor=Exception.class)
	public ResponseBase deleteRole(String roleCode) {
		try {
			roleService.deleteRoleById(roleCode);
		} catch (Exception e) {
			e.printStackTrace();
			//系统错误
			return setResult(Constants.HTTP_RES_CODE_500, localeMessage.getMessage("sys.common.error"),null);
		}
		return setResultSuccess(localeMessage.getMessage("sys.user.deleteSuccess"));
	}

	@ApiOperation(value="角色基本信息插入", notes="角色基本信息插入")
	@RequestMapping(value = "/saveRole", method=RequestMethod.POST)
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public ResponseBase saveRole(@Valid @RequestBody RoleVO roleVO, BindingResult bindingResult) {
		// 验证实体输入参数
		ResponseBase validResult = validateField(bindingResult);
		if (validResult != null) {
			return validResult;
		}
		//存在性验证，姓名验证
		int count = roleService.getRoleByName(roleVO.getRoleName());
		if (count > Constants.CHAR_INT_ZERO) {
			return setResult(Constants.HTTP_RES_CODE_402,localeMessage.getMessage("sys.common.role.name.isExist"),null);
		}
		try {
			roleService.saveRole(roleVO);
			return setResultSuccess(roleVO);
		} catch (RuntimeException e) {
			e.printStackTrace();
			//系统错误
			return setResult(Constants.HTTP_RES_CODE_500,localeMessage.getMessage("sys.common.error"),null);
		}

	}

	@ApiOperation(value="角色基本信息修改", notes="角色基本信息修改")
	@RequestMapping(value = "/updateRole", method = RequestMethod.POST)
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public ResponseBase updateRole(@Valid @RequestBody RoleVO roleVO, BindingResult bindingResult) {
		// 验证实体输入参数
		ResponseBase validResult = validateField(bindingResult);
		if (validResult != null) {
			return validResult;
		}
		//存在性验证，姓名验证
		RoleMenuVO check = roleService.getRoleById(roleVO.getRoleCode());
		if(!check.getRoleName().equals(roleVO.getRoleName())) {	
			int count = roleService.getRoleByName(roleVO.getRoleName());
			if (count>Constants.CHAR_INT_ZERO) {
				return setResult(Constants.HTTP_RES_CODE_402,localeMessage.getMessage("sys.common.role.name.isExist"),null);
			}
		}
		try {
			roleService.updateRole(roleVO);
			return setResultSuccess(roleVO);
		} catch (RuntimeException e) {
			e.printStackTrace();
			return setResult(Constants.HTTP_RES_CODE_500,localeMessage.getMessage("sys.common.error"),null); //系统错误
		}
	}
}
