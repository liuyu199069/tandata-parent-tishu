package com.tansun.tandata.enums;

/**
 *  字典类型枚举
 *
 * @author gaoyufa
 */
public enum DictionaryTypeEnum {
    /**
     * 目标表名称
     */
    TARGET_TABLE_NAME("t_idx_result_wide","目标表名称"),
    /**
     * 指标维度
     */
    DICT_DIMENSION_TYPE("dict_dimension_type","指标维度");

    protected final String name;

    protected final String vaule;

    DictionaryTypeEnum(String vaule, String name){
        this.vaule = vaule;
        this.name = name;
    }

    public String getVaule() {
        return vaule;
    }

    public String getName() {
        return name;
    }

}
