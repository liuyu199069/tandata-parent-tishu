package com.tansun.tandata.apis.vo.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.*;
import java.io.Serializable;

/**
 * 新增接口主题信息
 * @author LinLaiChun
 * @date 2020-06-10 15:08:46
 * @Version v1.0
 */
@Data
@ApiModel(value="新增接口主题信息")
public class ApisSubjectAddREQ implements Serializable {
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "主题名称", required = true)
	@NotBlank
	@Size(max = 200)
	private String name;

	@ApiModelProperty(value = "父主题ID", required = true)
	@Size(max = 70)
	@NotBlank
	private String parentId;

	@ApiModelProperty(value = "类型（0服务、1主题）", required = true)
	@PositiveOrZero
	@Max(value = 1)
	@NotNull
	private Integer type;

	@ApiModelProperty(value = "主题描述", required = false)
	@Size(max = 1000)
	private String remark;

	@ApiModelProperty(value = "排序字段", required = true)
	@PositiveOrZero
	@Max(value = 999)
	@NotNull
	private Integer sort;




}