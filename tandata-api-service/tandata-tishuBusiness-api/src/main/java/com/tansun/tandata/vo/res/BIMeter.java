package com.tansun.tandata.vo.res;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@ApiModel(
        value = "BI系统仪表板",
        description = "BI系统仪表板对象"
)
@Data
public class BIMeter implements Serializable {
    @ApiModelProperty("仪表板id")
    private String meterId;
    @ApiModelProperty("仪表板名称")
    private String meterName;
    @ApiModelProperty("仪表板创建时间")
    private long createTime;
    @ApiModelProperty("来源表表名")
    private List<String> fromtableName;


}
