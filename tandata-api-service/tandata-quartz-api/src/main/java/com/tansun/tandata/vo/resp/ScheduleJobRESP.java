package com.tansun.tandata.vo.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.util.Date;

/**
 * 任务表信息
 * @author LinLaiChun
 * @date 2020-06-04 11:02:04
 * @Version v1.0
 */
@Data
@ApiModel(value="任务表信息")
public class ScheduleJobRESP implements Serializable {
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "主键")
	private String id;

	@ApiModelProperty(value = "任务名称")
	private String jobName;

	@ApiModelProperty(value = "任务状态(PAUSED暂停、NORMAL运行)")
	private String jobStatus;

	@ApiModelProperty(value = "任务分组")
	private String jobGroup;

	@ApiModelProperty(value = "表达式")
	private String cronExpression;

	@ApiModelProperty(value = "描述")
	private String description;

	@ApiModelProperty(value = "调用类型（0类调用1服务调用）")
	private Integer type;

	@ApiModelProperty(value = "调用服务名/调用类")
	private String beanName;

	@ApiModelProperty(value = "调用url/类中方法")
	private String methodName;

	@ApiModelProperty(value = "创建者ID")
	private String creatorId;

	@ApiModelProperty(value = "创建时间")
	private Date createTime;

	@ApiModelProperty(value = "创建者名称")
	private String creatorName;

	@ApiModelProperty(value = "修改者ID")
	private String modifierId;

	@ApiModelProperty(value = "修改时间")
	private Date modifyTime;

	@ApiModelProperty(value = "修改者名称")
	private String modifierName;

}