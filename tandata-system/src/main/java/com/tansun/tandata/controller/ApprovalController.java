package com.tansun.tandata.controller;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.tansun.tandata.common.TandataServerEnum;
import com.tansun.tandata.entity.Approval;
import com.tansun.tandata.entity.LogInfoErro;
import com.tansun.tandata.service.ApprovalService;
import com.tansun.tandata.service.LogService;
import com.tansun.tandata.utils.Constants;
import com.tansun.tandata.utils.JsonUtil;
import com.tansun.tandata.utils.ParentController;
import com.tansun.tandata.utils.ResponseBase;
import com.tansun.tandata.vo.req.ApprovalPageVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.*;

/**
 * @Description 审批人设置控制类

 * @CreateDate 20200426
 * @author shenxiaodong
 * @Version v1.0
 *
 */
@Api(value = "审批人设置")
@RestController
@RequestMapping(value = "/sys/approval")
public class ApprovalController extends ParentController {
    @Autowired
    private ApprovalService approvalService;
    @Autowired
    private LogService logService;

    @ApiOperation(value="审批人信息插入", notes="审批人信息插入")
    @RequestMapping(value = "/saveApproval", method = RequestMethod.POST)
    @Transactional(propagation= Propagation.REQUIRED, rollbackFor=Exception.class)
    public ResponseBase saveApproval(@Valid @RequestBody Approval approval, BindingResult bindingResult ) {
        ResponseBase rsult = null;
        try {
            rsult = approvalService.addApproval(approval);
        } catch (RuntimeException e) {
            e.printStackTrace();
            //保存错误日志
            LogInfoErro log = new LogInfoErro();
            log.setServerId(TandataServerEnum.One.getServerId());//tandata-sys
            log.setUrl(log.getServerId()+"/sys/approval/saveApproval");
            log.setParamStr(JsonUtil.toJsonObjectString(approval));
            log.setReqType(1);//POST
            log.setDesribe(e.getMessage());
            log.setSeverName("系统服务");
            log.setId(UUID.randomUUID().toString().replaceAll("-", ""));
            log.setCreateTime(new Date());
            logService.insertLogInfoErro(log);
            return setResult(Constants.HTTP_RES_CODE_500, localeMessage.getMessage("sys.common.error"), null); //系统错误
        }
        return rsult;
    }

    @ApiOperation(value="审批人信息修改", notes="审批人信息修改")
    @RequestMapping(value = "/updateApproval", method = RequestMethod.POST)
    @Transactional(propagation= Propagation.REQUIRED, rollbackFor=Exception.class)
    public ResponseBase updateApproval(@Valid @RequestBody Approval approval, BindingResult bindingResult ) {
        ResponseBase rsult = null;
        try {
            rsult = approvalService.updateApproval(approval);
        } catch (RuntimeException e) {
            e.printStackTrace();
            //保存错误日志
            LogInfoErro log = new LogInfoErro();
            log.setServerId(TandataServerEnum.One.getServerId());//tandata-sys
            log.setUrl(log.getServerId()+"/sys/approval/updateApproval");
            log.setParamStr(JsonUtil.toJsonObjectString(approval));
            log.setReqType(1);//POST
            log.setDesribe(e.getMessage());
            log.setSeverName("系统服务");
            log.setId(UUID.randomUUID().toString().replaceAll("-", ""));
            log.setCreateTime(new Date());
            logService.insertLogInfoErro(log);
            return setResult(Constants.HTTP_RES_CODE_500, localeMessage.getMessage("sys.common.error"), null); //系统错误
        }
        return rsult;
    }

    @ApiOperation(value="查询审批人权限列表", notes="查询审批人权限列表")
    @RequestMapping(value = "/getApplovalList", method = RequestMethod.POST)
    public ResponseBase getApplovalList(@Valid @RequestBody ApprovalPageVo vo) {
        Map<String, Object> data = new HashMap<>();
        try {
            Page<Object> pageInfo = PageHelper.startPage(vo.getPage(), vo.getLimit(), true);
            List<Approval> raiseNumList =approvalService.getApploval(vo);
            data.put("total", pageInfo.getTotal());
            data.put("data", raiseNumList);
        } catch (Exception e) {
            e.printStackTrace();
            //保存错误日志
            LogInfoErro log = new LogInfoErro();
            log.setServerId(TandataServerEnum.One.getServerId());//tandata-sys
            log.setUrl(log.getServerId()+"/sys/approval/getApplovalList");
            log.setParamStr(JsonUtil.toJsonObjectString(vo));
            log.setReqType(1);//POST
            log.setDesribe(e.getMessage());
            log.setSeverName("系统服务");
            log.setId(UUID.randomUUID().toString().replaceAll("-", ""));
            log.setCreateTime(new Date());
            logService.insertLogInfoErro(log);
            return setResult(Constants.HTTP_RES_CODE_500, localeMessage.getMessage("sys.common.error"), null);
        }
        return setResultSuccess(data);
    }
    @ApiOperation(value="物理删除审批人列", notes="物理删除审批人列")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "提数记录id", required = true, dataType = "String"),
    })
    @RequestMapping(value="deleteApploval", method=RequestMethod.POST)
    @Transactional(propagation= Propagation.REQUIRED, rollbackFor=Exception.class)
    public ResponseBase deleteApploval(String id, HttpServletRequest request){
        try {
            approvalService.deleteApproval(id);
        } catch (Exception e) {
            e.printStackTrace();
            //保存错误日志
            LogInfoErro log = new LogInfoErro();
            log.setServerId(TandataServerEnum.One.getServerId());//tandata-sys
            log.setUrl(log.getServerId()+"/sys/approval/deleteApploval");
            log.setParamStr("id:"+id);
            log.setReqType(1);//POST
            log.setDesribe(e.getMessage());
            log.setSeverName("系统服务");
            log.setId(UUID.randomUUID().toString().replaceAll("-", ""));
            log.setCreateTime(new Date());
            logService.insertLogInfoErro(log);
            return setResult(Constants.HTTP_RES_CODE_500, localeMessage.getMessage("sys.common.error"), null);
        }
        return setResultSuccess();
    }

    @ApiOperation(value="给用户授予业务人员或审批人员权限", notes="给用户授予业务人员或审批人员权限")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "loginName", value = "登录名称", required = true, dataType = "String"),
    })
    @RequestMapping(value="authUserApprovalRole", method=RequestMethod.GET)
    @Transactional(propagation= Propagation.REQUIRED, rollbackFor=Exception.class)
    public ResponseBase authUserApprovalRole(String loginName, HttpServletRequest request){
        ResponseBase result = null;
        try {
            result=  approvalService.authUserApprovalRole(loginName);
        } catch (Exception e) {
            e.printStackTrace();
            //保存错误日志
            LogInfoErro log = new LogInfoErro();
            log.setServerId(TandataServerEnum.One.getServerId());//tandata-sys
            log.setUrl(log.getServerId()+"/sys/approval/authUserApprovalRole");
            log.setParamStr("loginName:"+loginName);
            log.setReqType(0);//GET
            log.setDesribe(e.getMessage());
            log.setSeverName("系统服务");
            log.setId(UUID.randomUUID().toString().replaceAll("-", ""));
            log.setCreateTime(new Date());
            logService.insertLogInfoErro(log);
            return setResult(Constants.HTTP_RES_CODE_500, localeMessage.getMessage("sys.common.error"), null);
        }
        return result;
    }


}
