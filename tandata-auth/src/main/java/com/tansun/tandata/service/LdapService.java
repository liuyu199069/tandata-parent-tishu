package com.tansun.tandata.service;

import com.tansun.tandata.vo.LdapUser;

import java.util.List;

public interface LdapService {
     public LdapUser ldapAuth(String username, String password);
     public List<LdapUser> seachLdapUserByKeywords(String keyWords);

}
