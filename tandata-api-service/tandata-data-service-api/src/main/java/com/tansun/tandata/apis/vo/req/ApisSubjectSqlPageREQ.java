package com.tansun.tandata.apis.vo.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.util.Date;

/**
 * 分页查询主题sql文本
 * @author LinLaiChun
 * @date 2020-06-10 15:08:46
 * @Version v1.0
 */
@Data
@ApiModel(value="分页查询主题sql文本")
public class ApisSubjectSqlPageREQ implements Serializable {
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "ID", required = false)
	private String id;

	@ApiModelProperty(value = "所属主题（主题表）", required = false)
	private String subjectId;

	@ApiModelProperty(value = "sql文本", required = false)
	private String sqlTxt;

	@ApiModelProperty(value = "排序语句",required = false)
	private String orderBy;

	@ApiModelProperty(value = "当前页码",required = true)
	private Integer currentPage;

	@ApiModelProperty(value = "单页数据量",required = true)
	private Integer pageSize;
}