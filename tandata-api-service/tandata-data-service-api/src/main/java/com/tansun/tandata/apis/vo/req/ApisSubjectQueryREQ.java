package com.tansun.tandata.apis.vo.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 查询接口主题信息
 * @author LinLaiChun
 * @date 2020-06-10 15:08:46
 * @Version v1.0
 */
@Data
@ApiModel(value="查询接口主题信息")
public class ApisSubjectQueryREQ implements Serializable {
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "主题名称", required = false)
	private String name;

	@ApiModelProperty(value = "父主题ID", required = false)
	private String parentId;

	@ApiModelProperty(value = "类型（0服务、1主题）", required = false)
	private Integer type;

	@ApiModelProperty(value = "排序语句", required = false)
	private String orderBy;
}