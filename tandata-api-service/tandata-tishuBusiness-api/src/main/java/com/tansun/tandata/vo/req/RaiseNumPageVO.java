package com.tansun.tandata.vo.req;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@ApiModel(
        value = "传入RaiseNumRaiseNumPage对象",
        description = "提数记录对象RaiseNum"
)
@Data
public class RaiseNumPageVO implements Serializable {
    @ApiModelProperty(
            value = "标题",
            example = "测试提数1"
    )
    private String title;
    @ApiModelProperty(
            value = "序号",
            example = "测试提数1"
    )
    private String raiseNo;
    @ApiModelProperty(
            value = "发起人",
            example = "select * from T1"
    )
    private String raiseUsercode;
    @ApiModelProperty(
            value = "提数开始时间开始",
            example = "2020-09-30 09:08:05"
    )
    private String beginTime_beg;
    @ApiModelProperty(
            value = "提数开始时间结束",
            example = "2020-09-30 12:08:05"
    )
    private String beginTime_end;
    @ApiModelProperty(
            value = "状态",
            example = "1"
    )
    private String state;
    @ApiModelProperty(
            value = "当前页",
            example = "1"
    )
    private int page;
    @ApiModelProperty(
            value = "显示数量",
            example = "30"
    )
    private int limit;

}
