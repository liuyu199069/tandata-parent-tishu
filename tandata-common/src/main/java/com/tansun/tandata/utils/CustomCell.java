package com.tansun.tandata.utils;

/**
 * @Description
 * @Author will
 * @Date 2019/3/15 0015 下午 15:41
 * @
 */
public class CustomCell {

    /**
     * 行号
     */
    private int rowNum;
    /**
     * 列号
     */
    private int colNum;
   
    /**
     * 填充值
     */
    private String value;

    public CustomCell(int rowNum, int colNum,String value) {
        this.rowNum = rowNum;
        this.colNum = colNum;
        this.value = value;
    }

    public int getRowNum() {
        return rowNum;
    }

    public CustomCell setRowNum(int rowNum) {
        this.rowNum = rowNum;
        return this;
    }

	public int getColNum() {
		return colNum;
	}

	public void setColNum(int colNum) {
		this.colNum = colNum;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
