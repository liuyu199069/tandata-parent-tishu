package com.tansun.tandata.apis.vo.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * 新增主题sql文本
 * @author LinLaiChun
 * @date 2020-06-10 15:08:46
 * @Version v1.0
 */
@Data
@ApiModel(value="新增主题sql文本")
public class ApisSubjectSqlSaveREQ implements Serializable {
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "ID", required = false)
	@Size(max = 70)
	private String id;

	@ApiModelProperty(value = "所属主题（主题表）", required = true)
	@NotBlank
	@Size(max = 70)
	private String subjectId;

	@ApiModelProperty(value = "sql文本", required = false)
	private String sqlTxt;

}