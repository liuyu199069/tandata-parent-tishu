package com.tansun.tandata.entity;

import com.tansun.tjdp.orm.entity.BaseEntity;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * 服务配置
 * @author LinLaiChun
 * @date 2020-06-09 15:55:26
 * @Version v1.0
 */
@Entity
@Data
@Table(name="quartz_server")
public class QuartzServer extends BaseEntity<QuartzServer, String> implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	 * 主键
	 */
	@Id
	@Column(name = "id")
	private String id;

	/**
	 * 服务名
	 */
	@Column(name = "service_name")
	private String serviceName;

	/**
	 * 服务链接
	 */
	@Column(name = "service_url")
	private String serviceUrl;




}