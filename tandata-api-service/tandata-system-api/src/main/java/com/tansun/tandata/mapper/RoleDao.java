package com.tansun.tandata.mapper;

import java.util.List;

import com.tansun.tandata.entity.Role;
import com.tansun.tandata.entity.RoleMenu;
import com.tansun.tandata.entity.User;
import com.tansun.tandata.vo.res.RoleMenuVO;
import com.tansun.tjdp.orm.persistence.BaseDao;

/**
 * @Description 角色信息接口
 * @CreateDate 20200428
 * @author shenxiaodong
 * @Version v1.0
 *
 */
public interface RoleDao extends BaseDao<Role> {
	/**
	 * 查询所有角色信息
	 * @return
	 */
	List<RoleMenuVO> selectAllRole(String roleName);
	
	/**
	 * 根据角色编号删除角色信息
	 */
	void deleteRoleById(String roleCode);
	
	
	/**
	 * 根据角色编号删除用户角色信息
	 */
	void deleteUserRoleById(String roleCode);
	
	/**
	 * 根据角色编号删除角色菜单信息
	 */
	void deleteRoleMenuById(String roleCode);
	
	/**
	 * 添加角色信息
	 */
	void saveRole(Role role);
	
	/**
	 * 添加角色菜单信息
	 */
	void saveRoleMenu(RoleMenu rm);
	
	/**
	 * 根据角色名查询角色是否存在
	 */
	int getRoleByName(String roleName);
	
	/**
	 * 修改角色信息
	 */
	void updateRole(Role role);
	
	
	/**
	 * 根据角色id查询角色
	 */
	RoleMenuVO getRoleById(String role);

	/**
	 * 根据用户编号查询对应角色
	 */
    List<RoleMenuVO> selectRolesByUserCode(String userCode);
}
