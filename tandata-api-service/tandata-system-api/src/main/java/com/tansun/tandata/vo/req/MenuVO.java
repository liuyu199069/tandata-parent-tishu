package com.tansun.tandata.vo.req;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Description 菜单实体类
 * @CreateDate 20200426
 * @author shenxiaodong
 * @Version v1.0
 *
 */

@ApiModel(value="传入菜单对象",description="菜单对象")
@Data
public class MenuVO implements Serializable{
	private static final long serialVersionUID = 1188583090334390577L;
	
	private String id;

	private String prntId;

	@ApiModelProperty(value="菜单中文名称", name="fnChnNm")
	@Size(max=100,message="{sys.CompanyVO.companySocialCode.lengthError}")
	@NotNull(message="{sys.CompanyVO.companySocialCode.notNullError}")
	private String fnChnNm;

	@ApiModelProperty(value="菜单英文名称", name="fnEngNm")
	@Size(max=100,message="{sys.CompanyVO.companySocialCode.lengthError}")
	@NotNull(message="{sys.CompanyVO.companySocialCode.notNullError}")
	private String fnEngNm;

	@ApiModelProperty(value="菜单访问路径", name="fnEngNm")
	private String rsrcNo;

	@ApiModelProperty(value="菜单功能分类，1路由菜单，2api菜单", name="fnCtgrCd")
	private String fnCtgrCd;
}
