package com.tansun.tandata.vo.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.util.Date;

/**
 * 编辑
 * @author wzf
 * @date 2020-06-09 16:26:22
 * @Version v1.0
 */
@Data
@ApiModel(value="编辑")
public class OrgInfoUpdateVO implements Serializable {
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "机构编码", required = true)
	private String orgCode;

	@ApiModelProperty(value = "机构名称", required = false)
	private String orgName;

	@ApiModelProperty(value = "上一级机构节点", required = false)
	private String parentCode;

	@ApiModelProperty(value = "机构类型", required = false)
	private Integer type;

}