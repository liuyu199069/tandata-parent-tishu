package com.tansun.tandata.vo.res;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @Description 返回前台菜单实体类
 * @CreateDate 20200426
 * @author shenxiaodong
 * @Version v1.0
 *
 */

@ApiModel(value="User对象带有父菜单名称",description="用户对象user带有父菜单名称")
@Data
public class MenuWithParentVO implements Serializable{
	private static final long serialVersionUID = 1188583090334390577L;
	
	private String id;

	@ApiModelProperty(value="父菜单id", name="prntId")
	private String prntId;

	@ApiModelProperty(value="父菜单中文名称", name="prntFnChnNm")
	private String prntFnChnNm;

	@ApiModelProperty(value="菜单中文名称", name="fnChnNm")
	private String fnChnNm;

	@ApiModelProperty(value="菜单英文名称", name="fnEngNm")
	private String fnEngNm;

	@ApiModelProperty(value="菜单访问路径", name="fnEngNm")
	private String rsrcNo;

	@ApiModelProperty(value="菜单功能分类，1路由菜单，2api菜单", name="fnCtgrCd")
	private String fnCtgrCd;
}
