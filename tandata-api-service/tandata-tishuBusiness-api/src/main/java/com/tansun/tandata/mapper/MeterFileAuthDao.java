package com.tansun.tandata.mapper;


import com.tansun.tandata.entity.MeterFileAuth;
import com.tansun.tandata.entity.RaiseNumReceiveUser;
import com.tansun.tandata.vo.req.MeterFileAuthPageVO;
import com.tansun.tjdp.orm.persistence.BaseDao;

import java.util.List;

public interface MeterFileAuthDao extends BaseDao<MeterFileAuth> {
    int deleteByPrimaryKey(String id);

    int insert(MeterFileAuth record);

    int insertSelective(MeterFileAuth record);

    MeterFileAuth selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(MeterFileAuth record);

    int updateByPrimaryKey(MeterFileAuth record);

    List<MeterFileAuth> selectMeterFileAuthList(MeterFileAuthPageVO meterFileAuthPageVO);

    List<MeterFileAuth> selectMeterFileAuthList2(MeterFileAuthPageVO meterFileAuthPageVO);
}