package com.tansun.tandata.entity;

import com.tansun.tjdp.orm.entity.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(
        name = "BUS_RAISENUM_RECEIVEUSER"
)
public class RaiseNumReceiveUser extends BaseEntity<RaiseNumReceiveUser, String> implements Serializable {
    private static final long serialVersionUID = -2701052307289939921L;
    /**
     * ID
     */
    @Id
    @Column(name = "ID")
    private String id;
    /**
     * 提数记录id
     */
    @Id
    @Column(name = "RAISENUM_ID")
    private String raisenumId;
    /**
     * 业务人员域账户
     */
    @Id
    @Column(name = "USER_NAME")
    private String userName;
    /**
     * 业务人员姓名
     */
    @Id
    @Column(name = "USER_NICKNAME")
    private String userNickname;
    /**
     * 状态（0：未保存处理结果；1：处理结果保存中；2：处理结果保存完毕；3：处理结果保存失败）
     */
    @Id
    @Column(name = "STATE")
    private Integer state;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRaisenumId() {
        return raisenumId;
    }

    public void setRaisenumId(String raisenumId) {
        this.raisenumId = raisenumId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserNickname() {
        return userNickname;
    }

    public void setUserNickname(String userNickname) {
        this.userNickname = userNickname;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }
}