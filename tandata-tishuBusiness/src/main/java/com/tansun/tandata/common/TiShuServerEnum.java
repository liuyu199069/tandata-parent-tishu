package com.tansun.tandata.common;

import java.util.HashMap;
import java.util.Map;

public enum TiShuServerEnum {
	One("rais-num","提数管理"),Two("user-meter","我的查询统计权限"),Three("meter-auth","仪表板下载授权管理");;
    private String serverId;

    private String serverName;

    public String getServerId() {
        return serverId;
    }

    public String getServerName() {
        return serverName;
    }

    private TiShuServerEnum(String serverId, String serverName){
        this.serverId = serverId;
        this.serverName = serverName;
    }
    
    public static final Map<String,String> map = new HashMap<String,String>();
    
    //通过number获取对应的name
    public static String getDescription(String serverId){
        return map.get(serverId);
    }
    
    static{
        for(TiShuServerEnum tandataServerEnum : TiShuServerEnum.values()){
            map.put(tandataServerEnum.getServerId(), tandataServerEnum.getServerName());
        }
    }
}
