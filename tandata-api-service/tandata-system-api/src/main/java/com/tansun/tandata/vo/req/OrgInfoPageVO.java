package com.tansun.tandata.vo.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.util.Date;

/**
 * 分页查询
 * @author wzf
 * @date 2020-06-09 16:26:22
 * @Version v1.0
 */
@Data
@ApiModel(value="分页查询")
public class OrgInfoPageVO implements Serializable {
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "机构编码", required = false)
	private String orgCode;

	@ApiModelProperty(value = "机构名称", required = false)
	private String orgName;

	@ApiModelProperty(value = "上一级机构节点", required = false)
	private String parentCode;

	@ApiModelProperty(value = "机构类型", required = false)
	private Integer type;

	@ApiModelProperty(value = "创建者code", required = false)
	private String createrId;

	@ApiModelProperty(value = "", required = false)
	private Date createrTime;

	@ApiModelProperty(value = "修改者ID", required = false)
	private String updateId;

	@ApiModelProperty(value = "修改时间", required = false)
	private Date updateTime;

	@ApiModelProperty(value = "修改次数", required = false)
	private Integer updateCount;

	@ApiModelProperty(value = "排序语句",required = false)
	private String orderBy;

	@ApiModelProperty(value = "当前页码",required = true)
	private Integer currentPage;

	@ApiModelProperty(value = "单页数据量",required = true)
	private Integer pageSize;
}