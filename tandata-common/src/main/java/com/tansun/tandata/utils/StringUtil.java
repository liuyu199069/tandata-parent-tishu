package com.tansun.tandata.utils;

import com.tansun.tjdp.orm.util.IdUtil;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @description: 字符串工具类
 * @author: linLaiChun
 * @create: 2020-05-19
 * @Version v1.0
 **/
public class StringUtil {
    private static Pattern linePattern = Pattern.compile("_(\\w)");

    /**
     * 判断是否为空字符串最优代码
     * @param str
     * @return 如果为空，则返回true
     */
    public static boolean isEmpty(String str){
        return str == null || str.trim().length() == 0;
    }

    /**
     * 判断字符串是否非空
     * @param str 如果不为空，则返回true
     * @return
     */
    public static boolean isNotEmpty(String str){
        return !isEmpty(str);
    }

    /**
     * 下划线转驼峰
     * @param str
     * @return
     */
    public static String lineToHump(String str) {
        str = str.toLowerCase();
        Matcher matcher = linePattern.matcher(str);
        StringBuffer sb = new StringBuffer();
        while (matcher.find()) {
            matcher.appendReplacement(sb, matcher.group(1).toUpperCase());
        }
        matcher.appendTail(sb);
        return sb.toString();
    }

    private static Pattern humpPattern = Pattern.compile("[A-Z]");

    /**
     * 驼峰转下划线
     * @param str
     * @return
     */
    public static String humpToLine(String str) {
        Matcher matcher = humpPattern.matcher(str);
        StringBuffer sb = new StringBuffer();
        while (matcher.find()) {
            matcher.appendReplacement(sb, "_" + matcher.group(0).toLowerCase());
        }
        matcher.appendTail(sb);
        return sb.toString();
    }

    /**
     * 字符串拼接  拼接格式：'a','b','c'
     * @param str
     * @return
     */
    public static String getParamString(String str){
        if(str == null || str.equals("")){
            return str;
        }
        String[] strp = str.split(",");
        StringBuffer strbuff = new StringBuffer();
        for(int i = 0;i<strp.length;i++){
            if(i == strp.length-1){
                strbuff.append("'");
                strbuff.append(strp[i]);
                strbuff.append("'");
            }else{
                strbuff.append("'");
                strbuff.append(strp[i]);
                strbuff.append("',");
            }
        }
        return strbuff.toString();
    }
    public static String getParamString(String str,String dividers){
        if(str == null || str.equals("")){
            return str;
        }
        String[] strp = str.split(",");
        StringBuffer strbuff = new StringBuffer();
        for(int i = 0;i<strp.length;i++){
            if(i == strp.length-1){
                strbuff.append("'");
                String s = getParamRemoveString(strp[i],dividers);
                strbuff.append(s);
                strbuff.append("'");
            }else{
                strbuff.append("'");
                String s = getParamRemoveString(strp[i],dividers);
                strbuff.append(s);
                strbuff.append("',");
            }
        }
        return strbuff.toString();
    }

    /**
     * 字符串分割  根据分割符将字符串进行分割，并保留分割符后的字符串
     * @param str
     * @return
     */
    public static String getParamRemoveString(String str,String dividers){
        if(str == null || str.equals("")){
            return str;
        }
        int index = str.indexOf(dividers);
        if(index!=-1){
            str = str.substring(index+1,str.length());
        }
        return str;
    }

    /**
     * 字符串截取
     * @param str
     * @return
     */
    public static String getInterceptintercept(String str,int len){
        if(str.length()>len){
            str = str.substring(0,len);
        }
        return str;
    }

    /**
     * 获取一个随机字符串（6位）
     * @param
     * @return str
     */
    public static String getRandom(){
        String str = "";
        str = IdUtil.generateUUId().substring(0,6);
        return str;
    }

    /**
     * 版本累加
     * @param str
     * @return str
     */
    public static String versionAccumulation(String str){
        if(str.indexOf(".")!=-1){
            String[] strp = str.split("[.]");
            String versionStr = "";
            for(String param:strp){
                versionStr = versionStr + param;
            }
            if(isInteger(versionStr)){
                String vStr = "";
                int versionInt = Integer.valueOf(versionStr);
                versionInt++;
                int num;
                while (versionInt!=0){
                    num=versionInt%10; //获取每一位
                    versionInt=versionInt/10;	//整数退一位
                    vStr = "."+String.valueOf(num) + vStr;
                }
                return vStr.substring(1);
            }else{
                return "";
            }
        }else{
            return "";
        }
    }

    /*
     * 判断是否为整数
     * @param str 传入的字符串
     * @return 是整数返回true,否则返回false
     */
    public static boolean isInteger(String str) {
        Pattern pattern = Pattern.compile("^[-\\+]?[\\d]*$");
        return pattern.matcher(str).matches();
    }

    /**
     * 判断字符串中是否包含中文
     * @param str
     * 待校验字符串
     * @return 是否为中文
     * @warn 不能校验是否为中文标点符号
     */
    public static boolean isContainChinese(String str) {
        Pattern p = Pattern.compile("[\u4e00-\u9fa5]");
        Matcher m = p.matcher(str);
        if (m.find()) {
            return true;
        }
        return false;
    }

    public static void main(String[] args) {
        String lineToHump = lineToHump("f_parent_no_leader");
        System.out.println(lineToHump);// fParentNoLeader
        System.out.println(humpToLine(lineToHump));// f_parent_no_leader
        System.out.println(versionAccumulation("内度"));
    }

}
