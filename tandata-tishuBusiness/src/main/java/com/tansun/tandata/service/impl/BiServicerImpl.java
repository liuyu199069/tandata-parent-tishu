package com.tansun.tandata.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.tansun.tandata.service.BiServicer;
import com.tansun.tandata.utils.HttpUtil;
import com.tansun.tandata.utils.JsonUtil;
import com.tansun.tandata.vo.req.bi.AddBiTableListVo;
import com.tansun.tandata.vo.req.bi.AddBiTableVo;
import com.tansun.tandata.vo.req.bi.CarrierTableUserVo;
import com.tansun.tandata.vo.res.BIMeter;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @Description 调用BI接口业务类

 * @CreateDate 20200907
 * @author liuyu
 * @Version v1.0
 *
 */
@Service("biServicerImpl")
public class BiServicerImpl implements BiServicer {

    @Value("${funyuanbi.addtable.packId}")
    private String packId;
    @Value("${funyuanbi.connectionName}")
    private String connectionName;
    @Value("${funyuanbi.baseUrl}")
    private String biBaseUrl;
    private org.slf4j.Logger logger= LoggerFactory.getLogger(this.getClass());
    /**
     * @Description 添加BI系统基础表
     * @CreateDate 20200924
     * @author liuyu
     * @Version v1.0
     */
    @Override
    public JSONObject addBiTable(String tableName) {
       //组装参数
        List<AddBiTableListVo> tableList = new ArrayList<>();
        AddBiTableListVo addBiTableListVo = new AddBiTableListVo();
        addBiTableListVo.setConnectionName(connectionName);
        addBiTableListVo.setTableName(tableName);
        tableList.add(addBiTableListVo);

        AddBiTableVo obj = new AddBiTableVo();
        obj.setPackId(packId);
        obj.setTables(tableList);

        String pameter = JsonUtil.toJsonObjectString(obj);
        logger.error("-------添加BI系统基础表URL------"+biBaseUrl+"dynamic/packs/table/add");
        logger.error("-------添加BI系统基础表传入参数------"+pameter);
       //发送http请求调用bi接口
        return HttpUtil.doPostJson(biBaseUrl+"dynamic/packs/table/add",pameter,true);
    }
    /**
     * @Description 用户授权操作基础表
     * @CreateDate 20200924
     * @author liuyu
     * @Version v1.0
     */
    @Override
    public JSONObject carrierTableUser(List<CarrierTableUserVo> list) {
        //组装参数
        JSONObject obj = new JSONObject();
        obj.put("list",list);
        //发送http请求调用bi接口
        logger.error("-------------调用bi同步接口开始传入参数-----------"+obj.toJSONString());
        return HttpUtil.doPostJson(biBaseUrl+"dynamic/carrier/user",obj.toJSONString(),true);
    }
    /**
     * @Description 获取用户仪表板列表
     * @CreateDate 20200924
     * @author liuyu
     * @Version v1.0
     */
    @Override
    public List<BIMeter> getDashboardList(String userName) {
        //组装参数
        //组装参数
        JSONObject obj = new JSONObject();
        obj.put("userName",userName);
        //发送http请求调用bi接口
        logger.error("------------拉取bi系统我的仪表板-----------"+obj.toJSONString());
        JSONObject result =HttpUtil.doPostJson(biBaseUrl+"dynamic/platform/dashboard/list",obj.toJSONString(),true);
        logger.error("------------拉取bi系统我的仪表板成功-----------"+result.toJSONString());
        List<BIMeter> list = new ArrayList<>();
        if(result!=null&&result.getBoolean("success")){
            JSONArray array = result.getJSONArray("data");
            logger.error("------------array-----------"+array.toJSONString());
            if(array!=null&&array.size()>0){
                list.addAll(array.toJavaList(BIMeter.class));
                logger.error("------------拉取bi系统我的仪表板成功-----------"+list.get(0).getMeterName());
            }
        }
        logger.error("------------拉取bi系统我的仪表板成功-----------"+result.toJSONString());
        return list;
    }

}
