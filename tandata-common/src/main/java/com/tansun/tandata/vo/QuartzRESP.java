package com.tansun.tandata.vo;

import com.tansun.tandata.utils.Constants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 计划任务执行返回数据
 * @author LinLaiChun
 * @date 2020-06-08 15:41:04
 * @Version v1.0
 */
@Data
@ApiModel(value="计划任务执行返回数据")
public class QuartzRESP implements Serializable {
	private static final long serialVersionUID = 1L;

	public static String STATUS_SUCCESS = Constants.QUARTZ_JOB_RUN_STATUS_SUCCESS;
	public static String STATUS_ERROR = Constants.QUARTZ_JOB_RUN_STATUS_ERROR;

	@ApiModelProperty(value = "描述")
	private String desc;

	@ApiModelProperty(value = "开始")
	private Date startTime;

	@ApiModelProperty(value = "任务结束时间")
	private Date endTime;

	@ApiModelProperty(value = "执行状态(SUCCESS 成功、ERROR失败)")
	private String runStatus;

}