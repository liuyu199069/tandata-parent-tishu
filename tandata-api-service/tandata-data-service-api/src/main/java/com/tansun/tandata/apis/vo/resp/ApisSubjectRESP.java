package com.tansun.tandata.apis.vo.resp;

import com.tansun.tandata.utils.DateUtils;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 接口主题信息信息
 * @author LinLaiChun
 * @date 2020-06-10 15:08:46
 * @Version v1.0
 */
@Data
@ApiModel(value="接口主题信息信息")
public class ApisSubjectRESP implements Serializable {
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "ID")
	private String id;

	@ApiModelProperty(value = "主题名称")
	private String name;

	@ApiModelProperty(value = "父主题ID")
	private String parentId;

	@ApiModelProperty(value = "类型（0服务、1主题）")
	private Integer type;

	@ApiModelProperty(value = "主题描述")
	private String remark;

	@ApiModelProperty(value = "排序字段")
	private Integer sort;

	@ApiModelProperty(value = "创建者ID")
	private String creatorId;

	@ApiModelProperty(value = "创建者名称")
	private String creatorName;

	@ApiModelProperty(value = "创建时间")
	private String createTime;

	@ApiModelProperty(value = "修改者ID")
	private String modifierId;

	@ApiModelProperty(value = "修改者名称")
	private String modifierName;

	@ApiModelProperty(value = "修改时间")
	private String modifyTime;

	public void setCreateTime(Date createTime) {
		if(createTime!=null){
			this.createTime = DateUtils.formatDate(DateUtils.DATE_TO_STRING_DETAIAL_PATTERN,createTime);
		}
	}

	public void setModifyTime(Date modifyTime) {
		if(modifyTime!=null){
			this.modifyTime = DateUtils.formatDate(DateUtils.DATE_TO_STRING_DETAIAL_PATTERN,modifyTime);
		}
	}
}