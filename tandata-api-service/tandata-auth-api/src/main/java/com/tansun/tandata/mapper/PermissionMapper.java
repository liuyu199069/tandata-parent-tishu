package com.tansun.tandata.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.tansun.tandata.entity.Permission;

public interface PermissionMapper{
	/**
	 * @Description 查询权限通过角色ID
	 * @param roleIds
	 * @return
	 */
	public List<Permission> selectPermissionListByRoleIds(@Param("roleIds") List<Integer> roleIds);
}
