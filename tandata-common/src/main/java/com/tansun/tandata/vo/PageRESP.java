package com.tansun.tandata.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @description: 分页查询返回
 * @author: linLaiChun
 * @create: 2020-05-08
 * @Version v1.0
 **/

@Data
@ApiModel(value="分页查询返回")
public class PageRESP<T> implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty("总数据量")
    private Long totalRow;

    @ApiModelProperty(value = "当前页码")
    private Integer currentPage;

    @ApiModelProperty(value = "单页数据量")
    private Integer pageSize;

    @ApiModelProperty("数据集合")
    private List<T> pageList;
}
