package com.tansun.tandata.feign;

import com.tansun.tandata.entity.LogInfoErro;
import com.tansun.tandata.vo.LdapUser;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.tansun.tandata.fallback.SystemFeignClientFallback;
import com.tansun.tandata.feign.config.FeignLogConfig;
import com.tansun.tandata.utils.ResponseBase;

import javax.validation.Valid;


@FeignClient(value = "tandata-sys", configuration = FeignLogConfig.class, fallback = SystemFeignClientFallback.class)
public interface SysFeignService {

	@RequestMapping(value = "/sys/user/getUserAndRoleInfoByLoginName",method=RequestMethod.GET)
	public ResponseBase getUserAndRoleInfoByLoginName(@RequestParam("loginName")String loginName);
	
	@RequestMapping(value = "/sys/menu/getRoleAndApis",method=RequestMethod.GET)
	public ResponseBase getRoleAndApis();
	
	@RequestMapping(value = "/sys/orgInfo/getChildOrgByOrgCode",method=RequestMethod.GET)
	public ResponseBase getChildOrgByOrgCode(@RequestParam("orgCode") String orgCode);

	@RequestMapping(value = "/sys/user/ldapUserToSysUser",method=RequestMethod.POST)
	public ResponseBase ldapUserToSysUser(@RequestBody LdapUser ldapUser);

	@RequestMapping(value = "/sys/user/lockLdapUserByLoginName",method=RequestMethod.GET)
	public ResponseBase lockLdapUserByLoginName(@RequestParam("loginName") String loginName);

	@RequestMapping(value = "/sys/log/insertLogInfoErro",method=RequestMethod.POST)
	public ResponseBase insertLogInfoErro(@Valid @RequestBody LogInfoErro logInfoErro);

	@RequestMapping(value = "/sys/approval/authUserApprovalRole",method=RequestMethod.GET)
	public ResponseBase authUserApprovalRole(@RequestParam("loginName")String loginName);
}
