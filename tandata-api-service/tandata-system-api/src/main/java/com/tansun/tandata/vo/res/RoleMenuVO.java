package com.tansun.tandata.vo.res;

import com.tansun.tandata.entity.Menu;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 角色菜单
 * @author wzf
 * @date 2020-06-09 16:26:22
 * @Version v1.0
 */
@Data
@ApiModel(value="角色菜单")
public class RoleMenuVO implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "角色编号")
	private String roleCode;

	@ApiModelProperty(value = "角色名称")
	private String roleName;

	@ApiModelProperty(value = "角色描述")
	private String roleDesc;

	@ApiModelProperty(value = "创建人")
	private String creater;

	@ApiModelProperty(value = "创建时间")
	private Date createTime;

	@ApiModelProperty(value = "修改人")
	private String updater;

	@ApiModelProperty(value = "修改时间")
	private Date updateTime;

	@ApiModelProperty(value = "是否删除")
	private Integer isDelete;

	@ApiModelProperty(value = "乐观锁")
	private Integer updateCount;

	@ApiModelProperty(value = "对应菜单集合")
	private List<Menu> menus;

}