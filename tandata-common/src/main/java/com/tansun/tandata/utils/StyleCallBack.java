package com.tansun.tandata.utils;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Workbook;

/**
 * @Description 样式回调接口
 * @Author will
 * @Date 2019/04/23
 */
public interface StyleCallBack {

    CellStyle applyCellStyle(Workbook workbook);
}
