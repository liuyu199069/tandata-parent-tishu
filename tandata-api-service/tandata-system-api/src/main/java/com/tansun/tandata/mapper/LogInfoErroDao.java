package com.tansun.tandata.mapper;

import com.tansun.tandata.entity.LogInfoErro;
import com.tansun.tandata.vo.req.LogInfoSeachErroVo;

import java.util.List;


public interface LogInfoErroDao {
	/**
	 * 查询日志信息
	 */
	public List<LogInfoErro> selectLogInfoErroList(LogInfoSeachErroVo logInfoSeachErroVo);
	/**
	 * 插入日志信息
	 *
	 */
	public void insertLogInfoErro(LogInfoErro logInfoErro);
}
