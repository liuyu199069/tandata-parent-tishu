package com.tansun.tandata.service;

import com.tansun.tandata.vo.req.RoleVO;
import com.tansun.tandata.vo.res.RoleMenuVO;

import java.util.List;

public interface RoleService {

    /**
     * 根据用户编号获取角色信息
     */
    List<RoleMenuVO> selectRolesByUserCode(String userCode);

    /**
     * 查询所有角色信息
     */
    List<RoleMenuVO> selectAllRole(String roleName);

    /**
     * 根据角色编号删除角色信息
     */
    void deleteRoleById(String roleCode);

    /**
     * 根据角色名查询角色
     */
    int getRoleByName(String roleName);

    /**
     * 添加角色信息
     */
    void saveRole(RoleVO roleVO);

    /**
     * 根据角色编号获取角色信息
     */
    RoleMenuVO getRoleById(String roleCode);

    /**
     * 修改角色信息
     */
    void updateRole(RoleVO roleVO);
}
