package com.tansun.tandata.vo.req.bi;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@ApiModel(
        value = "bi建表对象",
        description = "bi建表对象"
)
@Data
public class AddBiTableVo {
    @ApiModelProperty(
            value = "业务包id",
            example = "11111111"
    )
    private String packId;
    @ApiModelProperty(
            value = "建表对象集合"
    )
    private List<AddBiTableListVo> tables;

}
