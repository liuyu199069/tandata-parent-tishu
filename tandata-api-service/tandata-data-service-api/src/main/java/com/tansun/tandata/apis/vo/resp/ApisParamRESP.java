package com.tansun.tandata.apis.vo.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.util.Date;

/**
 * 接口参数信息配置信息
 * @author LinLaiChun
 * @date 2020-06-10 15:08:46
 * @Version v1.0
 */
@Data
@ApiModel(value="接口参数信息配置信息")
public class ApisParamRESP implements Serializable {
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "ID")
	private String id;

	@ApiModelProperty(value = "类型（请求参数或返回参数）")
	private Integer type;

	@ApiModelProperty(value = "所属接口（接口表）")
	private String interfaceId;

	@ApiModelProperty(value = "参数名称")
	private String paramName;

	@ApiModelProperty(value = "参数类型")
	private String paramType;

	@ApiModelProperty(value = "参数示例值")
	private String paramSampleValue;

	@ApiModelProperty(value = "参数默认值")
	private String paramDefaultValue;

	@ApiModelProperty(value = "参数测试值")
	private String paramTestValue;

	@ApiModelProperty(value = "参数描述")
	private String remark;

	@ApiModelProperty(value = "排序字段")
	private Integer sort;

	@ApiModelProperty(value = "创建者ID")
	private String creatorId;

	@ApiModelProperty(value = "创建时间")
	private Date createTime;

	@ApiModelProperty(value = "修改者ID")
	private String modifierId;

	@ApiModelProperty(value = "修改时间")
	private Date modifyTime;

}