package com.tansun.tandata.service;

import java.util.List;

import com.tansun.tandata.vo.req.MenuVO;
import com.tansun.tandata.vo.req.TreeVo;
import com.tansun.tandata.vo.res.MenuNewVO;
import com.tansun.tandata.vo.res.MenuWithParentVO;
import com.tansun.tandata.vo.res.RoleInfoByRoleCodesVO;

public interface MenuService {

    /**
     * 查询角色对应菜单信息
     */
    List<String> selectMenuByRoleCode(String roleCode);

    /**
     * 查询所有菜单信息
     */
    List<TreeVo> selectAllMenu();

    /**
     * 通过id获取菜单信息
     */
    void deleteMenuById(String id);

    /**
     * 根据名称获取菜单
     */
    int getMenuByChnNameOrEngName(String fnChnNm, String fnEngNm, String id);

    /**
     * 修改菜单
     */
    void updateMenu(MenuVO menuVO);

    /**
     * 新增菜单
     */
    void saveMenu(MenuVO menuVO);

    /**
     * 根据id查询菜单
     */
    MenuWithParentVO selectMenuWithParentInfoById(String id);

    /**
     * 查询角色对应菜单的api信息
     */
    List<RoleInfoByRoleCodesVO> selectMenuApiByRoleCode();

    /**
     * 根据角色编号查询菜单
     */
    List<MenuNewVO> getMenuInfosByRoleCodes(String roleCodes);
}
