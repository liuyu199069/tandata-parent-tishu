package com.tansun.tandata.mapper;

import com.tansun.tandata.entity.ScheduleJob;
import com.tansun.tandata.entity.ScheduleJobExample;
import com.tansun.tjdp.orm.persistence.BaseDao;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ScheduleJobMapper extends BaseDao<ScheduleJob> {
    int countByExample(ScheduleJobExample example);

    int deleteByExample(ScheduleJobExample example);

    int deleteByPrimaryKey(String id);

    void insert(ScheduleJob record);

    void insertSelective(ScheduleJob record);

    List<ScheduleJob> selectByExample(ScheduleJobExample example);

    ScheduleJob selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") ScheduleJob record, @Param("example") ScheduleJobExample example);

    int updateByExample(@Param("record") ScheduleJob record, @Param("example") ScheduleJobExample example);

    int updateByPrimaryKeySelective(ScheduleJob record);

    int updateByPrimaryKey(ScheduleJob record);
}