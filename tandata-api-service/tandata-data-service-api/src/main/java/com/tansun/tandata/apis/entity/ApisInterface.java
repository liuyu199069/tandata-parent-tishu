package com.tansun.tandata.apis.entity;

import java.io.Serializable;
import java.util.Date;
import com.tansun.tjdp.orm.entity.BaseEntity;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 接口信息配置
 * @author LinLaiChun
 * @date 2020-06-10 15:08:46
 * @Version v1.0
 */
@Entity
@Data
@Table(name="t_ds_apis_interface")
public class ApisInterface extends BaseEntity<ApisInterface, String> implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	 * ID
	 */
	@Id
	@Column(name = "id")
	private String id;

	/**
	 * 接口名称
	 */
	@Column(name = "name")
	private String name;

	/**
	 * 接口类型（sql、http）
	 */
	@Column(name = "type")
	private Integer type;

	/**
	 * 所属主题（主题表）
	 */
	@Column(name = "subject_id")
	private String subjectId;

	/**
	 * 所属项目
	 */
	@Column(name = "project")
	private String project;

	/**
	 * 接口路径
	 */
	@Column(name = "path")
	private String path;

	/**
	 * 请求方式 （字典表）
	 */
	@Column(name = "request_method")
	private String requestMethod;

	/**
	 * 协议 （字典表）
	 */
	@Column(name = "agreement")
	private String agreement;

	/**
	 * 返回类型 （字典表）
	 */
	@Column(name = "return_type")
	private String returnType;

	/**
	 * 接口描述
	 */
	@Column(name = "remark")
	private String remark;

	/**
	 * 排序字段
	 */
	@Column(name = "sort")
	private Integer sort;

	/**
	 * 创建者ID
	 */
	@Column(name = "creator_id")
	private String creatorId;

	/**
	 * 创建时间
	 */
	@Column(name = "create_time")
	private Date createTime;

	/**
	 * 修改者ID
	 */
	@Column(name = "modifier_id")
	private String modifierId;

	/**
	 * 修改时间
	 */
	@Column(name = "modify_time")
	private Date modifyTime;




}