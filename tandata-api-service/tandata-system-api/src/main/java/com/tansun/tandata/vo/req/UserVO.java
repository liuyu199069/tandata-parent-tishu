package com.tansun.tandata.vo.req;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Length;

import com.tansun.tandata.entity.Role;

import lombok.Data;




/**
 * @Description 用于用户实体类入参使用
 * @CreateDate 20200427
 * @author shenxiaodong
 * @Version v1.0
 *
 */

@ApiModel(value="传入User对象",description="用户对象user")
@Data
public class UserVO implements Serializable{
	
	private static final long serialVersionUID = -2701052307289939921L;

	@ApiModelProperty(value="用户编码", name="userCode")
	private String userCode;

	@ApiModelProperty(value="用户名称", name="userName")
	@Size(max=100,message="{sys.CompanyVO.companySocialCode.lengthError}")
	@NotNull(message="{sys.CompanyVO.companySocialCode.notNullError}")
	private String userName;

	@ApiModelProperty(value="登录名称")
	@Size(max=100,message="{sys.CompanyVO.companySocialCode.lengthError}")
	@NotNull(message="{sys.CompanyVO.companySocialCode.notNullError}")
	private String loginName;

	@ApiModelProperty(value="用户手机")
	@Pattern(regexp ="0?(13|14|15|18|17)[0-9]{9}",message="{sys.CompanyVO.companySocialCode.lengthError}")
	@NotNull(message="{sys.CompanyVO.companySocialCode.notNullError}")
	private String userPhone;

	@ApiModelProperty(value="登录密码")
	@Size(max=50,message="{sys.CompanyVO.companySocialCode.lengthError}")
	@NotNull(message="{sys.CompanyVO.companySocialCode.notNullError}")
	private String password;

	@ApiModelProperty(value="新密码")
	@Size(max=50, message="{sys.CompanyVO.companySocialCode.lengthError}")
	private String newPassword;

	@ApiModelProperty(value="用户类型")
	private String type;

	@ApiModelProperty(value="用户角色信息")
	private List<Role> roles;
}
