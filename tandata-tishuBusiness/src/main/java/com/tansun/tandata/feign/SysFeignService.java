package com.tansun.tandata.feign;

import com.tansun.tandata.entity.LogInfoErro;
import com.tansun.tandata.fallback.SystemFeignClientFallback;
import com.tansun.tandata.feign.config.FeignLogConfig;
import com.tansun.tandata.utils.ResponseBase;
import com.tansun.tandata.vo.LogInfoVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;


@FeignClient(value = "tandata-sys", configuration = FeignLogConfig.class, fallback = SystemFeignClientFallback.class)
public interface SysFeignService {

	@RequestMapping(value = "/sys/log/insertLogInfo",method=RequestMethod.POST)
	public ResponseBase insertLogInfo(@Valid @RequestBody LogInfoVO logInfoVO);

	@RequestMapping(value = "/sys/log/insertLogInfoErro",method=RequestMethod.POST)
	public ResponseBase insertLogInfoErro(@Valid @RequestBody LogInfoErro logInfoErro);
}
