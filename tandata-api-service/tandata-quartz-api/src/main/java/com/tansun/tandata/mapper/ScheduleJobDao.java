package com.tansun.tandata.mapper;

import com.tansun.tandata.entity.ScheduleJob;
import com.tansun.tjdp.orm.persistence.BaseDao;

import java.util.List;
import java.util.Map;

/**
 * 任务表Dao
 * @author LinLaiChun
 * @date 2020-06-04 11:02:04
 * @Version v1.0
 */
public interface ScheduleJobDao extends BaseDao<ScheduleJob> {

	int insert(ScheduleJob scheduleJob);

	int batchInsert(List<ScheduleJob> scheduleJobList);

	int updateById(ScheduleJob scheduleJob);

	int batchUpdate(List<ScheduleJob> scheduleJobList);

	int deleteById(String id);

	int deleteByIds(List<String> idList);

    ScheduleJob getById(String id);

	List<ScheduleJob> getByIds(List<String> idList);

	List<ScheduleJob> queryListByMap(Map<String, Object> queryMap);

	ScheduleJob getByName(Map<String, Object> queryMap);
}