package com.tansun.tandata.mapper;

import com.tansun.tandata.entity.Approval;
import com.tansun.tandata.entity.ApprovalSon;
import com.tansun.tandata.vo.req.ApprovalPageVo;
import com.tansun.tjdp.orm.persistence.BaseDao;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ApprovalDao extends BaseDao<Approval> {
    int insert(Approval approval);
    Approval selectByPrimaryKey(String id);
    int updateByPrimaryKeySelective(Approval approval);
    int deleteByPrimaryKey(String id);
    List<Approval> getApproval(ApprovalPageVo approvalPageVo);
    int selectContBysalesName(String sales_name);
    int deleteApprovalSonBysalesName(String sales_name);
    int insertApprovalSonBath(List<ApprovalSon> list);
    int selectSonContByApproval(String approval);
    int selectRoleCont(@Param("userCode") String userCode, @Param("roleCode")String roleCode);
    int deleteUserRole(@Param("userCode") String userCode, @Param("roleCode")String roleCode);

}
