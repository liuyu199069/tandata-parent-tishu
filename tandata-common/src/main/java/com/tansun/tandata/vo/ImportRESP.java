package com.tansun.tandata.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 导入返回参数
 * @author linlaichun
 * @date 2020-05-21 14:12:15
 * @Version v1.0
 */
@Data
@ApiModel(value="导入返回参数")
public class ImportRESP implements Serializable {
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "读取数据总数")
	private Integer total;

	@ApiModelProperty(value = "成功数据总数")
	private Integer insetNum;

	@ApiModelProperty(value = "失败数据总数")
	private Integer errorNum;

	@ApiModelProperty(value = "失败数据")
	private List<ImportErrorInfoRESP> errorData;



}