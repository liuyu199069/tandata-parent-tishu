package com.tansun.tandata.mapper;

import java.util.List;
import java.util.Map;

import com.tansun.tandata.entity.LogInfo;
import com.tansun.tandata.vo.req.LogInfoSeachVo;


public interface LogInfoDao {
	/**
	 * 查询日志信息
	 */
	public List<LogInfo> selectLogInfoByKey(LogInfoSeachVo ogInfoSeachVo);
	/**
	 * 插入日志信息
	 */
	public void insertLogInfo(LogInfo logInfo);

	/**
	 * 根据id查询
	 */
	public String selectLogInfoRaisnumById(String id);
}
