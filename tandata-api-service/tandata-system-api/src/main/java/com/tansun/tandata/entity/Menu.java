package com.tansun.tandata.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;


import com.tansun.tjdp.orm.entity.BaseEntity;
import lombok.Data;

/**
 * @Description 菜单实体类
 * @CreateDate 20200426
 * @author shenxiaodong
 * @Version v1.0
 *
 */

@Entity
@Data
@Table(name="sys_menu")
public class Menu extends BaseEntity<Menu, String> implements Serializable{
	private static final long serialVersionUID = 1188583090334390577L;

	/**
	 * 菜单ID
	 */
	@Id
	@Column(name = "id")
	private String id;

	/**
	 * 上级菜单ID
	 */
	@Column(name = "prnt_id")
	private String prntId;

	/**
	 * 菜单中文名称
	 */
	@Column(name = "fn_chn_nm")
	private String fnChnNm;

	/**
	 * 菜单英文名称
	 */
	@Column(name = "fn_eng_nm")
	private String fnEngNm;

	/**
	 * 菜单访问路径
	 */
	@Column(name = "rsrc_no")
	private String rsrcNo;

	/**
	 * 菜单功能分类
	 */
	@Column(name = "fn_ctgr_cd")
	private String fnCtgrCd;

	/**
	 * 排序字段
	 */
	@Column(name = "sort")
	private Integer sort;

	/**
	 * 创建人
	 */
	@Column(name = "creater")
	private String creater;

	/**
	 * 创建时间
	 */
	@Column(name = "create_time")
	private Date createTime;

	/**
	 * 修改人
	 */
	@Column(name = "updater")
	private String updater;

	/**
	 * 修改时间
	 */
	@Column(name = "update_time")
	private Date updateTime;

	/**
	 * 是否删除
	 */
	@Column(name = "is_delete")
	private Integer isDelete;

	/**
	 * 乐观锁
	 */
	@Column(name = "update_count")
	private Integer updateCount;

	
	
	
}
