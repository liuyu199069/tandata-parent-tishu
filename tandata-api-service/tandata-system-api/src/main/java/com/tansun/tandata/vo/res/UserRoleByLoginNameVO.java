package com.tansun.tandata.vo.res;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;


/**
 * @Description 用户角色基本信息实体类
 * @CreateDate 20200426
 * @author shenxiaodong
 * @Version v1.0
 *
 */

@ApiModel(value = "查找用户信息带角色编码", description = "查找用户信息带角色编码")
@Data
public class UserRoleByLoginNameVO implements Serializable {

    private static final long serialVersionUID = -4248484822880965620L;

    @ApiModelProperty(value="用户信息对象")
    private UserInfoVO userInfo;

    @ApiModelProperty(value="角色编码集合")
    private String role = "";
}
