package com.tansun.tandata.vo.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
/**
 * @Description 审批人分页查询对象
 * @CreateDate 20200426
 * @author shenxiaodong
 * @Version v1.0
 *
 */

@ApiModel(value="传入ApprovalPageVo对象",description="分页查询审批人列表")
@Data
public class ApprovalPageVo implements Serializable {
    @ApiModelProperty(value = "业务人员域账号", required = false)
    private String sales_name;
    @ApiModelProperty(value = "业务人员域账号姓名", required = false)
    private String sales_nick_name;
    @ApiModelProperty(value = "审批人姓名", required = false)
    private String approval_nick_name;
    @ApiModelProperty(
            value = "当前页",
            example = "1"
    )
    private int page;
    @ApiModelProperty(
            value = "显示数量",
            example = "30"
    )
    private int limit;


}
