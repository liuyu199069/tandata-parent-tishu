package com.tansun.tandata.vo.req.bi;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel(
        value = "bi表授权",
        description = "bi建表集合对象"
)
@Data
public class CarrierTableUserVo {
    @ApiModelProperty(
            value = "表名称",
            example = "liuydev"
    )
    private String tableName;
    @ApiModelProperty(
            value = "用户账号",
            example = "111"
    )
    private String userName;
}
