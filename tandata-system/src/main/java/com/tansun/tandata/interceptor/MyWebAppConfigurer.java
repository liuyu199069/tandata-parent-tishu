package com.tansun.tandata.interceptor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * @Description 拦截器的注册
 * @CreateDate 20200425
 * @author yangliu
 * @Version v1.0
 */

@Configuration
public class MyWebAppConfigurer extends WebMvcConfigurerAdapter {
	
    @Autowired
    private AuthorizationInterceptor authorizationInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // addPathPatterns 用于添加拦截规则, 这里假设拦截 /url 后面的全部链接
        // excludePathPatterns 用户排除拦截
       // registry.addInterceptor(SpringMVCInterceptor.loadInterceptor(ipStr)).addPathPatterns("/**").excludePathPatterns("/common/**","/layout/**","/login/**","/js/**","/css/**");
    	InterceptorRegistration addInterceptor = registry.addInterceptor(authorizationInterceptor);
    	addInterceptor.addPathPatterns("/**");
    	addInterceptor.excludePathPatterns("/swagger-ui.html");
//    	addInterceptor.excludePathPatterns("/editorService/findDepartmentList");
//    	addInterceptor.excludePathPatterns("/editorService/findRoleList");
//    	addInterceptor.excludePathPatterns("/editorService/findUsersList");
//    	addInterceptor.excludePathPatterns("/sysInfo/user/login");
//    	addInterceptor.excludePathPatterns("/sysInfo/login/sendMsg");
//    	addInterceptor.excludePathPatterns("/sysInfo/login/getMobileMsgList");
    	
    	super.addInterceptors(registry);
    }
}

