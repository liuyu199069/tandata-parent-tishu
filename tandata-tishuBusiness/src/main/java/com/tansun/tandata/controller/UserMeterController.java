package com.tansun.tandata.controller;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.tansun.tandata.common.TandataServerEnum;
import com.tansun.tandata.entity.LogInfoErro;
import com.tansun.tandata.entity.MeterFileAuth;
import com.tansun.tandata.entity.UserMeter;
import com.tansun.tandata.feign.SysFeignService;
import com.tansun.tandata.service.UserMeterService;
import com.tansun.tandata.utils.Constants;
import com.tansun.tandata.utils.JsonUtil;
import com.tansun.tandata.utils.ParentController;
import com.tansun.tandata.utils.ResponseBase;
import com.tansun.tandata.vo.req.MeterFileAuthPageVO;
import com.tansun.tandata.vo.req.UserMeterPageVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author liuyu
 * @Description 用户仪表板控制器
 * @CreateDate 20200426
 * @Version v1.0
 */
@Api(value = "提数管理接口")
@RestController
@RequestMapping(value = "/tishu/UserMeter")
public class UserMeterController extends ParentController {

    @Autowired
    private UserMeterService userMeterService;
    @Autowired
    private SysFeignService sysFeignService;

    @ApiOperation(value = "查询用户仪表板列表", notes = "查询用户仪表板列表")
    @RequestMapping(value = "/getUserMeterList", method = RequestMethod.POST)
    public ResponseBase getUserMeterList(@Valid @RequestBody UserMeterPageVO userMeterPageVO) {
        Map<String, Object> data = new HashMap<>();
        try {
            Page<Object> pageInfo = PageHelper.startPage(userMeterPageVO.getPage(), userMeterPageVO.getLimit(), true);
            List<UserMeter> raiseNumList = userMeterService.selectUserMeter(userMeterPageVO);
            data.put("total", pageInfo.getTotal());
            data.put("data", raiseNumList);
        } catch (Exception e) {
            e.printStackTrace();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            e.printStackTrace(new PrintStream(baos));
            //保存错误日志
            LogInfoErro log = new LogInfoErro();
            log.setServerId(TandataServerEnum.Five.getServerId());//tandata-tishuussiness
            log.setUrl(log.getServerId()+"/tishu/UserMeter/getUserMeterList");
            log.setParamStr(JsonUtil.toJsonObjectString(userMeterPageVO));
            log.setReqType(1);//post
            log.setDesribe(baos.toString());
            sysFeignService.insertLogInfoErro(log);
            return setResult(Constants.HTTP_RES_CODE_500, localeMessage.getMessage("sys.common.error"), null); //系统错误
        }
        return setResultSuccess(data);
    }


    @ApiOperation(value = "拉取BI系统仪表板列表", notes = "拉取BI系统仪表板列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userName", value = "账号名", required = true, dataType = "String"),
    })
    @RequestMapping(value = "pullUserMeter", method = RequestMethod.GET)
    public ResponseBase pullUserMeter(String userName, HttpServletRequest request) {
        try {
            userMeterService.pullUserMeterList(userName);
        } catch (RuntimeException e) {
            e.printStackTrace();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            e.printStackTrace(new PrintStream(baos));
            //保存错误日志
            LogInfoErro log = new LogInfoErro();
            log.setServerId(TandataServerEnum.Five.getServerId());//tandata-tishuussiness
            log.setUrl(log.getServerId()+"/tishu/UserMeter/getUserMeterList");
            log.setParamStr("userName:"+userName);
            log.setReqType(0);//GET
            log.setDesribe(baos.toString());
            sysFeignService.insertLogInfoErro(log);
            return setResult(Constants.HTTP_RES_CODE_500, localeMessage.getMessage("sys.common.error"), null); //系统错误
        }
        return setResultSuccess();
    }

    @ApiOperation(value = "查询仪表板授权列表", notes = "查询仪表板授权列表")
    @RequestMapping(value = "/getMeterFileAuthList", method = RequestMethod.POST)
    public ResponseBase getMeterFileAuthList(@Valid @RequestBody MeterFileAuthPageVO meterFileAuthPageVO) {
        Map<String, Object> data = new HashMap<>();
        try {
            Page<Object> pageInfo = PageHelper.startPage(meterFileAuthPageVO.getPage(), meterFileAuthPageVO.getLimit(), true);
            List<MeterFileAuth> meterFileAuthList = userMeterService.selectMeterFileAuthList(meterFileAuthPageVO);
            data.put("total", pageInfo.getTotal());
            data.put("data", meterFileAuthList);
        } catch (Exception e) {
            e.printStackTrace();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            e.printStackTrace(new PrintStream(baos));
            //保存错误日志
            LogInfoErro log = new LogInfoErro();
            log.setServerId(TandataServerEnum.Five.getServerId());//tandata-tishuussiness
            log.setUrl(log.getServerId()+"/tishu/UserMeter/getMeterFileAuthList");
            log.setParamStr(JsonUtil.toJsonObjectString(meterFileAuthPageVO));
            log.setReqType(1);//POST
            log.setDesribe(baos.toString());
            sysFeignService.insertLogInfoErro(log);
            return setResult(Constants.HTTP_RES_CODE_500, localeMessage.getMessage("sys.common.error"), null); //系统错误
        }
        return setResultSuccess(data);
    }


    @ApiOperation(value = "用户仪表板处理完成", notes = "用户仪表板处理完成")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userMeterId", value = "用户仪表板列表id", required = true, dataType = "String"),
    })
    @RequestMapping(value = "handleDynamic", method = RequestMethod.GET)
    public ResponseBase handleDynamic(String userMeterId, String userCode,HttpServletRequest request) {
        try {
            userMeterService.handleDynamic(userMeterId);
        } catch (RuntimeException e) {
            e.printStackTrace();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            e.printStackTrace(new PrintStream(baos));
            //保存错误日志
            LogInfoErro log = new LogInfoErro();
            log.setServerId(TandataServerEnum.Five.getServerId());//tandata-tishuussiness
            log.setUrl(log.getServerId()+"/tishu/UserMeter/handleDynamic");
            log.setParamStr("userMeterId:"+userMeterId+"  userCode:"+userCode);
            log.setReqType(0);//GET
            log.setDesribe(baos.toString());
            sysFeignService.insertLogInfoErro(log);
            return setResult(Constants.HTTP_RES_CODE_500, localeMessage.getMessage("sys.common.error"), null); //系统错误
        }
        return setResultSuccess();
    }


    @ApiOperation(value = "授权仪表板文件下载", notes = "授权仪表板文件下载")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "meterFileId", value = "仪表板授权表ID", required = true, dataType = "String"),
    })
    @RequestMapping(value = "meterFileAuthoraze", method = RequestMethod.GET)
    public ResponseBase meterFileAuthoraze(String meterFileId, HttpServletRequest request) {
        try {
            userMeterService.MeterFileAuthoraze(meterFileId);
        } catch (RuntimeException e) {
            e.printStackTrace();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            e.printStackTrace(new PrintStream(baos));
            //保存错误日志
            LogInfoErro log = new LogInfoErro();
            log.setServerId(TandataServerEnum.Five.getServerId());//tandata-tishuussiness
            log.setUrl(log.getServerId()+"/tishu/UserMeter/meterFileAuthoraze");
            log.setParamStr("meterFileId:"+meterFileId);
            log.setReqType(0);//GET
            log.setDesribe(baos.toString());
            sysFeignService.insertLogInfoErro(log);
            return setResult(Constants.HTTP_RES_CODE_500, localeMessage.getMessage("sys.common.error"), null); //系统错误
        }
        return setResultSuccess();
    }
}
