package com.tansun.tandata.vo.res;

import lombok.Data;

import java.io.Serializable;

@Data
public class RoleCodeApiVO implements Serializable {

    private static final long serialVersionUID = 2725220514319359693L;

    private String roleCode;
    private String rsrcNos;
}
