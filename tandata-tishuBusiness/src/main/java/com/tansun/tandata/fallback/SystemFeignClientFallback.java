package com.tansun.tandata.fallback;

import com.tansun.tandata.entity.LogInfoErro;
import com.tansun.tandata.feign.SysFeignService;
import com.tansun.tandata.utils.LocaleMessage;
import com.tansun.tandata.utils.ParentController;
import com.tansun.tandata.utils.ResponseBase;
import com.tansun.tandata.vo.LogInfoVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.Valid;

@Component
public class SystemFeignClientFallback extends ParentController implements SysFeignService {

	@Autowired
	private LocaleMessage localeMessage;
	@Override
	public ResponseBase insertLogInfo(@Valid LogInfoVO logInfoVO) {
		return setResultError(localeMessage.getMessage("sys.feign.result.error"));
	}

	@Override
	public ResponseBase insertLogInfoErro(@Valid LogInfoErro logInfoErro) {
		return setResultError(localeMessage.getMessage("sys.feign.result.error"));
	}
}
