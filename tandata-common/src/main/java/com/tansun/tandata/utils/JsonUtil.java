package com.tansun.tandata.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.springframework.stereotype.Repository;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.*;

/**
 * @description: json工具类
 * @author: wbh
 * @create: 2020-05-07
 * @Version v1.0
 **/
@Repository
public class JsonUtil {

	/**
	 * 集合转JSONArray
	 * @param col
	 * @param b
	 * @return
	 */
	public static JSONArray toJsonArray(Collection<?> col, boolean b){
		return JSONArray.parseArray(toJsonArrayString(col, b));
	}
	/**
	 * 集合转JSONArray
	 * @param col
	 * @return
	 */
	public static JSONArray toJsonArray(Collection<?> col){
		return JSONArray.parseArray(toJsonArrayString(col));
	}


	/**
	 * 集合转json字符串, 不包含父类属性
	 * @param col
	 * @return
	 */
	public static String toJsonArrayString(Collection<?> col){
		return toJsonArrayString(col, false);
	}

	/**
	 * 集合转json字符串, 不包含父类属性
	 * @param col
	 * @return
	 */
	public static String toJsonMapArrayString(List<Map<String, Object>> col){
		String toJsonString = "";
		toJsonString = toJsonArrayString(col, false);
		return toJsonString;
	}

	/**
	 * 集合转json字符串
	 * @param col
	 * @param b：为true包含父类属性，false不包含父类属性
	 * @return
	 */
	public static String toJsonArrayString(Collection<?> col, boolean b){
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		for(Object obj : col){
			list.add(toMap(obj, b));
		}
		return JSONArray.toJSONString(list, b);
	}

	/**
	 * 集合转json字符串
	 * @param col
	 * @param b：为true包含父类属性，false不包含父类属性
	 * @return
	 */
	public static String test(Collection<?> col, boolean b){
		List<Object> list = new ArrayList< Object>();
		for(Object obj : col){
			list.add(toMap(obj, b));
		}
		String jsonString = list.toString().replace("=",":");
		return jsonString;
	}

	/**
	 * obj转json对象
	 */
	public static JSONObject toJsonObject(Object obj){
		return JSONObject.parseObject(toJsonObjectString(obj));
	}

	/**
	 * obj转json对象，不包含父类属性
	 * @param obj
	 * @param b：为true包含父类属性，false不包含父类属性
	 * @return
	 */
	public static JSONObject toJsonObject(Object obj, boolean b){
		return JSONObject.parseObject(toJsonObjectString(obj, b));
	}

	/**
	 * obj转json字符串，不包含父类属性
	 */
	public static String toJsonObjectString(Object obj){
		return toJsonObjectString(obj, false);
	}

	/**
	 * obj转json字符串，不包含父类属性
	 * @param obj
	 * @param b：为true包含父类属性，false不包含父类属性
	 * @return
	 */
	public static String toJsonObjectString(Object obj, boolean b){
		Map<String, Object> map = toMap(obj, b);
		return JSONObject.toJSONString(map);
	}

	/**
	 * jsonArray字符串转json对象，不包含父类属性
	 * @param jsonText
	 * */
	public static JSONArray arrayToJsonArray(String jsonText){
		return JSON.parseArray(jsonText);
	}

	/**
	 * json字符串转json对象，不包含父类属性
	 * @param jsonText
	 * @return
	 */
	public static JSONObject stringToJson(String jsonText){
		return JSON.parseObject(jsonText);
	}

	private static Map<String, Object> toMap(Object obj, boolean b) {
		Class<?> clazz = obj.getClass();
		Field[] fields = clazz.getDeclaredFields();
		Map<String, Object> map = new HashMap<String, Object>();
		for(Field field : fields){
			if(!Modifier.isStatic(field.getModifiers())){
				try {
					Object value = loadGetMothed(field.getName(), clazz).invoke(obj);
					if(value == null){
						value = "";
					}
					map.put(field.getName(), value);
				} catch (Exception e) {
				}
			}
		}
		if(b){
			Class<?> superClass = clazz.getSuperclass();
			while(superClass != null){
				Field[] superFields = superClass.getDeclaredFields();
				for(Field field : superFields){
					if(!Modifier.isStatic(field.getModifiers())){
						try {
							Object value = loadGetMothed(field.getName(), clazz).invoke(obj);
							if(value == null){
								value = "";
							}
							map.put(field.getName(), value);
						} catch (Exception e) {
						}
					}
				}
				superClass = superClass.getSuperclass();
			}
		}
		return map;
	}

	public static Method loadGetMothed(String name, Class<?> clazz) throws Exception {
		StringBuffer buf = new StringBuffer();
		buf.append(name.substring(0, 1).toUpperCase())
				.append(name.substring(1));
		return clazz.getMethod("get" + buf.toString());
	}

	/**
	 * 根据传入的KEY值获取JSON中的数据并转化为String
	 * @param json
	 * @param data
	 * @return
	 */
	public static String getJsonData(JSONObject json, String data) {
		return json.get(data) == null ? null : String.valueOf(json.get(data));
	}

	/**
	 * json转List<Map<String,Object>>
	 * @param json
	 * @return
	 */
	public static List<Map<String,Object>> toListMap(String json){
		List<Object> list =JSON.parseArray(json);
		List<Map<String,Object>> listw=new ArrayList<Map<String, Object>>();
		for (Object object : list){
			Map<String,Object> ageMap =new HashMap<String,Object>();
			Map<String,Object> ret = (Map<String, Object>) object;//去除list里面的值转为map
			listw.add(ret);
		}
		return  listw;
	}

	/**
	 * json（param）转Map<String,String>
	 * @param paramJson
	 * @return
	 */
	public static Map<String,String> paramToMap(String paramJson,String key,String mapKey){
		Map<String,String> map = new HashMap<String,String>();
		if(!key.equals("")){
			JSONObject jsonObjectStr = JSONObject.parseObject(paramJson);
			JSONArray jsonarray = (JSONArray) jsonObjectStr.get(key);
			for (int i=0;i<jsonarray.size();i++){
				JSONObject jsons=jsonarray.getJSONObject(i);
				if(mapKey.equals("code")){
					map.put(jsons.get("code").toString(), jsons.get("value").toString());
				}
				if(mapKey.equals("name")){
					map.put(jsons.get("name").toString(), jsons.get("value").toString());
				}
			}
		}else{
			JSONArray jsonarray = JSONArray.parseArray(paramJson);
			for (int i=0;i<jsonarray.size();i++){
				JSONObject jsons=jsonarray.getJSONObject(i);
				if(mapKey.equals("code")){
					map.put(jsons.get("code").toString(), jsons.get("value").toString());
				}
				if(mapKey.equals("name")){
					map.put(jsons.get("name").toString(), jsons.get("value").toString());
				}
			}
		}
		return map;
	}
	public static Map<String,String> paramToMap(String paramJson){
		JSONObject jsonObjectStr = JSONObject.parseObject(paramJson);
		JSONArray jsonarray = (JSONArray) jsonObjectStr.get("properties");
		Map<String,String> map = new HashMap<String,String>();
		for (int i=0;i<jsonarray.size();i++){
			JSONObject jsons=jsonarray.getJSONObject(i);
			map.put(jsons.get("code").toString(), jsons.get("value").toString());
		}
		return map;
	}
	/**
	 * json string 转换为 map 对象
	 * @param json
	 * @return
	 */
	public static Map<Object, Object> jsonToMap(String json) {
		Map map = JSON.parseObject(json);
		return map;
	}

	/**json string 转换为 对象
	 * @param json
	 * @param type
	 * @return
	 */
	public  static <T>  T jsonToBean(String json, Class<T> type) throws Exception{
		Map map = JSON.parseObject(json);
		return MapUtils.transMap(type,map);
	}
}
