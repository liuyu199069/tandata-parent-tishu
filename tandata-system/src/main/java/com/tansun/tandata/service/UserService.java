package com.tansun.tandata.service;

import java.util.List;

import com.tansun.tandata.vo.LdapUser;
import com.tansun.tandata.vo.req.UserChangePwdVO;
import com.tansun.tandata.vo.req.UserVO;
import com.tansun.tandata.vo.res.UserInfoVO;
import com.tansun.tandata.vo.res.UserRoleByLoginNameVO;
import com.tansun.tandata.vo.res.UserRoleVO;

public interface UserService {

    /**
     * 模糊查询用户信息
     */
    List<UserRoleVO> getUserByKey(String keywords, int status);

    /**
     * 查询所有用户信息
     */
    List<UserRoleVO> selectAllUser();

    /**
     * 添加用户信息
     */
    void saveUser(UserVO user);

    /**
     * 修改用户信息
     */
    void updateUser(UserVO user);

    /**
     * 删除用户信息
     */
    void deleteUser(String userCode);

    /**
     * 根据用户编号修改用户类型
     */
    void updateUserType(String userCode, String type);

    /**
     * 根据用户登录名称查询用户基本信息
     */
    UserRoleByLoginNameVO selectUserAndRoleInfoByLoginName(String loginName);

    /**
     * 用户登录以后返回用户基本信息
     */
    UserInfoVO getUserInfoAfterLogin(String loginName);

    /**
     * 更新用户密码
     */
    void updateUserPwd(UserChangePwdVO userChangePwdVO);

    /**
     * 验证名字和电话是否存在
     */
    int getUserByLoginNameOrPhone(String loginName, String userPhone);

    /**
     * 根据用户id查询用户信息
     */
    UserRoleVO selectUserById(String userCode);

    /**
     * 验证名字是否存在
     */
    int getUserByName(String userName);

    /**
     * 修改用户对应的机构
     */
    void updateOrgCodeByUser(String userCode, String orgCode);

    /**
     * 获取用户通过组织编码
     */
    int getUserByOrgCode(String orgCode);

    /**
     * 添加或修改ldap域账号到提数系统
     */
    int ldapUserToSysUser(LdapUser ldapUser);

    /**
     * 根据登录名锁定系统中的域账号
     */
    int lockLdapUserByLoginName(String loginName);


}
