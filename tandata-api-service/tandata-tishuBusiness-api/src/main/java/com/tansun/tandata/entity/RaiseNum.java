package com.tansun.tandata.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.tansun.tjdp.orm.entity.BaseEntity;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(
        name = "BUS_RAISENUM"
)
public class RaiseNum extends BaseEntity<RaiseNum, String> implements Serializable {
    private static final long serialVersionUID = -2701052307289939921L;

    /**
     * 提数ID
     */
    @Id
    @Column(name = "ID")
    private String id;
    /**
     * 提数序号
     */
    @Column(name = "RAISE_NO")
    private String raiseNo;
    /**
     * 标题
     */
    @Column(name = "TITLE")
    private String title;
    /**
     * 发起人用户编码
     */
    @Column(name = "RAISE_USERCODE")
    private String raiseUsercode;
    /**
     * 状态
     */
    @Column(name = "STATE")
    private Integer state;
    /**
     * 提数开始时间
     */
    @Column(name = "BEGIN_TIME")
    @JsonFormat(locale = "ZH",timezone = "GMT+8")
    private Date beginTime;
    /**
     * 提数结束时间
     */
    @Column(name = "END_TIME")
    @JsonFormat(locale = "ZH",timezone = "GMT+8")
    private Date endTime;
    /**
     * 接收人姓名
     */
    @Column(name = "RECIIVE_USERNAME")
    private String reciiveUsername;
    /**
     * SQL文
     */
    @Column(name = "SQL_TEXT")
    private String sqlText;
    /**
     * 结果表表名
     */
    @Column(name = "TABLE_NAME")
    private String tableName;
    /**
     * 可查看结束日期
     */
    @Column(name = "SEE_END_TIME")
    @JsonFormat(locale = "ZH",timezone = "GMT+8")
    private Date seeEndTime;
    /**
     * 数据保存结束日期
     */
    @Column(name = "SAVE_END_TME")
    @JsonFormat(locale = "ZH",timezone = "GMT+8")
    private Date saveEndTme;
    /**
     * 提数方式（0：立即提数；1：延时提数）
     */
    @Column(name = "RAISE_TYPE")
    private Integer raiseType;
    /**
     * 是否删除（0：否；1：是）
     */
    @Column(name = "ISDELE")
    private Integer isdele;
    /**
     * 可查看天数
     */
    @Column(name = "SEE_DATE")
    private Integer seeDate;
    /**
     * 数据保存天数
     */
    @Column(name = "SAVE_DATE")
    private Integer saveDate;
    /**
     * 提数序号
     */
    @Column(name = "CREATE_TIME")
    @JsonFormat(locale = "ZH",timezone = "GMT+8")
    private Date createTime;
    /**
     * 接收人域账号用户姓名字符串
     */
    @Column(name = "RECIIVE_STR")
    private String reciiveStr;

    public RaiseNum() {
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRaiseNo() {
        return this.raiseNo;
    }

    public void setRaiseNo(String raiseNo) {
        this.raiseNo = raiseNo;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getRaiseUsercode() {
        return this.raiseUsercode;
    }

    public void setRaiseUsercode(String raiseUsercode) {
        this.raiseUsercode = raiseUsercode;
    }

    public Integer getState() {
        return this.state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Date getBeginTime() {
        return this.beginTime;
    }

    public void setBeginTime(Date beginTime) {
        this.beginTime = beginTime;
    }

    public Date getEndTime() {
        return this.endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getReciiveUsername() {
        return this.reciiveUsername;
    }

    public void setReciiveUsername(String reciiveUsername) {
        this.reciiveUsername = reciiveUsername;
    }

    public String getSqlText() {
        return this.sqlText;
    }

    public void setSqlText(String sqlText) {
        this.sqlText = sqlText;
    }

    public String getTableName() {
        return this.tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public Date getSeeEndTime() {
        return this.seeEndTime;
    }

    public void setSeeEndTime(Date seeEndTime) {
        this.seeEndTime = seeEndTime;
    }

    public Date getSaveEndTme() {
        return this.saveEndTme;
    }

    public void setSaveEndTme(Date saveEndTme) {
        this.saveEndTme = saveEndTme;
    }

    public Integer getRaiseType() {
        return this.raiseType;
    }

    public void setRaiseType(Integer raiseType) {
        this.raiseType = raiseType;
    }

    public Integer getIsdele() {
        return this.isdele;
    }

    public void setIsdele(Integer isdele) {
        this.isdele = isdele;
    }

    public Integer getSeeDate() {
        return this.seeDate;
    }

    public void setSeeDate(Integer seeDate) {
        this.seeDate = seeDate;
    }

    public Integer getSaveDate() {
        return this.saveDate;
    }

    public void setSaveDate(Integer saveDate) {
        this.saveDate = saveDate;
    }

    public Date getCreateTime() {
        return this.createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getReciiveStr() {
        return this.reciiveStr;
    }

    public void setReciiveStr(String reciiveStr) {
        this.reciiveStr = reciiveStr;
    }
}
