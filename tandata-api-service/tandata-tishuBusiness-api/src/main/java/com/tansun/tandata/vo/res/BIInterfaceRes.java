package com.tansun.tandata.vo.res;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
@ApiModel(
        value = "调用BI系统通用返回Bean",
        description = "调用BI系统通用返回Bean"
)
@Data
public class BIInterfaceRes<T> implements Serializable {

    @ApiModelProperty("是否正常返回")
    private boolean success;
    @ApiModelProperty("正常返回状态码")
    private String code;
    @ApiModelProperty("返回消息")
    private String message;
    @ApiModelProperty("返回data")
    private T data;
    @ApiModelProperty("异常返回状态码")
    private String errorCode;
    public BIInterfaceRes(){}
}
