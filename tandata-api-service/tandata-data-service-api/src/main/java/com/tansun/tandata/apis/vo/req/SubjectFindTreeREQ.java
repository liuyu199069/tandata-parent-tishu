package com.tansun.tandata.apis.vo.req;

import com.tansun.tandata.utils.Constants;
import com.tansun.tandata.utils.StringUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @description: 获取树
 * @author: linLaiChun
 * @create: 2020-05-08
 * @Version v1.0
 **/

@Data
@ApiModel(value="获取树")
public class SubjectFindTreeREQ implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "父级ID")
    private String parentId;

    @ApiModelProperty(value = "名称")
    private String name;

    @ApiModelProperty(value = "类型（0服务、1主题）")
    private Integer type;

    @ApiModelProperty(value = "排序语句")
    private String orderStr;

    @ApiModelProperty(value = "排序字段")
    private String order;

    @ApiModelProperty(value = "排序类型(ASE:正序，DESC:倒序)")
    private String orderType;

    /**
     * 拼接数据库排序语句
     */
    public void makeOrderStr(){
        if(StringUtil.isEmpty(this.order)){
            return;
        }
        String orderStr = StringUtil.humpToLine(this.order);
        if(StringUtil.isEmpty(this.orderType)){
            orderStr += Constants.CHAR_SPACE + Constants.ORDER_ASC;
        }else {
            orderStr += Constants.CHAR_SPACE + this.orderType;
        }
        this.orderStr = orderStr;
    }

}
