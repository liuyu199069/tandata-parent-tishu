package com.tansun.tandata.vo.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.util.List;

/**
 * 主键集合
 * @author wzf
 * @date 2020-06-09 16:26:22
 * @Version v1.0
 */
@Data
@ApiModel(value="主键集合")
public class OrgInfoPkVO implements Serializable {
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "主键集",required = true)
	private List<String> orgCodeList;

}