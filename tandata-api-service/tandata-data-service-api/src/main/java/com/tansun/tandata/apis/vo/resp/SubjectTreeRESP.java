package com.tansun.tandata.apis.vo.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @description: 树
 * @author: linLaiChun
 * @create: 2020-05-08
 * @Version v1.0
 **/
@Data
@ApiModel(value="树")
public class SubjectTreeRESP implements Serializable {
    @ApiModelProperty(value = "ID")
    private String id;

    @ApiModelProperty(value = "父级ID")
    private String parentId;

    @ApiModelProperty(value = "名称")
    private String label;

    @ApiModelProperty(value = "类型（0服务、1主题）")
    private Integer type;

    @ApiModelProperty(value = "子集")
    private List<SubjectTreeRESP> children = new ArrayList<>();


    /**
     * 递归树
     * @param treeList
     * @return List<MdTreeVO>
     */
    public static List<SubjectTreeRESP> buildTree(List<SubjectTreeRESP> treeList, String parentCod) {
        List<SubjectTreeRESP> treeVoList = new ArrayList<>();
        List<SubjectTreeRESP> inner = new ArrayList<>();
        inner.addAll(treeList);
        for (SubjectTreeRESP treeVO : treeList) {
            if(treeVO.getParentId().equals(parentCod)) {
                inner.remove(treeVO);
                List<SubjectTreeRESP> childList = buildTree(inner, treeVO.getId());
                treeVO.setChildren(childList);
                treeVoList.add(treeVO);
            }
        }
        return treeVoList;
    }
}
