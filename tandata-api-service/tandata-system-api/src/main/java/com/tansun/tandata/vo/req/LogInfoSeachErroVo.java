package com.tansun.tandata.vo.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@ApiModel(
        value = "操作错误日志查询对象",
        description = "操作错误日志查询对象"
)
@Data
public class LogInfoSeachErroVo implements Serializable {
    @ApiModelProperty(
            value = "当前页",
            example = "1"
    )
    private int page;
    @ApiModelProperty(
            value = "显示数量",
            example = "30"
    )
    private int limit;
    @ApiModelProperty(
            value = "请求方式",
            example = "1"
    )
    private Integer reqType;
    @ApiModelProperty(
            value = "操作时间开始",
            example = "2020-09-30 09:08:05"
    )
    private String operationTime_beg;
    @ApiModelProperty(
            value = "操作时间结束",
            example = "2020-09-30 12:08:05"
    )
    private String operationTime_end;

}
