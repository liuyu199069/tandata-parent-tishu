package com.tansun.tandata.apis.mapper;

import com.tansun.tandata.apis.entity.ApisSubject;
import com.tansun.tjdp.orm.persistence.BaseDao;

import java.util.List;
import java.util.Map;

/**
 * 接口主题信息表Dao
 * @author LinLaiChun
 * @date 2020-06-10 14:36:59
 * @Version v1.0
 */
public interface ApisSubjectDao extends BaseDao<ApisSubject> {

	int insert(ApisSubject apisSubject);

	int batchInsert(List<ApisSubject> apisSubjectList);

	int updateById(ApisSubject apisSubject);

	int batchUpdateTraditional(List<ApisSubject> apisSubjectList);

	int batchUpdate(List<ApisSubject> apisSubjectList);

	int deleteById(String id);

	int deleteByIds(List<String> idList);

    ApisSubject getById(String id);

	List<ApisSubject> getByIds(List<String> idList);

	List<ApisSubject> queryListByMap(Map<String, Object> queryMap);
}