package com.tansun.tandata.utils;

import java.util.Date;
import java.util.Random;
import java.util.UUID;

public class CodeGenerator {
	
	private static long num=0; 
	public static String[] chars = new String[] { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n","o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "0", "1", "2", "3", "4", "5", "6", "7", "8","9", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T","U", "V", "W", "X", "Y", "Z" };
	
	public static String generateOrderShortUuid() {
		StringBuffer shortBuffer = new StringBuffer();
		String uuid = UUID.randomUUID().toString().replace("-", "");
		for (int i = 0; i < 7; i++) {
			String str = uuid.substring(i * 4, i * 4 + 4);
			int x = Integer.parseInt(str, 16);
			shortBuffer.append(chars[x % 0x3E]);
		}
		return DateUtils.formatDate("yyyyMMddHHmmss", new Date())+shortBuffer.toString();
	}
	
	public static String generateShortUuid(String title) {
		StringBuffer shortBuffer = new StringBuffer();
		String uuid = UUID.randomUUID().toString().replace("-", "");
		for (int i = 0; i < 8; i++) {
			String str = uuid.substring(i * 4, i * 4 + 4);
			int x = Integer.parseInt(str, 16);
			shortBuffer.append(chars[x % 0x3E]);
		}
		return title+DateUtils.formatDate("yyyyMMdd", new Date())+shortBuffer.toString();
	}
	
    /**
	   * 随机生成UUID
	   * @return
	   */
	  public static synchronized String getUUID(){
	    UUID uuid=UUID.randomUUID();
	    String str = uuid.toString(); 
	    String uuidStr=str.replace("-", "");
	    return uuidStr;
	  }
	  
	  /**
	   * 根据字符串生成固定UUID
	   * @param name
	   * @return
	   */
	  public static synchronized String getUUID(String name){
		UUID uuid=UUID.randomUUID();
	    String str = uuid.toString(); 
	    String uuidStr=str.replace("-", "");
	    return name+uuidStr;
	  }

	/**
	 * 根据类名生成固定UUID
	 * @param clazz
	 * @return
	 */
	public static synchronized String getUUID(Class clazz){
		String clazzName = clazz.getName();
		String modelName = clazzName.substring(clazzName.lastIndexOf(".")+1);
		UUID uuid=UUID.randomUUID();
		String str = uuid.toString();
		String uuidStr=str.replace("-", "");
		return modelName+uuidStr;
	}
	  
	  /**
	   * 根据日期生成长整型id
	   */
	  public static synchronized long getLongId(){
	    String date=DateUtils.formatDate("yyyyMMddHHmmssS", new Date());
	    System.out.println("原始id="+date);
	    if(num>=99) num=0l;
	    ++num;
	    if(num<10) {
	      date=date+00+num;
	    }else{
	      date+=num;
	    }
	    return Long.valueOf(date);
	  }
	  
	  /**
	   * 产生随机6位短信码
	   * @param 
	   */
	  public static String createVerifyCode(){
		  String verifyCode = String.valueOf(new Random().nextInt(899999) + 100000);//生成短信验证码
		  return verifyCode;
	  }
}
