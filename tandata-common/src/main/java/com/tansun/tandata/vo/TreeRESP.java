package com.tansun.tandata.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @description: 树
 * @author: linLaiChun
 * @create: 2020-05-08
 * @Version v1.0
 **/
@Data
@ApiModel(value="树")
public class TreeRESP implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "ID")
    private String id;

    @ApiModelProperty(value = "父级ID")
    private String parentId;

    @ApiModelProperty(value = "名称")
    private String label;

    @ApiModelProperty(value = "子集")
    private List<TreeRESP> children = new ArrayList<>();


    /**
     * 递归树
     * @param treeList
     * @return List<MdTreeVO>
     */
    public static List<TreeRESP> buildTree(List<TreeRESP> treeList, String parentCod) {
        List<TreeRESP> treeVoList = new ArrayList<>();
        List<TreeRESP> inner = new ArrayList<>();
        inner.addAll(treeList);
        for (TreeRESP treeVO : treeList) {
            if(treeVO.getParentId().equals(parentCod)) {
                inner.remove(treeVO);
                List<TreeRESP> childList = buildTree(inner, treeVO.getId());
                treeVO.setChildren(childList);
                treeVoList.add(treeVO);
            }
        }
        return treeVoList;
    }
}
