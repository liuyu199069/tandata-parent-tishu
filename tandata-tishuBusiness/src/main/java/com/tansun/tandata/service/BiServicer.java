package com.tansun.tandata.service;

import com.alibaba.fastjson.JSONObject;
import com.tansun.tandata.vo.req.bi.CarrierTableUserVo;
import com.tansun.tandata.vo.res.BIMeter;

import java.util.List;

public interface BiServicer {

    public JSONObject addBiTable(String tableName);

    public JSONObject carrierTableUser(List<CarrierTableUserVo> list);

    public List<BIMeter> getDashboardList(String userName);



}
