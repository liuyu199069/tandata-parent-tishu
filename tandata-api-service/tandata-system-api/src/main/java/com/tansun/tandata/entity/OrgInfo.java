package com.tansun.tandata.entity;

import java.io.Serializable;
import java.util.Date;
import com.tansun.tjdp.orm.entity.BaseEntity;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * @author wzf
 * @date 2020-06-09 16:26:22
 * @Version v1.0
 */
@Entity
@Data
@Table(name="org_info")
public class OrgInfo extends BaseEntity<OrgInfo, String> implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	 * 机构编码
	 */
	@Id
	@Column(name = "org_code")
	private String orgCode;

	/**
	 * 机构名称
	 */
	@Column(name = "org_name")
	private String orgName;

	/**
	 * 上一级机构节点
	 */
	@Column(name = "parent_code")
	private String parentCode;

	/**
	 * 机构类型
	 */
	@Column(name = "type")
	private Integer type;

	/**
	 * 创建者code
	 */
	@Column(name = "creater_id")
	private String createrId;

	/**
	 * 
	 */
	@Column(name = "creater_time")
	private Date createrTime;

	/**
	 * 修改者ID
	 */
	@Column(name = "update_id")
	private String updateId;

	/**
	 * 修改时间
	 */
	@Column(name = "update_time")
	private Date updateTime;

	/**
	 * 修改次数
	 */
	@Column(name = "update_count")
	private Integer updateCount;




}