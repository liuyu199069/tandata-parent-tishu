package com.tansun.tandata.mapper;

import java.util.List;


public interface SysRoleMapper {
	
	/**
	 * @Description 查询用户根据用户名
	 * @param name
	 * @return
	 */
	public List<Integer> selectSysRoleByUserId(int userId);
}
