package com.tansun.tandata.apis.entity;

import com.tansun.tjdp.orm.entity.BaseEntity;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * 主题sql文本
 * @author LinLaiChun
 * @date 2020-06-10 15:08:46
 * @Version v1.0
 */
@Entity
@Data
@Table(name="t_ds_apis_subject_sql")
public class ApisSubjectSql extends BaseEntity<ApisSubjectSql, String> implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	 * ID
	 */
	@Id
	@Column(name = "id")
	private String id;

	/**
	 * 所属主题（主题表）
	 */
	@Column(name = "subject_id")
	private String subjectId;

	/**
	 * sql文本
	 */
	@Column(name = "sql_txt")
	private String sqlTxt;




}