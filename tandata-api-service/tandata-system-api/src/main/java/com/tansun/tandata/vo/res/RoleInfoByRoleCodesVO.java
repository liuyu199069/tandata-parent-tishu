package com.tansun.tandata.vo.res;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@ApiModel(value = "角色编号和Api路径集合", description = "角色编号和Api路径集合")
@Data
public class RoleInfoByRoleCodesVO implements Serializable {

    private static final long serialVersionUID = 1659680269393267474L;

    @ApiModelProperty(value="角色菜单")
    private String roleCode;

    @ApiModelProperty(value="Api路径集合")
    private List<String> apis = new ArrayList<>();
}
