package com.tansun.tandata.vo.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.util.Date;

/**
 * 分页查询服务配置表
 * @author LinLaiChun
 * @date 2020-06-09 15:55:26
 * @Version v1.0
 */
@Data
@ApiModel(value="分页查询服务配置表")
public class QuartzServerPageREQ implements Serializable {
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "主键", required = false)
	private String id;

	@ApiModelProperty(value = "服务名", required = false)
	private String serviceName;

	@ApiModelProperty(value = "服务链接", required = false)
	private String serviceUrl;

	@ApiModelProperty(value = "排序语句",required = false)
	private String orderBy;

	@ApiModelProperty(value = "当前页码",required = true)
	private Integer currentPage;

	@ApiModelProperty(value = "单页数据量",required = true)
	private Integer pageSize;
}