package com.tansun.tandata.utils;

import java.math.BigDecimal;

public class MathUtil {
	private static final Double MONEY_RANGE = 0.01;

	/**
	 * 比较2个金额是否相等
	 * 
	 * @param d1
	 * @param d2
	 * @return
	 */
	public static Boolean equals(BigDecimal d1, BigDecimal d2) {
		Double result = Math.abs(Double.valueOf(String.valueOf(d1)) - Double.valueOf(String.valueOf(d2)));
		if (result < MONEY_RANGE) {
			return true;
		} else {
			return false;
		}
	}
}
