package com.tansun.tandata.bigdata.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @author Tansun
 * @date 2020/4/18 15:50
 */
@Data
public class FlinkSqlTableParam implements Serializable {

    private static final long serialVersionUID = 3235807763439451231L;

    private String tempTableName;
    private String schema;
    private String connectParam;

}
