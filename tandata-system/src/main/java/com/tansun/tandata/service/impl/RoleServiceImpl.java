package com.tansun.tandata.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tansun.tandata.entity.Role;
import com.tansun.tandata.entity.RoleMenu;
import com.tansun.tandata.mapper.RoleDao;
import com.tansun.tandata.service.RoleService;
import com.tansun.tandata.utils.CodeGenerator;
import com.tansun.tandata.utils.Constants;
import com.tansun.tandata.utils.ParentServiceImp;
import com.tansun.tandata.vo.req.RoleVO;
import com.tansun.tandata.vo.res.RoleMenuVO;

/**
 * @Description 角色信息业务类
 * @CreateDate 20200428
 * @author shenxiaodong
 * @Version v1.0
 *
 */
@Service("roleServiceImpl")
public class RoleServiceImpl extends ParentServiceImp<RoleDao, Role> implements RoleService {

	@Autowired
	RoleDao roleDao;
	/**
	 * 查询所有角色信息
	 * @return List<Role>
	 */
	public List<RoleMenuVO> selectAllRole(String roleName){
		return roleDao.selectAllRole(roleName);
	}

	/**
	 * 根据角色编号删除角色信息
	 */
	public void deleteRoleById(String roleCode) {
		roleDao.deleteRoleById(roleCode);
		roleDao.deleteUserRoleById(roleCode);
		roleDao.deleteRoleMenuById(roleCode);
	}

	/**
	 * 根据角色编号删除用户角色信息
	 */
	public void deleteUserRoleById(String roleCode) {
		roleDao.deleteUserRoleById(roleCode);
	}
	
	
	/**
	 * 根据角色编号删除角色菜单信息
	 */
	public void deleteRoleMenuById(String roleCode) {
		roleDao.deleteRoleMenuById(roleCode);
	}

	/**
	 * 添加角色信息
	 *
	 */
	public void saveRole(RoleVO roleVO) {
		//角色属性赋值
		Role role = new Role();
		BeanUtils.copyProperties(roleVO, role);
		role.setRoleCode(CodeGenerator.getUUID(Role.class));
		role.setCreater(getCurrUserId());
		role.setCreateTime((new Date()));
		role.setIsDelete(Constants.DELETE_FLAG_RESERVED);
		role.setUpdateCount(Constants.USER_UPDATE_COUNT_INIT);
		//角色信息保存
		roleDao.saveRole(role);
		if(roleVO.getMenuIds() != null && roleVO.getMenuIds().size() > 0) {
			RoleMenu rm = new RoleMenu();
			rm.setRoleCode(role.getRoleCode());
			rm.setMenuIds(roleVO.getMenuIds());
			roleDao.saveRoleMenu(rm);
		}
	}
	
	/**
	 * 添加角色菜单信息
	 *
	 */
	public void saveRoleMenu(RoleMenu rm) {
		roleDao.saveRoleMenu(rm);
	}
	
	
	/**
	 * 根据角色名查询角色
	 */
	public int getRoleByName(String roleName) {
		
		return roleDao.getRoleByName(roleName);
	}
	
	/**
	 * 修改角色信息
	 */
	public void updateRole(RoleVO roleVO) {

		//角色属性赋值
		Role role = new Role();
		BeanUtils.copyProperties(roleVO, role);
		role.setUpdater(getCurrUserId());
		role.setUpdateTime(new Date());
		//角色信息修改
		roleDao.updateRole(role);
		roleDao.deleteRoleMenuById(role.getRoleCode());
		if(roleVO.getMenuIds() != null && roleVO.getMenuIds().size() > 0) {
			RoleMenu rm = new RoleMenu();
			rm.setRoleCode(role.getRoleCode());
			rm.setMenuIds(roleVO.getMenuIds());
			roleDao.saveRoleMenu(rm);
		}
	}

	/**
	 * 根据角色编号获取角色信息
	 */
	public RoleMenuVO getRoleById(String roleCode) {
		return roleDao.getRoleById(roleCode);
	}

	/**
	 * 根据用户编号获取角色信息
	 */
	public List<RoleMenuVO> selectRolesByUserCode(String userCode) {
		return roleDao.selectRolesByUserCode(userCode);
	}
}
