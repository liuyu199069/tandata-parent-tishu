package com.tansun.tandata.apis.vo.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.io.Serializable;

/**
 * 分页查询接口主题信息
 * @author LinLaiChun
 * @date 2020-06-10 15:08:46
 * @Version v1.0
 */
@Data
@ApiModel(value="分页查询接口主题信息")
public class ApisSubjectPageREQ implements Serializable {
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "主题名称", required = false)
	private String name;

	@ApiModelProperty(value = "父主题ID", required = false)
	private String parentId;

	@ApiModelProperty(value = "类型（0服务、1主题）", required = false)
	private Integer type;

	@ApiModelProperty(value = "排序语句",required = false)
	private String orderBy;

	@ApiModelProperty(value = "当前页码",required = true)
	@Positive
	@NotNull
	private Integer currentPage;

	@ApiModelProperty(value = "单页数据量",required = true)
	@Positive
	@Max(value = 100)
	@NotNull
	private Integer pageSize;
}