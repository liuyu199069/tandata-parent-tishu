package com.tansun.tandata.service;

import com.tansun.tandata.entity.RaiseNum;
import com.tansun.tandata.vo.LdapUser;
import com.tansun.tandata.vo.req.RaiseNumPageVO;
import com.tansun.tandata.vo.req.RaiseNumVO;

import java.util.List;

public  interface RaisNumService
{
    public Integer addRaisNum(RaiseNumVO paramRaiseNumVO);
    public Integer upDateRaisNum(RaiseNumVO paramRaiseNumVO);
    public List<RaiseNum> getRaiseNumList(RaiseNumPageVO raiseNumPageVO);
    public Integer deleteRaisNum(String id);
    public Integer deleteRaisNum_logic(String id);
    public List<RaiseNum> getReciivetBylimit();
    public Integer execRaisShell(RaiseNum obj,String shellPath);
    public Integer execStopRaisShell(String id);
    public Integer execRaisShellBack(String state,String raisNumId,String logId);
    public List<LdapUser> findLdapUserList(String name);
    public void execRaisNumShell();
    public void execCleanTableShell();
    public Integer updateBeginTime(String id);


}