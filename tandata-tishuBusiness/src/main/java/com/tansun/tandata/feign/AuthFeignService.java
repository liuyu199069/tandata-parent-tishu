package com.tansun.tandata.feign;

import com.tansun.tandata.feign.config.FeignLogConfig;
import com.tansun.tandata.vo.LdapUser;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import java.util.List;


@FeignClient(value = "auth-server", configuration = FeignLogConfig.class)
public interface AuthFeignService {
	@RequestMapping(value = "/ldap/seachLdapUserByKeywords",method=RequestMethod.POST)
	List<LdapUser> seachLdapUserByKeywords(@Valid @RequestBody LdapUser ldapUser);
}
