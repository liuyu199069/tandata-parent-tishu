package com.tansun.tandata.apis.entity;

import java.io.Serializable;
import java.util.Date;
import com.tansun.tjdp.orm.entity.BaseEntity;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 接口参数信息配置
 * @author LinLaiChun
 * @date 2020-06-10 15:08:46
 * @Version v1.0
 */
@Entity
@Data
@Table(name="t_ds_apis_param")
public class ApisParam extends BaseEntity<ApisParam, String> implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	 * ID
	 */
	@Id
	@Column(name = "id")
	private String id;

	/**
	 * 类型（请求参数或返回参数）
	 */
	@Column(name = "type")
	private Integer type;

	/**
	 * 所属接口（接口表）
	 */
	@Column(name = "interface_id")
	private String interfaceId;

	/**
	 * 参数名称
	 */
	@Column(name = "param_name")
	private String paramName;

	/**
	 * 参数类型
	 */
	@Column(name = "param_type")
	private String paramType;

	/**
	 * 参数示例值
	 */
	@Column(name = "param_sample_value")
	private String paramSampleValue;

	/**
	 * 参数默认值
	 */
	@Column(name = "param_default_value")
	private String paramDefaultValue;

	/**
	 * 参数测试值
	 */
	@Column(name = "param_test_value")
	private String paramTestValue;

	/**
	 * 参数描述
	 */
	@Column(name = "remark")
	private String remark;

	/**
	 * 排序字段
	 */
	@Column(name = "sort")
	private Integer sort;

	/**
	 * 创建者ID
	 */
	@Column(name = "creator_id")
	private String creatorId;

	/**
	 * 创建时间
	 */
	@Column(name = "create_time")
	private Date createTime;

	/**
	 * 修改者ID
	 */
	@Column(name = "modifier_id")
	private String modifierId;

	/**
	 * 修改时间
	 */
	@Column(name = "modify_time")
	private Date modifyTime;




}