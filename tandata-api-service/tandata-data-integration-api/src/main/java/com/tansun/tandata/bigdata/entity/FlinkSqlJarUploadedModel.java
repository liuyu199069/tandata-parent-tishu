package com.tansun.tandata.bigdata.entity;


import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class FlinkSqlJarUploadedModel implements Serializable {

    private static final long serialVersionUID = 5733050930425432318L;

    private Long id;
    private String jarId;
    private String jarName;
    private String uploadedPerson;
    private Date uploadedTime;
    private String deletePerson;
    private Date deleteTime;
    private Integer flag;

}
