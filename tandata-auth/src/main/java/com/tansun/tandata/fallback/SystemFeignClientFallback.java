package com.tansun.tandata.fallback;

import com.tansun.tandata.entity.LogInfoErro;
import com.tansun.tandata.vo.LdapUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.tansun.tandata.feign.SysFeignService;
import com.tansun.tandata.utils.LocaleMessage;
import com.tansun.tandata.utils.ParentController;
import com.tansun.tandata.utils.ResponseBase;

import javax.validation.Valid;

@Component
public class SystemFeignClientFallback extends ParentController implements SysFeignService {

	@Autowired
	private LocaleMessage localeMessage;

	@Override
	public ResponseBase getUserAndRoleInfoByLoginName(String loginName) {
		return setResultError(localeMessage.getMessage("sys.feign.result.error"));
	}

	@Override
	public ResponseBase getRoleAndApis() {
		return setResultError(localeMessage.getMessage("sys.feign.result.error"));
	}

	@Override
	public ResponseBase getChildOrgByOrgCode(String orgCode) {
		return setResultError(localeMessage.getMessage("sys.feign.result.error"));
	}

	@Override
	public ResponseBase ldapUserToSysUser(LdapUser ldapUser) {
		return setResultError(localeMessage.getMessage("sys.feign.result.error"));
	}

	@Override
	public ResponseBase lockLdapUserByLoginName(String loginName) {
		return setResultError(localeMessage.getMessage("sys.feign.result.error"));
	}

	@Override
	public ResponseBase insertLogInfoErro(@Valid LogInfoErro logInfoErro) {
		return setResultError(localeMessage.getMessage("sys.feign.result.error"));
	}

	@Override
	public ResponseBase authUserApprovalRole(String loginName) {
		return setResultError(localeMessage.getMessage("sys.feign.result.error"));
	}
}
