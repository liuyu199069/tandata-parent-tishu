package com.tansun.tandata.vo.req;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Length;

import com.tansun.tandata.entity.Menu;
import com.tansun.tjdp.orm.entity.BaseEntity;

import lombok.Data;

/**
 * @Description 角色实体类
 * @CreateDate 20200426
 * @author shenxiaodong
 * @Version v1.0
 *
 */

@ApiModel(value="传入Role对象",description="角色对象")
@Data
public class RoleVO implements Serializable{

	private static final long serialVersionUID = -8034772436466915846L;

	@ApiModelProperty(value="角色编码")
	private String roleCode;

	@Size(max=100,message="{sys.CompanyVO.companySocialCode.lengthError}")
	@NotNull(message="{sys.CompanyVO.companySocialCode.notNullError}")
	@ApiModelProperty(value="角色名称")
	private String roleName;

	@ApiModelProperty(value="角色描述")
	private String roleDesc;

	@ApiModelProperty(value="角色对应的菜单信息", required = false)
	private List<Menu> menus;

	@ApiModelProperty(value="角色对应菜单id")
	private List<String> menuIds;

}
