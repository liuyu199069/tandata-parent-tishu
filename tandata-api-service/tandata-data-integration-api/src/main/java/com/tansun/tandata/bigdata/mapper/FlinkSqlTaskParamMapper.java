package com.tansun.tandata.bigdata.mapper;


import com.tansun.tandata.bigdata.entity.FlinkSqlTaskParamModel;

import java.util.List;
import java.util.Map;

/**
 * @author wenzhifeng
 * @date 2020/4/15 16:16
 */
public interface FlinkSqlTaskParamMapper {

    Integer fetchTaskCountByName(String taskName);

    //新增flinkSql任务的一组参数
    void insert(FlinkSqlTaskParamModel flinkSqlTaskParamModel);

    //根据参数查找任务参数列表
    Integer fetchTaskCountByIdAndName(Map<String, Object> params);

    void updateTaskParam(FlinkSqlTaskParamModel taskParamModel);

    List<FlinkSqlTaskParamModel> fetchTaskParamList(Map<String, String> req);

    void deleteTaskParamById(FlinkSqlTaskParamModel flinkSqlTaskParamModel);
}
