package com.tansun.tandata.vo.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 服务配置表信息
 * @author LinLaiChun
 * @date 2020-06-09 15:55:26
 * @Version v1.0
 */
@Data
@ApiModel(value="服务配置表信息")
public class QuartzServerRESP implements Serializable {
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "主键")
	private String id;

	@ApiModelProperty(value = "服务名")
	private String serviceName;

	@ApiModelProperty(value = "服务链接")
	private String serviceUrl;

}