package com.tansun.tandata.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @description: 树
 * @author: linLaiChun
 * @create: 2020-05-08
 * @Version v1.0
 **/
@Data
@ApiModel(value="树")
public class TreeREQ implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "父级ID")
    private String parentId;

}
