package com.tansun.tandata.utils;

import com.github.pagehelper.PageInfo;
import com.tansun.tjmas.cloud.base.redis.utils.RedisUtils;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 
 * 控制器父类，主要是封装成功和出错的返回
 * @param <T>
 */
public class ParentController{

	@Value("${upload.suffixs}")
	protected String uploadSuffixs;
	@Value("${upload.path}")
	protected String uploadFilePath;
	@Value("${upload.size}")
	protected String uploadFileSize;
	@Value("${upload.unit}")
	protected String fileUnit;

	protected org.slf4j.Logger logger= LoggerFactory.getLogger(this.getClass());

	@Autowired
	protected LocaleMessage localeMessage;
	@Autowired
	protected HttpServletRequest currentRequest;

	@Autowired
	protected RedisUtils redisUtils;


	/*************************ResponseBase***************************************/
	
	// 返回成功 ,data值为null
	public ResponseBase setResultSuccessSetMsg(String msg) {
		return setResult(Constants.HTTP_RES_CODE_200, msg, null);
	}
	// 返回成功 ,data值为null
	public ResponseBase setResultSuccess() {
		return setResult(Constants.HTTP_RES_CODE_200, Constants.HTTP_RES_CODE_200_VALUE, null);
	}
	// 返回成功 ,data可传
	public ResponseBase setResultSuccess(Object data) {
		return setResult(Constants.HTTP_RES_CODE_200, Constants.HTTP_RES_CODE_200_VALUE, data);
	}
	// 返回失败
	public ResponseBase setResultError(String msg){
		logger.error(msg);
		return setResult(Constants.HTTP_RES_CODE_500,msg, null);
	}
	// 返回系统错误
	public ResponseBase sysError(){
		String msg = localeMessage.getMessage("sys.common.error");
		logger.error(msg);
		return setResultError(msg);
	}

	// 返回系统错误
	public ResponseBase sysError(Exception e){
		String msg = localeMessage.getMessage("sys.common.error");
		logger.error(e.toString(),e);
		return setResultError(msg);
	}

	// 返回参数为空
	public ResponseBase sysNullError(){
		String msg = localeMessage.getMessage("sys.common.paramEmpty");
		logger.error(msg);
		return setResultError(msg);
	}

	// 返回数据不存在
	public ResponseBase sysNotExistError(){
		String msg = localeMessage.getMessage("sys.common.isNotExist");
		logger.error(msg);
		return setResultError(msg);
	}

	// 自定义返回结果
	public ResponseBase setResult(Integer code, String msg, Object data) {
		ResponseBase responseBase = new ResponseBase();
		responseBase.setCode(code);
		responseBase.setMsg(msg);
		if (data != null) {
			responseBase.setData(data);
		}
		return responseBase;
	}

	public ResponseBase setTableResultSuccess(List<?> list) {
		Map<String, Object> data = new HashMap<>();
		data.put("rows", list);
		data.put("total", new PageInfo(list).getTotal());

		return setResult(Constants.HTTP_RES_CODE_200, Constants.HTTP_RES_CODE_200_VALUE, data);
	}

	public List<FieldErrorTip> getFieldErrorTips(BindingResult bindingResult) {
		List<FieldErrorTip> fields = new ArrayList<FieldErrorTip>();
		FieldErrorTip fieldErrorTip;
		for (FieldError error : bindingResult.getFieldErrors()) {
			fieldErrorTip = new FieldErrorTip(error.getField(), error.getDefaultMessage());
			fields.add(fieldErrorTip);
		}
		return fields;
	}

	/**
	 * 参数校验
	 * @param bindingResult
	 * @return
	 */
	public ResponseBase validateField(BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			List<FieldErrorTip> fields = getFieldErrorTips(bindingResult);
			return setResult(Constants.HTTP_RES_CODE_401, null, fields);
		}
		return null;
	}

	/**
	 * 通过ID获取用户名
	 * @param userCode
	 * @return
	 */
	protected String getUserNameById(String userCode){
		if(StringUtil.isEmpty(userCode)){
			return Constants.CHAR_BLANK;
		}
		Object obj = redisUtils.get(Constants.USER_REDIS + userCode);
		if(obj==null){
			return Constants.CHAR_BLANK;
		}
		return (String) obj;
	}
}
