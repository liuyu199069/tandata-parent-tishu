package com.tansun.tandata.bigdata.vo.req;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * @author wenzhifeng
 * @date 2020/4/15 16:01
 */
@ApiModel(value="新增任务参数",description="新增任务参数")
@Data
public class FlinkSqlAddTaskParamReq implements Serializable {

    private static final long serialVersionUID = 9149467872280077464L;

    @ApiModelProperty(value="流程图的json串", name="allJson")
    @NotBlank
    private String allJson;

}
