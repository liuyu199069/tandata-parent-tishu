package com.tansun.tandata.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Data
@Table(name="LOG_INFO_ERRO")
public class LogInfoErro implements Serializable {

    private static final long serialVersionUID = -1244581593373387004L;
    /**
     * ID
     */
    @Id
    @Column(name = "ID")
    private String id;//主键
    /**
     * 请求url
     */
    @Column(name = "URL")
    private String url;
    /**
     * 请求参数
     */
    @Column(name = "PARAM_STR")
    private String paramStr;
    /**
     * 服务英文名
     */
    @Column(name = "server_id")
    private String serverId;
    /**
     * 服务员中文名
     */
    @Column(name = "server_name")
    private String severName;
    /**
     * 创建时间
     */
    @Column(name = "creater_time")
    @JsonFormat(locale = "ZH",timezone = "GMT+8")
    private Date createTime;
    /**
     *请求方式
     */
    @Column(name = "REQ_TYPE")
    private int reqType;
    /**
     * 报错详情
     */
    @Column(name = "DESRIBE")
    private String desribe;


}
