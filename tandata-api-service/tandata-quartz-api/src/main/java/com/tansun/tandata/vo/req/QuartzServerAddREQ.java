package com.tansun.tandata.vo.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 新增服务配置
 * @author LinLaiChun
 * @date 2020-06-09 15:55:26
 * @Version v1.0
 */
@Data
@ApiModel(value="新增服务配置")
public class QuartzServerAddREQ implements Serializable {
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "主键", required = false)
	private String id;

	@ApiModelProperty(value = "服务名", required = false)
	private String serviceName;

	@ApiModelProperty(value = "服务链接", required = false)
	private String serviceUrl;

}